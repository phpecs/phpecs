<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class StoreOrderList extends Model{

    /**
     * 查询指定数据
     * @param $id
     * @param $field
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selOrderListZhi($id, $field){
        return self::alias('o')->join('store_product p', 'o.pid = p.id')->where('o.oid', $id)->field($field)->select();
    }

    /**
     * 统计数量
     * @param string $field
     * @return int|string
     */
    public static function countOrderList($where, $field = '*'){
        return self::where($where)->count($field);
    }

    /**
     * 总和
     * @param string $field
     * @return float|int
     */
    public static function sumOrderList($where, $field = ''){
        return self::where($where)->sum($field);
    }

    /**
     * 查询指定条数
     * @param $where
     * @param int $limit
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selOrderListSort($where, $limit = 5){
        return self::where($where)->group('pid')->field('pid, SUM(num) num')->order('SUM(num) DESC')->limit($limit)->select();
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class StoreRefund extends Model{

    /**
     * 统计条数
     * @param $where
     * @return int|string
     * @throws \think\Exception
     */
    public static function countRefund($where){
        return self::where($where)->count();
    }

    /**
     * 分页查询
     * @param $where
     * @param $paginate
     * @param string $order
     * @return $this
     */
    public static function selRefundPage($where, $paginate, $order = 'time desc'){
        return self::where($where)->order($order)->paginate($paginate)->each(function ($item, $k){
            $order = StoreOrder::getOrderInfo(['id' => $item['oid']], 'order, money');
            $item['order'] = $order['order'];
            $item['money'] = $order['money'];
            //查询出订单详情
            $field = 'o.spec,o.num,p.id,p.name';
            $details = StoreOrderList::selOrderListZhi($item['oid'], $field);
            foreach ($details as $key => $val){
                $details[$key]['img'] = StoreProductImg::getField(['pid' => $val['id']], 'img');
            }
            $item['lower'] = $details;
            return $item;
        });
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getRefundInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改某个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询一个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }
}
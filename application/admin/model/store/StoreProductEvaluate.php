<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class StoreProductEvaluate extends Model{

    /**
     * 获取评价数据
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selEvaluate($where, $page, $limit, $order = 'time desc'){
        $data = self::whereEvaluate($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = StoreProduct::getField(['id' => $v['pid']], 'name');
        }
        $count = self::whereEvaluate($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereEvaluate($where){
        return self::where($where);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getEvaluateInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insertGetId($data);
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除一条数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }
}
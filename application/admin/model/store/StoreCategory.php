<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class StoreCategory extends Model{

    /**
     * 查出所有商品分类
     * @param string $where
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selCategory($where = '', $order = 'sort asc'){
        return self::where($where)->order($order)->select();
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getCategoryInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 查询一个字段值
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field, $type = 0){
        if ($type == 0){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除一条数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }

    /**
     * 修改某个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }
}
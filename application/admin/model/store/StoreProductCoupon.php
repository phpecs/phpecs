<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class StoreProductCoupon extends Model{

    /**
     * 查询指定数据
     * @param $start
     * @param $limit
     * @return array
     */
    public static function selCouponZhi($start, $limit){
        $data = self::query("SELECT * FROM higo_store_product_coupon WHERE total = receive AND total != 0 ORDER BY id DESC LIMIT $start,$limit");
        $count = self::query("SELECT COUNT(*) FROM higo_store_product_coupon WHERE total = receive AND total != 0");
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 分页查询
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selCoupon($where, $page, $limit, $order = 'id desc'){
        $data = self::whereCoupon($where)->order($order)->limit($page, $limit)->select();
        $count = self::whereCoupon($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereCoupon($where){
        return self::where($where);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getCouponInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改一条数据
     * @param $where
     * @return int
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 修改一个字段的值
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 获取某个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class User extends Model{

    /**
     * 返回某一字段一组或一条数据
     * @param string $where
     * @param null $field
     * @param int $type
     * @return array|mixed
     */
    public static function getField($where, $field, $type = 0){
        if ($type == 0){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }


    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 字段值减
     * @param $where
     * @param $field
     * @param int $value
     * @return int|true
     */
    public static function decField($where, $field, $value = 1){
        return self::where($where)->setDec($field, $value);
    }

    /**
     * 礼品商家
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function lipinShop(){
        $user =  self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.gift', 1)->field('v.id, v.name, v.phone')->select();
        foreach ($user as $k => $v){
            $user[$k]['name'] = base64_decode($v['name']);
        }
        return $user;
    }

    /**
     * 统计
     * @param $where
     * @return int|string
     * @throws \think\Exception
     */
    public static function countUser($where){
        return self::where($where)->count();
    }

    /**
     * 查询指定数据
     * @param $where
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selUserZhi($where, $page, $limit){
        $data = self::alias('v')->join('user_role r', 'v.id = r.uid')->where($where)->where(function ($query){
            $query->where('r.gift', 1)->whereOr('r.drainage', 1);
        })->field('v.id,v.name,v.time,v.phone')->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            $data[$k]['phone'] = $v['phone'] ? $v['phone'] : '-';
            $data[$k]['time'] = date('Y-m-d H:i:s');
        }
        $count = self::alias('v')->join('user_role r', 'v.id = r.uid')->where($where)->where(function ($query){
            $query->where('r.gift', 1)->whereOr('r.drainage', 1);
        })->count();
        $list = [
            'data' => $data->toArray(),
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条数查询
     * @param $info
     * @param $name
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selUserLimit($info, $name, $page, $limit, $order = 'id desc'){
        $data = self::where('phone', 'like', "%$info%")->whereOr('name', 'like', "%$name%")->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            //查询上级没有则为平台
            $upper = self::where(['id' => $v['upper']])->value('name');
            $data[$k]['upper'] = $upper ? base64_decode($upper) : '平台';
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
            $find = UserRole::getUserRoleInfo(['uid' => $v['id']]);
            $data[$k]['pf'] = $find['pf'] ? $find['pf'] : 0;
            $data[$k]['super'] = $find['super'] ? $find['super'] : 0;
            $data[$k]['retail'] = $find['retail'] ? $find['retail'] : 0;
            //会员等级查询
            $grade_name = UserGrade::getField(['grade' => $v['grade'], 'status' => 1], 'name');
            $data[$k]['grade_name'] = $grade_name ? $grade_name : '-';
        }
        $count = self::where('phone', 'like', "%$info%")->whereOr('name', 'like', "%$name%")->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 查询指定条数
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selUserZhiLimit($where, $page, $limit, $order = 'id desc'){
        $data = self::where($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
        }
        $count = self::where($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 修改某一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询业务员
     * @param $info
     * @param $name
     * @param $page
     * @param $limit
     */
    public static function selUserY($info, $name, $page, $limit){
        $data = self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.business', 1)
            ->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->limit($page, $limit)->order('v.id desc')->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            //查询上级没有则为平台
            $upper = self::getField(['id' => $v['upper']], 'name');
            $data[$k]['upper'] = $upper ? $upper : '平台';
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
        }
        $count = self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.business', 1)
            ->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 查询礼品商家
     * @param $info
     * @param $name
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selUserG($info, $name, $page, $limit){
        $data = self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.gift', 1)
            ->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->limit($page, $limit)->order('v.id desc')->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            //查询招商员
            $recruit = self::getUserInfo(['id' => $v['recruit']], 'name, phone');
            $recruit['name'] = base64_decode($recruit['name']);
            $data[$k]['recruit'] = $recruit;
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
        }
        $count = self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.gift', 1)
            ->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 查询引流商家
     * @param $info
     * @param $name
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selUserD($info, $name, $page, $limit){
        $data = self::alias('v')->join('user_role r', 'v.id = r.uid')->where('r.drainage', 1)
            ->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->limit($page, $limit)->order('v.id desc')->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            //查询招商员
            $recruit = self::getUserInfo(['id' => $v['recruit']], 'name, phone');
            $recruit['name'] = base64_decode($recruit['name']);
            $data[$k]['recruit'] = $recruit;
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
        }
        $count = self::alias('v')->join('user_role r', 'v.id = r.uid')
            ->where('r.drainage', 1)->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 批发会员
     * @param $info
     * @param $name
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selUserP($info, $name, $page, $limit){
        $data = self::alias('v')->join('user_role r', 'v.id = r.uid')
            ->where('r.pf', 1)->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->limit($page, $limit)->order('v.id desc')->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            //查询招商员
            $recruit = self::getUserInfo(['id' => $v['recruit']], 'name, phone');
            $recruit['name'] = base64_decode($recruit['name']);
            $data[$k]['recruit'] = $recruit;
            $data[$k]['phone'] = $data[$k]['phone'] ? $data[$k]['phone'] : '-';
        }
        $count = self::alias('v')->join('user_role r', 'v.id = r.uid')
            ->where('r.pf', 1)->where(function ($query)use($info, $name){
                $query->where('v.phone', 'like', "%$info%")->whereOr('v.name', 'like', "%$name%");
            })->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 返回ECharts查询数据
     * @param $status
     * @param string $order
     * @return array
     */
    public static function selUserECharts($order = 'time asc'){
        $time = strtotime(date('Y-m-d', strtotime('-1 Month')));
        $minTime = strtotime(date('Y-m-d', self::min('time'))); //数据库里面最小的时间
        if ($time) {
            $where['time'] = ['egt', $time];
            //如果小于数据库最小时间直接使用数据库最小时间开始组合
            $time = $time < $minTime ? $minTime : $time;
        }else{
            $time = $minTime;
        }
        $data = self::where('time', 'egt', $time)->field('time')->order($order)->select();
        $day = ceil(((time() - $time) / 86400)); //计算有多少天
        $arr = []; //新的数组
        for ($i = 0; $i < $day; $i++){
            $startTime = $time + 86400 * $i; //每天开始时间戳
            $endTime = $time + 86400 * $i + 86400; //每天结束时间戳
            $daily = date('Y-m-d', $startTime); //每天的日期
            $arr[$daily]['count'] = 0; //当天新增人数
            foreach ($data as $k => $v){
                if ($v['time'] > $startTime && $v['time'] < $endTime){
                    $arr[$daily]['count'] += 1;
                }
            }
        }
        return $arr;
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserLipin extends Model{

    /**
     * 查出所有领取用户
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selUserLipin($where, $page, $limit, $order = 'time desc'){
        $data = self::whereUserLipin($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['time'] = date('Y-m-d H:i:s', $v['time']);
            $data[$k]['uid'] = base64_decode(User::getField(['id' => $v['uid']], 'name'));
        }
        $count = self::whereUserLipin($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereUserLipin($where){
        return self::where($where);
    }
}
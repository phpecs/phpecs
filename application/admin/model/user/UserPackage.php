<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserPackage extends Model{

    /**
     * 查出用户大礼包
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     */
    public static function selUserPackage($where, $page, $limit, $order = 'id desc'){
        $data = self::whereUserPackage($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $info = User::getUserInfo(['id' => $v['uid']]);
            $data[$k]['name'] = base64_decode($info['name']);
            $data[$k]['openid'] = $info['openid'];
            $data[$k]['phone'] = $info['phone'] ? $info['phone'] : '-';
        }
        $count = self::whereUserPackage($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereUserPackage($where){
        return self::where($where);
    }

    /**
     * 查询指定数据
     * @param $where
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selUserPackageZhi($where, $page, $limit){
        return self::where($where)->limit($page, $limit)->group('yid')->column('yid');
    }

    /**
     * 统计条数
     * @param $where
     * @return int|string
     */
    public static function countUserPackage($where){
        return self::where($where)->count();
    }
}
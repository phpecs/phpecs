<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserChoose extends Model{

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查出申请的大礼包
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     */
    public static function selUserChoose($where, $page, $limit){
        $data = self::joinUserChoose()->where($where)->field('c.*, p.name, p.zid')->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['uid'] = base64_decode(User::getField(['id' => $v['uid']], 'name'));
            $data[$k]['zid'] = $v['zid'] == 0 ? '平台' : base64_decode(User::getField(['id' => $v['zid']], 'name'));
            $data[$k]['money'] = $v['money'] > 0 ? $v['money'] : 0.00;
        }
        $count = self::joinUserChoose()->where($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * join
     * @return $this
     */
    public static function joinUserChoose(){
        return self::alias('c')->join('goods_package p', 'c.package = p.id');
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserChooseInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 修改一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }


}
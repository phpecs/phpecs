<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use app\admin\model\goods\GoodsPackage;
use think\Model;
class UserPifa extends Model{

    /**
     * 统计条数
     * @param $where
     * @return int|string
     * @throws \think\Exception
     */
    public static function countLead($where){
        return self::where($where)->count();
    }

    /**
     * 查出审核数据
     * @param $where
     * @param $info
     * @param $nameId
     * @param $page
     * @param $limit
     * @return array
     */
    public static function selPifa($where, $info, $nameId, $page, $limit){
        $data = self::where($where)->where(function ($query)use($info, $nameId){
            $query->where('phone', 'like', "%$info%")->whereOr('uid', 'in', $nameId);
        })->limit($page, $limit)->order('time desc')->select();
        foreach ($data as $k => $v){
            $name = User::getField(['id' => $v['uid']], 'name');
            $data[$k]['uid'] = base64_decode($name);
        }
        $count = self::where($where)->where(function ($query)use($info, $nameId){
            $query->where('phone', 'like', "%$info%")->whereOr('uid', 'in', $nameId);
        })->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 修改某一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getPifaInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }
}
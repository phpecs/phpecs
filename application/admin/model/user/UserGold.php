<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserGold extends Model{

    /**
     * 佣金列表
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selUserGold($status, $dateTime, $page, $limit, $order = 'time desc'){
        if ($status == 5){
            $dateTime = explode(' - ', $dateTime);
            $start = strtotime($dateTime[0]);
            $end = strtotime($dateTime[1]) + 86399; //结束时间是日期按当天最后一秒算
            $where['time'] = [['egt', $start], ['elt', $end]];
        }else{
            $time = TimeAssign($status); //开始时间
            if ($time) $where['time'] = ['egt', $time];
        }
        $data = self::whereUserGold($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $find = User::getUserInfo(['id' => $v['uid']], 'id, name, phone');
            $name = base64_decode($find['name']);
            $phone = '('.$find['phone'].')';
            $data[$k]['uid'] = $name.$phone;
        }
        $count = self::whereUserGold($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereUserGold($where){
        return self::where($where);
    }

    /**
     * 合计某个字段
     * @param $where
     * @param $field
     * @return float|int
     */
    public static function sumGold($where, $field){
        return self::where($where)->sum($field);
    }

    /**
     * 返回ECharts查询数据
     * @param $status
     * @param string $order
     * @return array
     */
    public static function countGold($status, $dateTime, $order = 'time asc'){
        $min = self::min('time');
        $minTime = $min ? strtotime(date('Y-m-d', $min)) : time(); //数据库里面最小的时间
        if ($status == 5){
            $dateTime = explode(' - ', $dateTime);
            $start = strtotime($dateTime[0]) < $minTime ? $minTime : strtotime($dateTime[0]);
            $end = strtotime($dateTime[1]) + 86399; //结束时间是日期按当天最后一秒算
            $where['time'] = [['egt', $start], ['elt', $end]];
            $day = ceil((($end - $start) / 86400)); //计算有多少天
        }else{
            $start = TimeAssign($status); //开始时间
            if ($start) {
                $where['time'] = ['egt', $start];
                //如果小于数据库最小时间直接使用数据库最小时间开始组合
                $start = $start < $minTime ? $minTime : $start;
            }else{
                $start = $minTime;
            }
            $day = ceil(((time() - $start) / 86400)); //计算有多少天
        }
        $data = self::where($where)->field('time, gold')->order($order)->select();
        $arr = []; //新的数组
        for ($i = 0; $i < $day; $i++){
            $startTime = $start + 86400 * $i; //每天开始时间戳
            $endTime = $start + 86400 * $i + 86400; //每天结束时间戳
            $daily = date('Y-m-d', $startTime); //每天的日期
            $arr[$daily]['gold'] = 0; //返现佣金
            foreach ($data as $k => $v){
                if ($v['time'] >= $startTime && $v['time'] < $endTime){
                    $arr[$daily]['gold'] += $v['gold'];
                }
            }
        }

        $backNum = self::where($where)->count(); //返现笔数
        $backWholeNum = self::count(); //返现总笔数
        $backMoney = self::where($where)->sum('gold'); //返现佣金
        $backWholeMoney = self::sum('gold'); //返现总佣金
        $record = compact('backNum', 'backWholeNum', 'backMoney', 'backWholeMoney');
        return compact('record', 'arr');
    }

    /**
     * 查询指定数据
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selGold($where, $field = ''){
        return self::where($where)->field($field)->select();
    }
}
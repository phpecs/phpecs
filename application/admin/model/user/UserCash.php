<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserCash extends Model{

    /**
     * 提现列表
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selUserCash($where, $page, $limit, $order = 'time desc'){
        $data = self::whereUserCash($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $find = User::getUserInfo(['id' => $v['uid']], 'id, name, phone');
            $name = base64_decode($find['name']);
            $phone = '('.$find['phone'].')';
            $data[$k]['uid'] = $name.$phone;
        }
        $count = self::whereUserCash($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereUserCash($where){
        return self::where($where);
    }

    /**
     * 查询一条佣金记录
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserCashInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改一条字段值
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 统计条数
     * @param $where
     * @return int|string
     * @throws \think\Exception
     */
    public static function countCash($where){
        return self::where($where)->count();
    }

    /**
     * 合计某个字段
     * @param $where
     * @param $field
     * @return float|int
     */
    public static function sumCash($where, $field){
        return self::where($where)->sum($field);
    }

    /**
     * 查询指定数据
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selCash($where, $field = ''){
        return self::where($where)->field($field)->select();
    }

    /**
     * 计算记录
     * @return array
     */
    public static function countWholeCash(){
        $wholeUser = self::group('uid')->count(); //提现总人数
        $stroke = self::count(); //总提现笔数
        $toMonth = mktime(0, 0, 0, date('m'), 1, date('Y')); //本月开始时间
        $benUser = self::where('time', 'egt', $toMonth)->group('uid')->count(); //本月提现人数
        $benStroke = self::where('time', 'egt', $toMonth)->count(); //本月提现笔数
        $benMoney = self::where('time', 'egt', $toMonth)->sum('money'); //本月提现金额
        $upMonth = strtotime(date('Y-m', strtotime('-1 month'))); //上月开始时间
        $upUser = self::where('time', 'egt', $upMonth)->where('time', 'lt', $toMonth)->group('uid')->count(); //上月提现人数
        $upStroke = self::where('time', 'egt', $upMonth)->where('time', 'lt', $toMonth)->count(); //上月提现笔数
        $upMoney = self::where('time', 'egt', $upMonth)->where('time', 'lt', $toMonth)->sum('money'); //上月提现金额
        return compact('wholeUser', 'stroke', 'benUser', 'benStroke', 'benMoney', 'upUser', 'upStroke', 'upMoney');
    }

    /**
     * 返回ECharts查询数据
     * @param $status
     * @param string $order
     * @return array
     */
    public static function selCashECharts($status, $dateTime, $order = 'time asc'){
        $min = self::min('time');
        $minTime = $min ? strtotime(date('Y-m-d', $min)) : time(); //数据库里面最小的时间
        if ($status == 5){
            $dateTime = explode(' - ', $dateTime);
            $start = strtotime($dateTime[0]) < $minTime ? $minTime : strtotime($dateTime[0]);
            $end = strtotime($dateTime[1]) + 86399; //结束时间是日期按当天最后一秒算
            $where['time'] = [['egt', $start], ['elt', $end]];
            $day = ceil((($end - $start) / 86400)); //计算有多少天
        }else{
            $start = TimeAssign($status); //开始时间
            if ($start) {
                $where['time'] = ['egt', $start];
                //如果小于数据库最小时间直接使用数据库最小时间开始组合
                $start = $start < $minTime ? $minTime : $start;
            }else{
                $start = $minTime;
            }
            $day = ceil(((time() - $start) / 86400)); //计算有多少天
        }
        $data = self::where($where)->field('time, uid, money, type')->order($order)->select();
        $arr = []; //新的数组
        for ($i = 0; $i < $day; $i++){
            $startTime = $start + 86400 * $i; //每天开始时间戳
            $endTime = $start + 86400 * $i + 86400; //每天结束时间戳
            $daily = date('Y-m-d', $startTime); //每天的日期
            $arr[$daily]['money'] = 0; //提现金额
            $uidArr = []; //声明一个用户数组
            foreach ($data as $k => $v){
                if ($v['time'] >= $startTime && $v['time'] < $endTime){
                    array_push($uidArr, $v['uid']);
                    $arr[$daily]['money'] += $v['money'];
                }
            }
            $arr[$daily]['count'] = count(array_unique($uidArr)); //提现人数 统计去重后的人数
        }
        $gold = self::where($where)->where('type', 0)->count(); //提现佣金笔数
        $yue = self::where($where)->where('type', 1)->count(); //提现余额笔数
        $type = [['name' => '佣金', 'value' => $gold], ['name' => '余额', 'value' => $yue]];
        return compact('arr', 'type');
    }
}
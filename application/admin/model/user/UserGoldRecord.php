<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\store;
use think\Model;
class UserGoldRecord extends Model{

    /**
     * 条数查询
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     */
    public static function selGoldRecordLimit($where, $page, $limit, $order = 'time desc'){
        $data = self::where($where)->order($order)->limit($page, $limit)->select();
        $count = self::where($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }
}
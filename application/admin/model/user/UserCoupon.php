<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\user;
use think\Model;
class UserCoupon extends Model{

    /**
     * 添加多条数据
     * @param $data
     * @return int|string
     */
    public static function adds($data){
        return self::insertAll($data);
    }

    /**
     * 返回ECharts查询数据
     * @param $status
     * @param string $order
     * @return array
     */
    public static function selCouponECharts($status, $dateTime, $order = 'time asc'){
        $min = self::min('time');
        $minTime = $min ? strtotime(date('Y-m-d', $min)) : time(); //数据库里面最小的时间
        if ($status == 5){
            $dateTime = explode(' - ', $dateTime);
            $start = strtotime($dateTime[0]) < $minTime ? $minTime : strtotime($dateTime[0]);
            $end = strtotime($dateTime[1]) + 86399; //结束时间是日期按当天最后一秒算
            $where['time'] = [['egt', $start], ['elt', $end]];
            $day = ceil((($end - $start) / 86400)); //计算有多少天
        }else{
            $start = TimeAssign($status); //开始时间
            if ($start) {
                $where['time'] = ['egt', $start];
                //如果小于数据库最小时间直接使用数据库最小时间开始组合
                $start = $start < $minTime ? $minTime : $start;
            }else{
                $start = $minTime;
            }
            $day = ceil(((time() - $start) / 86400)); //计算有多少天
        }
        $data = self::where($where)->field('id, time, status, type')->order($order)->select();
        $arr = []; //新的数组
        for ($i = 0; $i < $day; $i++){
            $startTime = $start + 86400 * $i; //每天开始时间戳
            $endTime = $start + 86400 * $i + 86400; //每天结束时间戳
            $daily = date('Y-m-d', $startTime); //每天的日期
            $arr[$daily]['received'] = 0; //领取数量
            $arr[$daily]['grant'] = 0; //发放数量
            $arr[$daily]['use'] = 0; //使用数量
            foreach ($data as $k => $v){
                if ($v['time'] >= $startTime && $v['time'] < $endTime){
                    if ($v['type'] == 0){
                        $arr[$daily]['received'] += 1;
                    }else{
                        $arr[$daily]['grant'] += 1;
                    }
                    if ($v['status'] == 1){ //已使用
                        $arr[$daily]['use'] += 1;
                    }
                }
            }
        }
        $count['received'] = self::where('type', 0)->count(); //已领取优惠券
        $count['grant'] = self::where('type', 1)->count(); //系统赠送优惠券
        $count['use'] = self::where('status', 1)->where($where)->count(); //已使用优惠券
        return compact('arr', 'count');
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\system;
use think\Model;
class Secret extends Model{


    /**
     * 修改数据
     * @param $data
     * @return $this
     */
    public static function edit($data){
        return self::where('id', 1)->update($data);
    }

    /**
     * 查询一条数据
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getSecretInfo($field = ''){
        return self::where('id', 1)->field($field)->find();
    }
}
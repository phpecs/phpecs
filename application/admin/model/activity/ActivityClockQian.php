<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\activity;
use think\Model;
class ActivityClockQian extends Model{

    /**
     * 查询签到数据
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selQian($where, $page, $limit, $order = 'time desc'){
        $data = self::whereQian($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['time'] = date('Y-m-d', $v['time']);
            $data[$k]['use'] =$v['use'] == 0 ? '第一次签到' : '第二次签到';
        }
        $count = self::whereQian($where)->count();
        $list = [
            'data' => $data,
            'count' => $count,
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereQian($where){
        return self::where($where);
    }
}
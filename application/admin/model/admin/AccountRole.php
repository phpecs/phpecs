<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\admin;
use think\Model;
class AccountRole extends Model{

    /**
     * 获取角色权限
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field, $type = 0){
        if ($type == 0){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }

    /**
     * 查询角色权限
     * @param string $field
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selRole($field = ''){
        return self::field($field)->select();
    }

    /**
     * 分页查询
     * @param $page
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selRoleLimit($page, $limit, $order = 'id asc'){
        $data = self::order($order)->limit($page, $limit)->select();
        $count = self::count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getRoleInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除一条数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }
}
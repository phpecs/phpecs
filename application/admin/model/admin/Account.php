<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\admin;
use think\Model;
class Account extends Model{

    /**
     * 查询后台登录账号信息
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getAccountInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 获取账号角色
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 查询管理员数据
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selAccount($page, $limit, $order = 'id asc'){
        $data = self::order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            //替换成角色名字
            $role = explode(',', $v['role']);
            $role_name = AccountRole::getField(['id' => ['in', $role]], 'role', 1);
            $data[$k]['role'] = implode(',', $role_name);
        }
        $count = self::count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除一条数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }

    /**
     * 修改一条字段
     * @param $where
     * @param $field
     * @param $value
     * @return mixed
     */
    public static function upField($where, $field, $value){
        return self::where($where)->field($field)->value($value);
    }
}
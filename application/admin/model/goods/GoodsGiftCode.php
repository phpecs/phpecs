<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\goods;
use think\Model;
class GoodsGiftCode extends Model{

    /**
     * 搜索条件
     * @param $status
     * @return array
     */
    public static function switchWhere($status){
        switch ($status){
            case 1:
                $yiYou = self::group('lid')->column('lid'); //已添加核销码的礼品id
                return ['not in', $yiYou];
            case 2:
                $yiYou = self::group('lid')->column('lid'); //已添加核销码的礼品id
                return ['in', $yiYou];
            case 3:
                $yiYou = self::group('lid')->column('lid'); //已存在数据
                $whole = []; //声明数组
                foreach ($yiYou as $v){ //循环查询已添加的数据
                    $cun = self::where('lid', $v)->where('status', 0)->find();
                    if (!$cun){
                        array_push($whole, $v);
                    }
                }
                return ['in', $whole];
        }
    }

    /**
     * 查询code码数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getGiftCodeInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 删除所有核销码
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }

    /**
     * 添加核销码
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
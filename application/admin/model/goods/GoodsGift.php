<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\goods;
use app\admin\model\user\User;
use think\Model;
class GoodsGift extends Model{

    /**
     * 查询礼品
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getGiftInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 查询礼品
     * @param $lipin
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selGift($lipin, $where, $page, $limit, $order = 'id desc'){
        $data = self::whereGift($lipin, $where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $find = User::getUserInfo(['id' => $v['uid']], 'name, phone');
            $name = base64_decode($find['name']);
            $phone = '('.$find['phone'].')';
            $data[$k]['uid'] = $v['uid'] ? $name.$phone : '平台';
            //查询当前礼品是否已经生成核销码
            $is = GoodsGiftCode::getGiftCodeInfo(['lid' => $v['id']]);
            $data[$k]['is'] = $is ? 1 : 0;
            $data[$k]['money'] = $v['money'] ? $v['money'] : '0.00';
        }
        $count = self::whereGift($lipin, $where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $lipin
     * @param $where
     * @return $this
     */
    public static function whereGift($lipin, $where){
        return self::where('lipin', 'like', "%$lipin%")->where($where);
    }

    /**
     * 添加礼品
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改礼品
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除礼品
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }

    /**
     * 事务添加二维码
     * @param $whole
     * @param $num
     * @param $id
     */
    public static function trans($whole, $num, $id){
        // 启动事务
        self::startTrans();
        try{
            for ($i=0;$i<$whole;$i++){ //循环添加出所有的核销码
                $dang = $i + 1; //当前的个数
                $qi = ceil(($dang)/$num); //计算出当前核销码期数
                $number = str_pad($id.$dang, 6, '0', STR_PAD_LEFT); //前面补零
                $add = [
                    'lid' => $id,
                    'stage' => $qi,
                    'pi' => 1,
                    'number' => $number,
                    'status' => 0
                ];
                $insert = GoodsGiftCode::add($add);
                if (!$insert) {
                    throw new \Exception("添加第".$dang."条数据失败");
                }
            }
            // 提交事务
            self::commit();
            echo json_encode(array('info' => 1));
        } catch (\Exception $e) {
            // 回滚事务
            self::rollback();
            echo json_encode(array('info' => 0, 'msg' => $e->getMessage()));
        }
    }

    /**
     * 查询一个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 修改某一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\goods;
use app\admin\model\user\User;
use think\Model;
class GoodsGiftCodeRecord extends Model{

    /**
     * 核销记录列表
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     * @return array
     */
    public static function selUserCodeRecord($where, $page, $limit, $order = 'h_time desc'){
        $data = self::whereUserCodeRecord($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $my = User::getUserInfo(['id' => $v['uid']], 'name, phone'); //领取人的信息
            $he = User::getUserInfo(['id' => $v['hid']], 'name, phone'); //核销人的信息
            $yin = User::getUserInfo(['id' => $v['yid']], 'name, phone'); //引流商家的信息
            $my['name'] = base64_decode($my['name']);
            $he['name'] = base64_decode($he['name']);
            $yin['name'] = base64_decode($yin['name']);
            $data[$k]['my'] = $my;
            $data[$k]['he'] = $he;
            $data[$k]['yin'] = $yin;
            $data[$k]['lipin'] = GoodsGift::getGiftInfo(['id' => $v['lid']]);
            $data[$k]['hes'] = base64_decode(User::getField(['id' => $data[$k]['lipin']['uid']], 'name'));
        }
        $count = self::whereUserCodeRecord($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function whereUserCodeRecord($where){
        return self::where($where);
    }
}
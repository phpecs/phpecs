<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\goods;
use app\admin\model\user\User;
use think\Model;
class GoodsPackage extends Model{

    /**
     * 获取所有大礼包gid
     * @return array
     */
    public static function columnId(){
        return self::column('gid');
    }

    /**
     * 查出大礼包
     * @param $where
     * @param $page
     * @param $limit
     * @param string $order
     */
    public static function selPackage($where, $page, $limit, $order = 'sort asc'){
        $data = self::wherePackage($where)->order($order)->limit($page, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['zid'] = $v['zid'] == 0 ? '平台' : base64_decode(User::getField(['id' => $v['zid']], 'name'));
        }
        $count = self::wherePackage($where)->count();
        $list = [
            'data' => $data,
            'count' => $count
        ];
        return $list;
    }

    /**
     * 条件
     * @param $where
     * @return $this
     */
    public static function wherePackage($where){
        return self::where($where);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getPackageInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 组合一条大礼包数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改大礼包
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 查询一个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 删除一条数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }

    /**
     * 修改某一个字段的值
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\model\market;
use app\admin\model\user\User;
use think\Model;
class MarketSeckillTime extends Model{

    /**
     * 查询时间段
     * @param $where
     * @param $page
     * @param $limit
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selTimeLimit($where, $page, $limit, $field = '', $order = 'start_time asc'){
        $data = self::where($where)->field($field)->order($order)->limit($page, $limit)->select();
        $count = self::where($where)->count();
        return compact('data', 'count');
    }

    /**
     * 查询时间段
     * @param $where
     * @param $page
     * @param $limit
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selTime($where, $field = '', $order = 'start_time asc'){
        return self::where($where)->field($field)->order($order)->select();
    }

    /**
     * 修改某一字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getTimeInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 删除数据
     * @param $where
     * @return int
     */
    public static function del($where){
        return self::where($where)->delete();
    }
}
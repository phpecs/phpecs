<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2019 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\activity\ActivityClock;
use app\admin\model\activity\ActivityList;
use app\admin\model\activity\ActivityRecharge;
use app\admin\model\store\StoreProduct;

class Activity extends Common{

    //充值活动
    public function recharge(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $arr['status'] = $data['status'];
            $arr['name'] = $data['name'];
            $arr['lowest'] = $data['lowest'];
            $arr['give'] = $data['give'];
            $arr['cashback'] = $data['cashback'];
            $time = explode(' - ', $data['time']); //日期
            $arr['start'] = strtotime($time[0]); //开始日期 转换为时间戳
            $arr['end'] = strtotime($time[1]); //结束日期 转换为时间戳
            $result = ActivityRecharge::upRecharge($arr);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $find = ActivityRecharge::getRecharge();
            $find['time'] = date('Y-m-d', $find['start']) . ' - ' . date('Y-m-d', $find['end']);
            $this->assign('find', $find);
            return view();
        }
    }

    //免费领取
    public function free(){

    }

    //打卡
    public function clock(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = ActivityClock::selClock('', $limit*$page-$limit, $limit);
            foreach ($list['data'] as $k => $v){
                $list['data'][$k]['pid'] = StoreProduct::getField(['id' => $v['pid']], 'name');
            }
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //打卡操作页面
    public function clock_operate(){
        $id = input('id');
        if ($id){
            $find = ActivityClock::getClockInfo(['id' => $id]);
            $find['shop'] = StoreProduct::getField(['id' => $find['pid']], 'name');
            $this->assign('find', $find);
        }
        return view();
    }

    //打卡活动添加
    public function clock_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'name' => $field['name'],
                'status' => 0,
                'tian' => $field['tian'],
                'pid' => $field['pid'],
                'price' => $field['price'],
                'freight' => $field['freight'],
                'img' => $field['img'],
                'color' => $field['color'],
                'rule' => $field['rule'],
                'remind' => $field['remind'],
                'explain' => $field['explain'],
                'num' => $field['num'],
                'bao' => 1,
                'tui' => $field['tui'] == 1 ? 1 : 0,
            ];
            $re = ActivityClock::add($data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改
    public function clock_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $data = [
                'name' => $field['name'],
                'price' => $field['price'],
                'freight' => $field['freight'],
                'img' => $field['img'],
                'color' => $field['color'],
                'remind' => $field['remind'],
                'explain' => $field['explain'],
                'num' => $field['num'],
                'tui' => $field['tui'] == 1 ? 1 : 0,
            ];
            if ($field['status'] == 0){
                $data['tian'] = $field['tian'];
                $data['pid'] = $field['pid'];
                $data['rule'] = $field['rule'];
            }
            $re = ActivityClock::edit(['id' => $id], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //删除
    public function clock_del(){
        if (request()->isPost()){
            $id = input('id');
            $re = ActivityClock::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //关闭报名
    public function stop(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            if ($type == 1){
                $re = ActivityClock::upField(['id' => $id], 'bao', 0);
            }else{
                $re = ActivityClock::upField(['id' => $id], 'bao', 1);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //开关活动
    public function start_close(){
        if (request()->isPost()){
            $id = input('id');
            $value = input('value');
            $re = ActivityClock::upField(['id' => $id], 'status', $value);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //图片上传
    public function upload(){
        upload('clock');
    }

    //图片上传
    public function upload2(){
        upload('activityList');
    }

    /**
     * 活动列表
     * @return \think\response\View
     */
    public function activity(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = ActivityList::selActivityList('', $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 操作页面
     * @return \think\response\View
     */
    public function activity_operate(){
        $id = input('id');
        if ($id){
            $find = ActivityList::getActivityListInfo(['id' => $id]);
            $find['p_name'] = implode(',', StoreProduct::getField(['id' => ['in', $find['pid']]], 'name', 1));
            $this->assign(['id' => $id, 'find' => $find]);
        }
        return view();
    }

    /**
     * 活动添加处理
     */
    public function activity_add(){
        $field = input()['field'];
        $data = [
            'name' => $field['name'],
            'image' => $field['image'],
            'banner' => $field['banner'],
            'type' => $field['type'],
            'action' => $field['action'],
            'pid' => $field['action'] == 1 ? $field['pid'] : '',
            'integral' => $field['action'] == 2 ? $field['consume'] : '0',
            'draw' => json_encode($field['xuan']),
            'content' => $field['content'],
            'status' => 1,
            'time' => time(),
            'num' => $field['num'],
        ];
        $re = ActivityList::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 活动编辑处理
     */
    public function activity_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $data = [
            'name' => $field['name'],
            'image' => $field['image'],
            'banner' => $field['banner'],
            'type' => $field['type'],
            'action' => $field['action'],
            'pid' => $field['action'] == 1 ? $field['pid'] : '',
            'integral' => $field['action'] == 2 ? $field['consume'] : '0',
            'draw' => json_encode($field['xuan']),
            'content' => $field['content'],
            'status' => 1,
            'num' => $field['num'],
        ];
        $image = ActivityList::getField(['id' => $id], 'image'); //查出原来的封面路径
        $banner = ActivityList::getField(['id' => $id], 'banner'); //查出原来的banner路径
        $re = ActivityList::edit(['id' => $id], $data);
        if ($re){
            if ($image != $field['image']) unlink('./uploads/' . $image);
            if ($banner != $field['banner']) unlink('./uploads/' . $banner);
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 删除活动
     */
    public function activity_del(){
        $id = input('id');
        $image = ActivityList::getField(['id' => $id], 'image'); //查出原来的封面路径
        $banner = ActivityList::getField(['id' => $id], 'banner'); //查出原来的banner路径
        $re = ActivityList::del(['id' => $id]);
        if ($re){
            unlink('./uploads/' . $image);
            unlink('./uploads/' . $banner);
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    /**
     * 下线活动
     */
    public function activity_xia(){
        $id = input('id');
        $re = ActivityList::upField(['id' => $id], 'status', 0);
        if ($re){
            successCode('下线成功');
        }else{
            errorCode('下线失败');
        }
    }

    public function add_activity(){
        return view();
    }
}
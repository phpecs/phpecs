<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\activity\ActivityClockQian;
use app\admin\model\store\StoreOrder;
use app\admin\model\store\StoreCategory;
use app\admin\model\store\StoreOrderList;
use app\admin\model\store\StoreOrderRemark;
use app\admin\model\store\StoreProduct;
use app\admin\model\store\StoreProductCoupon;
use app\admin\model\store\StoreProductEvaluate;
use app\admin\model\store\StoreProductEvaluateImg;
use app\admin\model\store\StoreProductImg;
use app\admin\model\store\StoreRefund;
use app\admin\model\user\User;
use app\admin\model\user\UserCoupon;

class Product extends Common{
    //产品类别
    public function category(){
        $list = StoreCategory::selCategory();
        $this->assign('list', json_encode($list));
        return view();
    }

    //类别操作
    public function category_operate(){
        $id = input('id');  //id
        $up = input('up'); //上级id
        if ($id){
            $find = StoreCategory::getCategoryInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        if ($up){
            $name = StoreCategory::getField(['id' => $up], 'name');
            $this->assign('up', $up);
            $this->assign('name', $name);
        }
        return view();
    }

    //添加分类
    public function category_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            if (empty($field['name'])){
                echo json_encode(array('info' => 0, 'msg' => '分类名称不能为空'));
                exit;
            }
            if ($field['sort'] === ''){
                echo json_encode(array('info' => 0, 'msg' => '排序不能为空'));
                exit;
            }elseif (!is_numeric($field['sort'])){
                echo json_encode(array('info' => 0, 'msg' => '排序只能为数字'));
                exit;
            }
            $data['name'] = $field['name'];
            $data['sort'] = $field['sort'];
            $data['pid'] = $field['pid'] ? $field['pid'] : 0;
            $data['status'] = $field['status'] ? $field['status'] : 0;
            $re = StoreCategory::add($data);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '添加失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //修改分类
    public function category_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            if (empty($field['name'])){
                echo json_encode(array('info' => 0, 'msg' => '分类名称不能为空'));
                exit;
            }
            if ($field['sort'] === ''){
                echo json_encode(array('info' => 0, 'msg' => '排序不能为空'));
                exit;
            }elseif (!is_numeric($field['sort'])){
                echo json_encode(array('info' => 0, 'msg' => '排序只能为数字'));
                exit;
            }
            $id = $field['id'];
            $data['name'] = $field['name'];
            $data['sort'] = $field['sort'];
            $data['status'] = $field['status'] ? $field['status'] : 0;
            $re = StoreCategory::edit(['id' => $id], $data);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '修改失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //删除分类
    public function category_del(){
        if (request()->isPost()){
            $id = input('id');
            if (!$id){
                echo json_encode(array('info' => 0, 'msg' => '数据错误'));
                exit;
            }

            $cun = StoreCategory::getCategoryInfo(['pid' => $id]);
            if ($cun){
                echo json_encode(array('info' => 0, 'msg' => '存在下级分类不能删除'));
                exit;
            }
            $re = StoreCategory::del(['id' => $id]);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '删除失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //修改排序
    public function sort_edit(){
        if (request()->isPost()){
            $id = input('id');
            $value = input('value');
            if (empty($id)){
                echo 0;
                exit;
            }
            if ($value === '' || !is_numeric($value)){
                echo 0;
                exit;
            }
            $re = StoreCategory::upField(['id' => $id], 'sort', $value);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改状态
    public function status_edit(){
        if (request()->isPost()){
            $id = input('id');
            $status = input('status');
            if (empty($id)){
                echo 0;
                exit;
            }
            if ($status == 1){
                $re = StoreCategory::upField(['id' => $id], 'status', 1);
            }elseif ($status == 0){
                $re = StoreCategory::upField(['id' => $id], 'status', 0);
            }else {
                $re = false;
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //商品管理
    public function product(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $name = input('name');
            $is_del = input('is_del');
            $cid = input('cid');
            $zhuang = input('zhuang');
            $is_jin = input('is_jin');
            if (!empty($name)){
                $where['name'] = ['like', "%$name%"];
            }
            if ($cid){
                //判断提交过来的分类有没有下级
                $all = StoreCategory::getField(['pid' => $cid], 'id', 1);
                array_unshift($all, $cid);
                $where['cid'] = ['in', $all];
            }
            switch ($zhuang){
                case 1:
                    $where['stock'] = ['>', 0];
                    $where['status'] = 0;
                    break;
                case 2:
                    $where['stock'] = ['=', 0];
                    $where['status'] = 0;
                    break;
                case 3:
                    $where['status'] = 1;
                    break;
            }
            $where['is_del'] = $is_del ? 1 : 0;
            $field = 'id,price,name,stock,volume,time,status,sp_code';
            $list = StoreProduct::selProductZhi($where, $field, $limit*$page-$limit, $limit, $is_jin);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            //查询分类
            $cate = StoreCategory::selCategory(['pid' => 0, 'status' => 1]);; //1级分类
            $cate2 = StoreCategory::selCategory(['pid' => ['neq', 0], 'status' => 1]);; //下级级分类
            if ($cate) $cate = $cate->toArray();
            if ($cate2) $cate2 = $cate2->toArray();
            foreach ($cate as $k => $v){
                foreach ($cate2 as $key => $val){
                    if ($cate2[$key]['pid'] == $cate[$k]['id']){
                        $cate[$k]['lower'][] = $cate2[$key];
                    }
                }
            }
            $count[0] = StoreProduct::countProduct(['is_del' => 0]);
            $count[1] = StoreProduct::countProduct(['is_del' => 1]);
            $this->assign('cate', $cate);
            $this->assign('count', $count);
            return view();
        }
    }

    //商品上下架
    public function up_and_down(){
        $id = input('id');
        $status = input('status', '', 'intval');
        $re = StoreProduct::upField(['id' => $id], 'status', $status);
        if ($re){
            echo 1;
        }else{
            echo 0;
        }
    }

    //商品操作
    public function product_operate(){
        $id = input('id');
        if ($id){
            $find = StoreProduct::getProductInfo(['id' => $id]);
            $find['genus_name'] = $find['genus'] == 0 ? '平台' : base64_decode(User::getField(['id' => $find['genus']], 'name'));
            $img = StoreProductImg::selImg(['pid' => $id]);
            $spec = json_decode($find['spec'], true); //产品规格
            $zhi = json_decode($find['value'], true); //产品值
            $can = [];
            foreach ($spec as $k => $v){
                foreach ($v as $key => $val){
                    $can[$k]['spec'] = $key;
                    $can[$k]['bei'] = $val['bei'];
                    $can[$k]['value']['name'] = $val['name'];
                    $can[$k]['value']['img'] = $val['img'];
                }
            }
            foreach ($zhi as $k => $v){
                $up = $k - 1;
                $up_group = explode(',', $zhi[$up]['group']); //上级组值
                $group = explode(',', $v['group']); //组值
                //统计当前需要显示的td个数
                if ($group[0] == $up_group[0]){ //判断是否为一个组的
                    $name = [];
                    for ($i=0;$i<count($group);$i++){
                        if ($group[$i] != $up_group[$i]){
                            array_push($name, $group[$i]);
                        }
                    }
                    $zhi[$k]['name'] = $name;
                }else{
                    $zhi[$k]['name'] = $group;
                }
            }

            $this->assign('find', $find);
            $this->assign('img', $img);
            if ($can){ //有参数再赋值
                $this->assign('can', $can);
            }
            $this->assign('zhi', $zhi);
            $this->assign('colspan', count($can) + 4);
        }
        //查询分类
        $cate = StoreCategory::selCategory(['pid' => 0, 'status' => 1]);; //1级分类
        $cate2 = StoreCategory::selCategory(['pid' => ['neq', 0], 'status' => 1]);; //下级级分类
        if ($cate) $cate = $cate->toArray();
        if ($cate2) $cate2 = $cate2->toArray();
        foreach ($cate as $k => $v){
            foreach ($cate2 as $key => $val){
                if ($cate2[$key]['pid'] == $cate[$k]['id']){
                    $cate[$k]['lower'][] = $cate2[$key];
                }
            }
        }
        $this->assign('cate', $cate);
        return view();
    }

    //商品添加
    public function product_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'cid' => $field['cid'],
                'name' => $field['name'],
                'stock' => $field['stock'],
                'price' => $field['price'],
                'weight' => $field['weight'],
                'volume' => $field['volume'],
                'ftype' => $field['ftype'],
                'freight' => $field['ftype'] ? $field['freight'] : 0,
                'details' => $field['details'],
                'is_tui' => $field['is_tui'],
                'is_jing' => $field['is_jing'],
                'is_new' => $field['is_new'],
                'service' => $field['service'],
                'video' => $field['video'],
                'p_price' => $field['p_price'],
                'd_price' => $field['d_price'],
                'is_open' => $field['is_open'],
                'kalman' => $field['kalman'],
                'offset' => $field['offset'] ? $field['ka'] : 0,
                'poster' => $field['poster'],
                'gold' => $field['gold'],
                'color' => $field['color'],
                'type' => $field['type'],
                'genus' => $field['genus'] ? $field['genus'] : 0,
                'liang' => $field['liang'],
                'spec' => json_encode($field['spec']),
                'value' => json_encode($field['value']),
                'status' => 0,
                'replace_open' => $field['replace_open'] ? 1 : 0,
                'replace_name' => $field['replace_name'],
                'time' => time(),
                'share' => $field['share'],
            ];
            $id = StoreProduct::add($data);
            if ($id){
                //数据添加成功
                foreach ($field['img'] as $v){
                    $data2 = [
                        'pid' => $id,
                        'img' => $v
                    ];
                    StoreProductImg::add($data2);
                }
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //商品修改
    public function product_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $data = [
                'cid' => $field['cid'],
                'name' => $field['name'],
                'stock' => $field['stock'],
                'price' => $field['price'],
                'weight' => $field['weight'],
                'volume' => $field['volume'],
                'ftype' => $field['ftype'],
                'freight' => $field['ftype'] ? $field['freight'] : 0,
                'details' => $field['details'],
                'is_tui' => $field['is_tui'],
                'is_jing' => $field['is_jing'],
                'is_new' => $field['is_new'],
                'service' => $field['service'],
                'video' => $field['video'],
                'p_price' => $field['p_price'],
                'd_price' => $field['d_price'],
                'is_open' => $field['is_open'],
                'kalman' => $field['kalman'],
                'offset' => $field['offset'] ? $field['ka'] : 0,
                'poster' => $field['poster'],
                'gold' => $field['gold'],
                'color' => $field['color'],
                'type' => $field['type'],
                'genus' => $field['genus'] ? $field['genus'] : 0,
                'liang' => $field['liang'],
                'spec' => json_encode($field['spec']),
                'value' => json_encode($field['value']),
                'status' => 0,
                'time' => time(),
                'replace_open' => $field['replace_open'] ? 1 : 0,
                'replace_name' => $field['replace_name'],
                'share' => $field['share'],
            ];
            $re = StoreProduct::edit(['id' => $id], $data);
            if ($re){
                $del = StoreProductImg::del(['pid' => $id]);
                if ($del){
                    //数据添加成功
                    foreach ($field['img'] as $v){
                        $data2 = [
                            'pid' => $id,
                            'img' => $v
                        ];
                        StoreProductImg::add($data2);
                    }
                }
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //删除至回收站
    public function product_soft_del(){
        $id = input('id');
        $del = input('del');
        $re = StoreProduct::upField(['id' => $id], 'is_del', $del);
        if ($re){
            successCode($del == 1 ? '删除成功' : '恢复成功');
        }else{
            errorCode($del == 1 ? '删除失败' : '恢复失败');
        }
    }

    //商品删除
    public function product_del(){
        $id = input('id');
        if (!$id) errorCode('数据错误');
        $video = StoreProduct::getField(['id' => $id], 'video');
        $re = StoreProduct::del(['id' => $id]);
        if ($re){
            $img = StoreProductImg::getField(['pid' => $id], 'img', 1);
            foreach ($img as $v){
                $imgs = './uploads/' . $v;
                unlink($imgs);
            }
            StoreProductImg::del(['pid' => $id]);
            $videos = './uploads/' . $video;
            unlink($videos);
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    //生成卡密
    public function kalman(){
        if (request()->isPost()){
            $value = input('value');
            $code = "ABCDEFGHIGKLMNOPQRSTUVWXYZ";
            $ka = [];
            for ($i = 0; $i < $value; $i++){
                $rand = $code[rand(0,25)].strtoupper(dechex(date('m'))).date('d').substr(time(),-5).substr(microtime(),2,5).sprintf('%02d',rand(0,99));
                $ka[$i] = $rand;
            }
            echo json_encode(array('info' => 1, 'data' => $ka));
        }else{
            echo json_encode(array('info' => 0));
        }
    }

    //商品二维码
    public function sp_code(){
        if (request()->isPost()){
            $ids = StoreProduct::isNullId();
            StoreProduct::trans($ids);
        }else{
            echo json_encode(array('info' => 0, 'msg' => '数据错误'));
        }
    }

    //订单列表
    public function order(){
        $field = json_decode(input()['field'], true);
        $info = trim($field['info']);
        $status = $field['status'];
        $type = input('type') ? input('type') : $field['type'];
        $active_id = input('active_id') ? input('active_id') : $field['active_id'];
        $active_status = input('active_status') ? input('active_status') : $field['active_status'];
        $bargain_id = input('bargain_id') ? input('bargain_id') : $field['bargain_id'];
        $spell_id = input('spell_id') ? input('spell_id') : $field['spell_id'];
        $seckill_id = input('seckill_id') ? input('seckill_id') : $field['seckill_id'];
        $back_id = input('back_id') ? input('back_id') : $field['back_id'];
        $purchase_id = input('purchase_id') ? input('purchase_id') : $field['purchase_id'];
        $likes_id = input('likes_id') ? input('likes_id') : $field['likes_id'];
        if (!empty($info)){
            $where['order|phone'] = ['like', "%$info%"];
            $this->assign('info', $info);
        }
        if ($status != ''){
            $where['status'] = $status;
            $this->assign('status', $status);
        }
        if ($type == 1){
            $where['type'] = 1;
            $where['active_id'] = $active_id;
            $this->assign('type', 1);
            $this->assign('active_id', $active_id);
        }elseif ($type){
            $where['type'] = $type;
            if ($bargain_id) $where['bargain_id'] = $bargain_id;
            if ($spell_id) $where['spell_id'] = $spell_id;
            if ($seckill_id) $where['seckill_id'] = $seckill_id;
            if ($back_id) $where['back_id'] = $back_id;
            if ($purchase_id) $where['purchase_id'] = $purchase_id;
            if ($likes_id) $where['likes_id'] = $likes_id;
            $this->assign(compact('type'));
        }
        $count[0] = StoreOrder::countOrder($where);
        $count[1] = StoreOrder::countOrderDuo($where, 1);
        $count[2] = StoreOrder::countOrderDuo($where, 2);
        $count[3] = StoreOrder::countOrderDuo($where, 0);
        if ($active_status == 2){
            $where['active_status'] = '1';
        }elseif ($active_status == 3){
            $where['active_status'] = '2';
        }elseif ($active_status == 4){
            $where['active_status'] = '0';
        }
        //分页输出
        $list = StoreOrder::selOrderPage($where, 8);
        // 把分页数据赋值给模板变量list
        $this->assign('list', $list);
        $this->assign('count', $count);
        // 渲染模板输出
        return $this->fetch();
    }

    //订单详情
    public function order_operate(){
        $id = input('id');
        if (!$id){
            $this->error('数据异常');
        }
        //订单信息
        $find = StoreOrder::getOrderInfo(['id' => $id]);
        //查询出订单详情
        $field = 'o.spec,o.unit,o.num,p.id,p.name';
        $details = StoreOrderList::selOrderListZhi($find['id'], $field);
        foreach ($details as $key => $val){
            $details[$key]['img'] = StoreProductImg::getField(['pid' => $val['id']], 'img');
        }
        $find['lower'] = $details;
        //查询出下单用户信息
        $user = User::getUserInfo(['id' => $find['uid']], 'name, phone');
        //备注信息
        $remark = StoreOrderRemark::selRemark(['oid' => $find['id']]);
        $this->assign('find', $find);
        $this->assign('user', $user);
        $this->assign('remark', $remark);
        return view();
    }

    //修改订单总价
    public function order_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $money = $field['money'];
            $newmoney = $field['newmoney'];
            $remark = $field['remark'];
            if (!$id || !$money || !$remark){
                echo 0;
                exit;
            }
            $benefit = StoreOrder::getField(['id' => $id], 'benefit');
            $re = StoreOrder::upField(['id' => $id], 'money', $newmoney + $benefit);
            if ($re){
                //修改成功 添加备注信息
                $data = [
                    'oid' => $id,
                    'user' => session('NAME'),
                    'time' => time(),
                    'info' => '修改订单价格['.$money.'元=>'.$newmoney.'元]',
                    'remark' => $remark,
                    'type' => 0
                ];
                StoreOrderRemark::add($data);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            //查询原本订单总额
            $id = input('id');
            $find = StoreOrder::getOrderInfo(['id' => $id], 'id, money, benefit');
            $this->assign('find', $find);
            return view();
        }
    }

    //收货
    public function order_collect(){
        if (request()->isPost()){
            $id = input('id');
            $re = StoreOrder::upField(['id' => $id], 'status', 3);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //添加订单备注
    public function order_remark(){
        if (request()->isPost()){
            $id = input('id');
            $value = input('value');
            if (!$id || !$value){
                echo 0;
                exit;
            }
            $data = [
                'oid' => $id,
                'user' => session('NAME'),
                'time' => time(),
                'info' => $value,
                'type' => 1
            ];
            $re = StoreOrderRemark::add($data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //删除订单备注
    public function remark_del(){
        if (request()->isPost()){
            $id = input('id');
            $type = StoreOrderRemark::getField(['id' => $id], 'type');
            if (!$id || $type != 1){
                echo 0;
                exit;
            }
            $re = StoreOrderRemark::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //订单发货
    public function order_delivery(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $shipment = $field['shipment'];
            $order_number = $field['order_number'];
            $remark = $field['remark'];
            $status = StoreOrder::getField(['id' => $id], 'status');
            if (!$id || $status != 1 || !$shipment || !$order_number){
                echo 0;
                exit;
            }
            $data = [
                'order_number' => $order_number,
                'shipment' => $shipment,
                'remark' => $remark,
                'status' => 2
            ];
            $re = StoreOrder::edit(['id' => $id], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $id = input('id');
            $address = StoreOrder::getOrderInfo(['id' => $id], 'username, address, phone');
            $this->assign('id', $id);
            $this->assign('address', $address);
            return view();
        }
    }

    //退款列表
    public function refund(){
        $field = json_decode(input()['field'], true);
        $status = $field['status'];
        if ($status != ''){
            $where['status'] = $status;
            $this->assign('status', $status);
        }
        //分页输出
        $list = StoreRefund::selRefundPage($where, 8);
        // 把分页数据赋值给模板变量list
        $this->assign('list', $list);
        // 渲染模板输出
        return $this->fetch();
    }

    //退款详情
    public function refund_operate(){
        $id = input('id');
        if (!$id){
            $this->error('数据异常');
        }
        //退款信息
        $find = StoreRefund::getRefundInfo(['id' => $id]);
        //查出订单
        $order = StoreOrder::getOrderInfo(['id' => $find['oid']]);
        $find['order'] = $order['order'];
        $find['phone'] = $order['phone'];
        $find['money'] = $order['money'];
        $find['benefit'] = $order['benefit'];
        $find['freight'] = $order['freight'];
        //查询出订单详情
        $field = 'o.spec,o.unit,o.num,p.id,p.name';
        $details = StoreOrderList::selOrderListZhi($find['oid'], $field);
        foreach ($details as $key => $val){
            $details[$key]['img'] = StoreProductImg::getField(['pid' => $val['id']], 'img');
        }
        $find['lower'] = $details;
        //查询出下单用户信息
        $user = User::getUserInfo(['id' => $order['uid']], 'name, phone');
        //备注信息
        $remark = StoreOrderRemark::selRemark(['oid' => $find['oid']]);
        $this->assign('find', $find);
        $this->assign('user', $user);
        $this->assign('remark', $remark);
        return view();
    }

    //修改退款总额
    public function refund_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $tui = $field['tui'];
            $newmoney = $field['newmoney'];
            $remark = $field['remark'];
            if (!$id || !$tui || !$remark){
                echo 0;
                exit;
            }
            $re = StoreRefund::upField(['id' => $id], 'tui', $newmoney);
            if ($re){
                //修改成功 添加备注信息
                $oid = StoreRefund::getField(['id' => $id], 'oid');
                $data = [
                    'oid' => $oid,
                    'user' => session('NAME'),
                    'time' => time(),
                    'info' => '修改退款总额['.$tui.'元=>'.$newmoney.'元]',
                    'remark' => $remark,
                    'type' => 0
                ];
                StoreOrderRemark::add($data);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            //查询原本订单总额
            $id = input('id');
            $find = StoreRefund::getRefundInfo(['id' => $id], 'id, tui');
            $this->assign('find', $find);
            return view();
        }
    }

    //通过
    public function refund_ok(){
        if (request()->isPost()){
            $id = input('id');
            $find = StoreRefund::getRefundInfo(['id' => $id]);
            $order = StoreOrder::getOrderInfo(['id' => $find['oid']]);
            if ($find['tui'] > $order['money']){
                echo 2;
                exit;
            }
            $re = $this->tui($order['order'], $order['money'] - $order['benefit'], $find['tui']);
            if ($re){
                StoreRefund::upField(['id' => $id], 'status', 1);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //拒绝
    public function refund_no(){
        if (request()->isPost()){
            $id = input('id');
            $re = StoreRefund::upField(['id' => $id], 'status', 2);
            if ($re){
                $oid = StoreRefund::getField(['id' => $id], 'oid');
                StoreOrder::upField(['id' => $oid, 'status' => 5], 'status', 1);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //退款
    public function tui($oid, $money, $tui){
        // 订单数据
        $order = array(
            'out_trade_no' => $oid, //订单号（需要根据自己的业务修改）
            'out_refund_no' => $oid, //退款单号
            'total_fee' => $money * 100, //订单总金额 (单位为分)
            'refund_fee' => $tui * 100, //退款总金额 (单位为分)
        );
        //商家同意退款
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $refund = $pay->refund($order);
        if ($refund['result_code'] == 'SUCCESS'){
            return true;
        }else{
            return false;
        }
    }

    //商品评价
    public function product_evaluate(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $date = explode(' - ', input('date')); //日期
            $start = strtotime($date[0]); //开始日期 转换为时间戳
            $end = strtotime($date[1]); //结束日期 转换为时间戳
            if ($start && $end){
                $where['time'] = [['gt', $start], ['lt', $end]];
            }
            $list = StoreProductEvaluate::selEvaluate($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //评价操作
    public function evaluate_operate(){
        $id = input('id');
        if ($id){
            $find = StoreProductEvaluate::getEvaluateInfo(['id' => $id]);
            $img = StoreProductEvaluateImg::selImg(['eid' => $id]);
            $find['name'] = StoreProduct::getField(['id' => $find['pid']], 'name');
            $this->assign('find', $find);
            $this->assign('img', $img);
        }
        return view();
    }

    //评价添加
    public function evaluate_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $pid = $field['pid']; //评价产品
            $content = $field['content']; //评价内容
            $img = $field['img']; //图片
            //添加大礼包
            $data = [
                'pid' => $pid,
                'content' => $content,
                'time' => time(),
            ];
            $id = StoreProductEvaluate::add($data);
            if ($id){
                //数据添加成功
                foreach ($img as $v) {
                    $data2 = [
                        'eid' => $id,
                        'img' => $v
                    ];
                    StoreProductEvaluateImg::add($data2);
                }
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //评价修改
    public function evaluate_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id']; //数据id
            $pid = $field['pid']; //评价产品
            $content = $field['content']; //评价内容
            $img = $field['img']; //图片
            //添加大礼包
            $data = [
                'pid' => $pid,
                'content' => $content,
                'time' => time(),
            ];
            $re = StoreProductEvaluate::edit(['id' => $id], $data);
            $del = StoreProductEvaluateImg::del(['eid' => $id]);
            if ($del){
                //数据添加成功
                foreach ($img as $v){
                    $data2 = [
                        'eid' => $id,
                        'img' => $v
                    ];
                    StoreProductEvaluateImg::add($data2);
                }
            }
            if ($re || $del){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //评价删除
    public function evaluate_del(){
        if (request()->isPost()){
            $id = input('id');
            $re = StoreProductEvaluate::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //优惠券
    public function coupon(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $status = input('status');
            $name = input('name');
            if ($status === '0'){
                $list = StoreProductCoupon::selCouponZhi($limit*$page-$limit, $limit);
                echo json_encode(array('code' => 0, 'count' => $list['count'][0]['COUNT(*)'], 'data' => $list['data']));
                exit;
            }elseif ($status === '1'){
                $where['status'] = 0;
            }elseif ($status === '2'){
                $where['status'] = 1;
            }
            if ($name) $where['name'] = ['like', "%$name%"];
            $list = StoreProductCoupon::selCoupon($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //优惠券处理
    public function coupon_operate(){
        $id = input('id');
        if ($id){
            $find = StoreProductCoupon::getCouponInfo(['id' => $id]);
            if ($find) $find = $find->toArray();
            if ($find['term'] == '0'){
                $valid = explode(' - ', $find['valid']);
                $find['start'] = $valid[0];
                $find['end'] = $valid[1];
            }
            if ($find['apply'] == '1'){
                $shop = StoreProduct::selProduct(['id' => ['in', $find['shop']]], 'id, name');
                $this->assign('shop', $shop);
            }
            $this->assign('find', $find);
        }
        return view();
    }

    //优惠券添加
    public function coupon_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'name' => $field['name'],
                'total' => $field['total'],
                'form' => $field['form'],
                'discount' => $field['form'] === '0' ? $field['zhi'] : $field['zhe'],
                'doorsill' => $field['doorsill'],
                'full' => $field['doorsill'] === '1' ? $field['man'] : '',
                'limit' => $field['limit'],
                'term' => $field['term'],
                'valid' => $field['term'] === '0' ? $field['start'] . ' - ' . $field['end'] : $field['tian'],
                'apply' => $field['apply'],
                'shop' => $field['apply'] === '1' ? $field['shop'] : '',
                'explain' => $field['explain'],
                'receive' => 0,
                'use' => 0,
                'status' => 1
            ];
            $re = StoreProductCoupon::add($data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //优惠券修改
    public function coupon_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $data = [
                'name' => $field['name'],
                'total' => $field['total'],
                'explain' => $field['explain'],
            ];
            $re = StoreProductCoupon::edit(['id' => $id], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //使优惠券失效
    public function coupon_shi(){
        if (request()->isPost()){
            $id = input('id');
            $re = StoreProductCoupon::upField(['id' => $id], 'status', 0);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //优惠券发送
    public function coupon_song(){
        if (request()->isPost()){
            $field = input()['field'];
            $pid = explode(',', $field['pid']);
            $all = [];
            for ($i = 0; $i < count($pid); $i++){
                $data = [
                    'uid' => $pid[$i],
                    'cid' => $field['id'],
                    'time' => time(),
                    'status' => 0,
                    'type' => 1
                ];
                array_push($all, $data);
            }
            //添加用户优惠券
            $re = UserCoupon::adds($all);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $id = input('id');
            $find = StoreProductCoupon::getCouponInfo(['id' => $id]);
            $this->assign('find', $find);
            return view();
        }
    }

    //查出所有商家
    public function trader(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $name = base64_encode(input('name'));
            if ($name){
                $where['v.name'] = ['like', "%$name%"];
            }
            $list = User::selUserZhi($where, $page*$limit-$limit, $limit);
            $arr = [['id' => 0, 'name' => '平台', 'phone' => '-', 'time' => '-']];
            $datas = array_merge($arr, $list['data']);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $datas));
        }

    }

    //签到信息
    public function qian(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $id = input('id');
            $list = ActivityClockQian::selQian(['ch_id' => $id], $page*$limit-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            $id = input('id');
            $this->assign('id', $id);
            return view();
        }
    }

    //商品多图上传
    public function shopUpload(){
        shopUpload('p_img');
    }

    //普通图片上传
    public function upload(){
        upload('poster');
    }

    //商品视频上传
    public function videoUpload(){
        upload('video');
    }

    //编辑器上传图片
    public function editorUpload(){
        editorUpload('editor');
    }

    //规格图片上传
    public function specUpload(){
        shopUpload('spec');
    }

}
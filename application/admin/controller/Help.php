<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\system\HelpCategory;
use app\admin\model\system\Help as Helps;

class Help extends Common{

    //类别
    public function help_category(){
        $list = HelpCategory::selHelpCategory();
        $this->assign('list', json_encode($list));
        return view();
    }

    //类别操作
    public function help_category_operate(){
        $id = input('id');  //id
        $up = input('up'); //上级id
        if ($id){
            $find = HelpCategory::getHelpCategoryInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        if ($up){
            $name = HelpCategory::getField(['id' => $up], 'name');
            $this->assign('up', $up);
            $this->assign('name', $name);
        }
        return view();
    }

    //添加分类
    public function help_category_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            if (empty($field['name'])){
                echo json_encode(array('info' => 0, 'msg' => '分类名称不能为空'));
                exit;
            }
            if ($field['sort'] === ''){
                echo json_encode(array('info' => 0, 'msg' => '排序不能为空'));
                exit;
            }elseif (!is_numeric($field['sort'])){
                echo json_encode(array('info' => 0, 'msg' => '排序只能为数字'));
                exit;
            }
            $data['name'] = $field['name'];
            $data['sort'] = $field['sort'];
            $data['pid'] = $field['pid'] ? $field['pid'] : 0;
            $data['status'] = $field['status'] ? $field['status'] : 0;
            $re = HelpCategory::add($data);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '添加失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //修改分类
    public function help_category_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            if (empty($field['name'])){
                echo json_encode(array('info' => 0, 'msg' => '分类名称不能为空'));
                exit;
            }
            if ($field['sort'] === ''){
                echo json_encode(array('info' => 0, 'msg' => '排序不能为空'));
                exit;
            }elseif (!is_numeric($field['sort'])){
                echo json_encode(array('info' => 0, 'msg' => '排序只能为数字'));
                exit;
            }
            $id = $field['id'];
            $data['name'] = $field['name'];
            $data['sort'] = $field['sort'];
            $data['status'] = $field['status'] ? $field['status'] : 0;
            $re = HelpCategory::edit(['id' => $id], $data);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '修改失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //删除分类
    public function help_category_del(){
        if (request()->isPost()){
            $id = input('id');
            if (!$id){
                echo json_encode(array('info' => 0, 'msg' => '数据错误'));
                exit;
            }
            $cun = HelpCategory::getHelpCategoryInfo(['pid' => $id]);
            if ($cun){
                echo json_encode(array('info' => 0, 'msg' => '存在下级分类不能删除'));
                exit;
            }
            $re = HelpCategory::del(['id' => $id]);
            if ($re){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '删除失败'));
            }
        }else{
            echo json_encode(array('info' => 0, 'msg' => '状态错误'));
        }
    }

    //修改排序
    public function sort_edit(){
        if (request()->isPost()){
            $id = input('id');
            $value = input('value');
            if (empty($id)){
                echo 0;
                exit;
            }
            if ($value === '' || !is_numeric($value)){
                echo 0;
                exit;
            }
            $re = HelpCategory::upField(['id' => $id], 'sort', $value);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改状态
    public function status_edit(){
        if (request()->isPost()){
            $id = input('id');
            $status = input('status');
            if (empty($id)){
                echo 0;
                exit;
            }
            if ($status == 1){
                $re = HelpCategory::upField(['id' => $id], 'status', 1);
            }elseif ($status == 0){
                $re = HelpCategory::upField(['id' => $id], 'status', 0);
            }else {
                $re = false;
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //帮助中心
    public function help(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $title = input('title');
            $cid = input('cid');
            if ($title){
                $where['title'] = ['like', "%$title%"];
            }
            if ($cid){
                //判断提交过来的分类有没有下级
                $all = HelpCategory::getField(['pid' => $cid], 'id', 1);
                array_unshift($all, $cid);
                $where['cid'] = ['in', $all];
            }
            $list = Helps::selHelp($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            //查询分类
            $cate = HelpCategory::selHelpCategory(['pid' => 0, 'status' => 1]); //1级分类
            $cate2 = HelpCategory::selHelpCategory(['pid' => ['neq', 0], 'status' => 1]); //下级级分类
            if ($cate) $cate = $cate->toArray();
            if ($cate2) $cate2 = $cate2->toArray();
            foreach ($cate as $k => $v){
                foreach ($cate2 as $key => $val){
                    if ($cate2[$key]['pid'] == $cate[$k]['id']){
                        $cate[$k]['lower'][] = $cate2[$key];
                    }
                }
            }
            $this->assign('cate', $cate);
            return view();
        }
    }

    //帮助操作
    public function help_operate(){
        $id = input('id');
        if ($id){
            $find = Helps::getHelpInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        //查询分类
        $cate = HelpCategory::selHelpCategory(['pid' => 0, 'status' => 1]); //1级分类
        $cate2 = HelpCategory::selHelpCategory(['pid' => ['neq', 0], 'status' => 1]); //下级级分类
        if ($cate) $cate = $cate->toArray();
        if ($cate2) $cate2 = $cate2->toArray();
        foreach ($cate as $k => $v){
            foreach ($cate2 as $key => $val){
                if ($cate2[$key]['pid'] == $cate[$k]['id']){
                    $cate[$k]['lower'][] = $cate2[$key];
                }
            }
        }
        $this->assign('cate', $cate);
        return view();
    }

    //添加内容
    public function help_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'cid' => $field['cid'],
                'title' => $field['title'],
                'img' => $field['img'],
                'jian' => $field['jian'],
                'content' => $field['content'],
                'time' => time(),
            ];
            $re = Helps::add($data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改内容
    public function help_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $data = [
                'cid' => $field['cid'],
                'title' => $field['title'],
                'img' => $field['img'],
                'jian' => $field['jian'],
                'content' => $field['content']
            ];
            $re = Helps::edit(['id' => $id], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //删除内容
    public function help_del(){
        if (request()->isPost()){
            $id = input('id');
            if (!$id){
                echo 0;
                exit;
            }
            $re = Helps::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //图片上传
    public function upload(){
        upload('help');
    }
}

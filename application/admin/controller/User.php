<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\store\UserCheck;
use app\admin\model\store\UserGoldRecord;
use app\admin\model\store\StoreOrder;
use app\admin\model\user\UserGrade;
use app\admin\model\user\User as Users;
use app\admin\model\user\UserRecord;
use app\admin\model\user\UserCarry;
use app\admin\model\user\UserLead;
use app\admin\model\user\UserPifa;
use app\admin\model\user\UserRole;
use app\admin\model\user\UserTrade;

class User extends Common{

    /**
     * 用户数据
     * @return \think\response\View
     */
    public function index(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $list = Users::selUserLimit($info, $name, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 用户信息
     * @return \think\response\View
     */
    public function user_operate(){
        $id = input('id');
        if ($id){
            $find = Users::getUserInfo(['id' => $id]);
            $find['name'] = base64_decode($find['name']);
            $this->assign('find', $find);
        }
        return view();
    }

    /**
     * 信息修改
     */
    public function user_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $find = Users::getUserInfo(['id' => $field['id']], 'balance, gold');
            $data = [
                'balance' => $field['balance_xuan'] == 1 ? $find['balance'] + $field['balance'] : $find['balance'] - $field['balance'],
                'gold' => $field['gold_xuan'] == 1 ? $find['gold'] + $field['gold'] : $find['gold'] - $field['gold'],
            ];
            $re = Users::edit(['id' => $field['id']], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 消费记录
     * @return \think\response\View
     */
    public function user_jilu(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $type = input('type');
            $id = input('id');
            switch ($type){
                case 1:
                    $list = StoreOrder::selOrderLimit(['uid' => $id, 'status' => ['neq', 0], 'type' => 0], $limit*$page-$limit, $limit);
                    echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
                    break;
                case 2:
                    $list = UserRecord::selRecordLimit(['uid' => $id, 'status' => 1], $limit*$page-$limit, $limit);
                    echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
                    break;
                case 3:
                    $list = StoreOrder::selOrderLimit(['uid' => $id, 'status' => ['neq', 0], 'type' => 1], $limit*$page-$limit, $limit);
                    echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
                    break;
                case 4:
                    echo json_encode(array('code' => 0, 'count' => 0, 'data' => []));
                    break;
                case 5:
                    $list = UserCheck::selCheckLimit(['uid' => $id, 'status' => 1], $limit*$page-$limit, $limit);
                    echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
                    break;
                case 6:
                    $list = UserGoldRecord::selGoldRecordLimit(['uid' => $id, 'status' => 1], $limit*$page-$limit, $limit);
                    echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
                    break;
            }
        }else{
            $id = input('id');
            $this->assign('id', $id);
            return view();
        }
    }

    /**
     * 下级会员
     * @return \think\response\View
     */
    public function lower(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $id = input('id');
            $list = Users::selUserZhiLimit(['upper' => $id], $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            $id = input('id');
            $this->assign('id', $id);
            return view();
        }
    }

    /**
     * 设置超级管理员
     */
    public function super(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            if ($type == 1){
                $re = UserRole::upField(['uid' => $id], 'super', 1);
            }elseif ($type == 2){
                $re = UserRole::upField(['uid' => $id], 'super', 0);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 设置批发会员
     */
    public function pf(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            if ($type == 1){
                $re = UserRole::upField(['uid' => $id], 'pf', 1);
            }elseif ($type == 2){
                $re = UserRole::upField(['uid' => $id], 'pf', 0);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 分销
     */
    public function retail(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            if ($type == 1){
                $re = UserRole::upField(['uid' => $id], 'retail', 1);
            }elseif ($type == 2){
                $re = UserRole::upField(['uid' => $id], 'retail', 0);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 禁用会员
     */
    public function jin(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            if ($type == 1){
                $re = Users::upField(['id' => $id], 'is_jin', 1);
            }elseif ($type == 2){
                $re = Users::upField(['id' => $id], 'is_jin', 0);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 会员等级管理页面
     * @return \think\response\View
     */
    public function user_grade_operate(){
        $id = input('id');
        if ($id){
            $find = Users::getUserInfo(['id' => $id], 'id, grade');
            $this->assign(compact('find'));
        }
        //查出所有会员等级
        $grade = UserGrade::selGrade(['status' => 1], 'name, grade');
        $this->assign(compact('grade'));
        return view();
    }

    /**
     * 会员等级修改
     */
    public function user_grade_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $data = [
            'grade' => $field['grade']
        ];
        $re = Users::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 会员等级
     * @return \think\response\View
     */
    public function user_grade(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $status = input('status');
            if ($status != '') $where['status'] = $status;
            $list = UserGrade::selGradeLimit($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 等级开关
     */
    public function grade_open(){
        $id = input('id');
        $field = input('field');
        $open = input('open');
        $re = UserGrade::upField(['id' => $id], $field, $open ? 1 : 0);
        if ($re){
            successCode($open ? '已开启' : '已关闭');
        }else{
            errorCode('失败');
        }
    }

    /**
     * 单个字段修改
     */
    public function grade_field(){
        $id = input('id');
        $field = input('field');
        $value = input('value');
        $re = UserGrade::upField(['id' => $id], $field, $value);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('失败');
        }
    }

    /**
     * 会员等级操作页面
     * @return \think\response\View
     */
    public function grade_operate(){
        $id = input('id');
        if ($id){
            $find = UserGrade::getGradeInfo(['id' => $id]);
            $this->assign(compact('find'));
        }
        return view();
    }

    /**
     * 会员等级添加
     */
    public function grade_add(){
        $field = input()['field'];
        $data = [
            'name' => $field['name'],
            'grade' => $field['grade'],
            'img' => $field['img'],
            'discount' => $field['discount'],
            'status' => $field['status'],
            'explain' => $field['explain'],
        ];
        $re = UserGrade::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 会员等级编辑
     */
    public function grade_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $data = [
            'name' => $field['name'],
            'grade' => $field['grade'],
            'img' => $field['img'],
            'discount' => $field['discount'],
            'status' => $field['status'],
            'explain' => $field['explain'],
        ];
        $re = UserGrade::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 用户等级删除
     */
    public function grade_del(){
        $id = input('id');
        $img = UserGrade::getField(['id' => $id], 'img');
        $re = UserGrade::del(['id' => $id]);
        if ($re){
            unlink('./uploads/' . $img);
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    /**
     * 业务员
     * @return \think\response\View
     */
    public function business(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $list = Users::selUserY($info, $name, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 添加业务员
     */
    public function business_add(){
        $img_url = 'uploads/business/business.png';
        if (file_exists($img_url)){
            echo json_encode(array('img' => $img_url));
        }else{
            $access_token = access_token();
            $scene = 'page:8';
            $img = createImg($access_token, $scene);
            file_put_contents($img_url, $img);
            //存储二维码路径
            echo json_encode(array('img' => $img_url));
        }
    }

    /**
     * 更新业务员二维码
     */
    public function business_edit(){
        $img_url = 'uploads/business/business.png';
        $access_token = access_token();
        $scene = 'type:1,upper:0,recruit:0';
        $img = createImg($access_token, $scene);
        $result = file_put_contents($img_url, $img);
        if ($result){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 礼品商家
     * @return \think\response\View
     */
    public function provide(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $list = Users::selUserG($info, $name, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 礼品商家审核操作
     * @return \think\response\View
     */
    public function provide_operate(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $status = input('status');
            if ($status != ''){
                $where['status'] = $status;
            }
            $nameId = Users::getField(['name' => ['like', "%$name%"]], 'id', 1);
            $list = UserCarry::selCarry($where, $info, $nameId, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 礼品商家审核
     * @return \think\response\View
     */
    public function provide_shenhe(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id']; //当前申请id
            $recruit = $field['recruit']; //招商人
            $phone = $field['phone']; //商家手机号
            $uid = $field['uid']; //商家
            $name = $field['name']; //商家名称
            $latitude = $field['latitude']; //纬度
            $longitude = $field['longitude']; //经度
            $address = $field['address']; //地址
            if (!$uid){
                echo json_encode(array('info' => 0, 'msg' => '数据错误'));
                exit;
            }
            //添加商家信息
            $data = [
                'uid' => $uid,
                'name' => $name,
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ];
            $find = UserTrade::getTradeInfo(['uid' => $uid]);
            if ($find){
                $re = UserTrade::edit(['id' => $find['id']], $data);
            }else{
                $re = UserTrade::add($data);
            }
            //成功后申请数据的状态
            if ($re){
                //修改当前商家的账户属性和上级招商员
                Users::edit(['id' => $uid], ['recruit' => $recruit, 'phone' => $phone]);
                UserRole::upField(['uid' => $uid], 'gift', 1);
                //修改当前审核状态
                UserCarry::upField(['id' => $id], 'status', 1);
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '审核失败'));
            }
        }else{
            $id = input('id');
            $find = UserCarry::getCarryInfo(['id' => $id]);
            $this->assign('find', $find);
            return view();
        }
    }

    /**
     * 拒绝通过
     */
    public function provide_jujue(){
        if (request()->isPost()){
            $id = input('id');
            if (!$id){
                echo 0;
                exit;
            }
            $re = UserCarry::upField(['id' => $id], 'status', 2);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 礼品商家信息
     * @return \think\response\View
     */
    public function provide_xinxi(){
        $id = input('id');
        if ($id){
            $find = UserTrade::getTradeInfo(['uid' => $id]);
            $this->assign('find', $find);
            $this->assign('uid', $id);
        }
        return view();
    }

    /**
     * 信息修改
     */
    public function xinxi_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $dizhi = explode(',', $field['dingwei']);
            $data = [
                'uid' => $field['uid'],
                'name' => $field['name'],
                'address' => $field['address'],
                'latitude' => $dizhi[0],
                'longitude' => $dizhi[1],
            ];
            $find = UserTrade::getTradeInfo(['uid' => $field['id']]);
            if ($find){
                $re = UserTrade::edit(['uid' => $field['id']], $data);
            }else{
                $re = UserTrade::add($data);
            }
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 地图
     * @return \think\response\View
     */
    public function ditu(){
        return view();
    }

    /**
     * 引流商家
     * @return \think\response\View
     */
    public function drainage(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $list = Users::selUserD($info, $name, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 引流商家审核操作
     * @return \think\response\View
     */
    public function drainage_operate(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $status = input('status');
            if ($status != ''){
                $where['status'] = $status;
            }
            $nameId = Users::getField(['name' => ['like', "%$name%"]], 'id', 1);
            $list = UserLead::selLead($where, $info, $nameId, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 引流商家审核
     * @return \think\response\View
     */
    public function drainage_shenhe(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id']; //当前申请id
            $recruit = $field['recruit']; //招商人
            $phone = $field['phone']; //商家手机号
            $uid = $field['uid']; //商家
            $name = $field['name']; //商家名称
            $latitude = $field['latitude']; //纬度
            $longitude = $field['longitude']; //经度
            $address = $field['address']; //地址
            if (!$uid){
                echo json_encode(array('info' => 0, 'msg' => '数据错误'));
                exit;
            }
            //添加商家信息
            $data = [
                'uid' => $uid,
                'name' => $name,
                'address' => $address,
                'latitude' => $latitude,
                'longitude' => $longitude,
            ];
            $find = UserTrade::getTradeInfo(['uid' => $uid]);
            if ($find){
                $re = UserTrade::edit(['id' => $find['id']], $data);
            }else{
                $re = UserTrade::add($data);
            }
            //成功后申请数据的状态
            if ($re){
                //修改当前商家的账户属性和上级招商员
                Users::edit(['id' => $uid], ['recruit' => $recruit, 'phone' => $phone]);
                UserRole::upField(['uid' => $uid], 'drainage', 1);
                //修改当前审核状态
                UserLead::upField(['id' => $id], 'status', 1);
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '审核失败'));
            }
        }else{
            $id = input('id');
            $find = UserLead::getLeadInfo(['id' => $id]);
            $this->assign('find', $find);
            return view();
        }
    }

    /**
     * 引流商家拒绝
     */
    public function drainage_jujue(){
        $id = input('id');
        if (!$id){
            echo 0;
            exit;
        }
        $re = UserCarry::upField(['id' => $id], 'status', 2);
        if ($re){
            echo 1;
        }else{
            echo 0;
        }
    }

    /**
     * 批发会员
     * @return \think\response\View
     */
    public function pifa(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $list = Users::selUserP($info, $name, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 批发会员审核操作
     * @return \think\response\View
     */
    public function pifa_operate(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_encode($info);
            $status = input('status');
            if ($status != ''){
                $where['status'] = $status;
            }
            $nameId = Users::getField(['name' => ['like', "%$name%"]], 'id', 1);
            $list = UserPifa::selPifa($where, $info, $nameId, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 批发会员审核
     */
    public function pifa_shenhe(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            $find = db('pifa')->where('id', $id)->find();
            if ($find){
                if ($type == 1){
                    $re = UserPifa::upField(['id' => $id], 'status', 1);
                    Users::upField(['id' => $find['uid']], 'phone', $find['phone']);
                    UserRole::upField(['uid' => $find['uid']], 'pf', 1);
                }else{
                    $re = UserPifa::upField(['id' => $id], 'status', 2);
                }
                if ($re){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 图片上传
     */
    public function upload(){
        upload('lipin');
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\activity\Activity;
use app\admin\model\store\StoreProduct;
use app\admin\model\store\StoreProductCoupon;
use app\admin\model\system\Ad;
use app\admin\model\system\Banner;
use app\admin\model\system\ContentPage;
use app\admin\model\system\Foot;
use app\admin\model\system\Link;
use app\admin\model\system\Message;
use app\admin\model\system\Page;
use app\admin\model\system\Popup;
use app\admin\model\system\Secret;
use app\admin\model\system\Site;
use app\admin\model\system\Template;

class System extends Common{

    //系统设置
    public function site(){
        if (request()->isPost()){
            $field = input()['field'];
            $type = input('type');
            if ($type == 1){
                $data = [
                    'title' => $field['title'],
                    'gong' => $field['gong'],
                    'ad' => $field['ad'],
                    'tel' => $field['tel'],
                    'open' => $field['open'],
                ];
            }elseif ($type == 2){
                $data = [
                    'retail_open' => $field['retail_open'] ? 1 : 0,
                    'retail_type' => $field['retail_type'],
                    'tui1' => $field['tui1'],
                    'tui2' => $field['tui2'],
                    'tui3' => $field['tui3'],
                    'color' => $field['color'],
                ];
            }elseif ($type == 3){
                $data = [
                    'ratio' => $field['ratio'],
                    'reward' => $field['reward'],
                    'reward2' => $field['reward2'],
                    'give' => $field['give'],
                    'cashback' => $field['cashback'],
                    'is_fei' => $field['is_fei'] ? 1 : 0,
                ];
            }elseif ($type == 4){
                $data = [
                    'huo' => $field['huo'],
                    'yin' => $field['yin'],
                ];
            }
            $result = Site::edit($data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $site = Site::getSiteInfo();
            $this->assign('site', $site);
            return view();
        }
    }

    //banner管理
    public function banner(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Banner::selBanner($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //banner操作
    public function banner_operate(){
        $id = input('id');
        if ($id){
            $find = Banner::getBannerInfo(['id' => $id]);
            $find['name'] = StoreProduct::getField(['id' => $find['link']], 'name');
            $this->assign('find', $find);
        }
        return view();
    }

    //banner添加
    public function banner_add(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Banner::add($arr);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //banner修改
    public function banner_edit(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $img = Banner::getField(['id' => $data['id']], 'img');
            $arr['id'] = $data['id'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Banner::edit(['id' => $arr['id']], $arr);
            if ($result){
                if ($img != $arr['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //banner删除
    public function banner_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Banner::getField(['id' => $id], 'img');
            $result = Banner::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改banner排序
    public function sort_edit(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Banner::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 页面管理
     * @return \think\response\View
     */
    public function content_page(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = ContentPage::selPage('', $limit * $page - $limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 页面操作
     * @return \think\response\View
     */
    public function content_page_operate(){
        $id = input('id');
        if ($id){
            $find = ContentPage::getPageInfo(['id' => $id]);
            $find['coupon_name'] = StoreProductCoupon::getField(['id' => $find['coupon']], 'name');
            $find['shop_name'] = implode(',', StoreProduct::getField(['id' => ['in', explode(',', $find['shop'])]], 'name', 1));
            $this->assign(compact('find'));
        }
        $popup = Popup::selPopup('', 'id, name');
        $this->assign(compact('popup'));
        return view();
    }

    /**
     * 页面添加
     */
    public function content_page_add(){
        $field = input()['field'];
        $data = [
            'name' => $field['name'],
            'background_image' => $field['background_image'],
            'background_color' => $field['background_color'],
            'form' => $field['form'],
            'type' => $field['type'],
            'coupon' => $field['coupon'],
            'num' => $field['num'],
            'content' => $field['content'],
            'shop' => $field['shop'],
            'enter_popup_open' => $field['enter_popup_open'] ? 1 : 0,
            'enter_popup_page' => $field['enter_popup_page'],
            'browse_popup_open' => $field['browse_popup_open'] ? 1 : 0,
            'browse_popup_miao' => $field['browse_popup_miao'],
            'browse_popup_page' => $field['browse_popup_page'],
            'rule' => $field['rule'],
        ];
        $re = ContentPage::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 页面修改
     */
    public function content_page_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $data = [
            'name' => $field['name'],
            'background_image' => $field['background_image'],
            'background_color' => $field['background_color'],
            'form' => $field['form'],
            'type' => $field['type'],
            'coupon' => $field['coupon'],
            'num' => $field['num'],
            'content' => $field['content'],
            'shop' => $field['shop'],
            'enter_popup_open' => $field['enter_popup_open'] ? 1 : 0,
            'enter_popup_page' => $field['enter_popup_page'],
            'browse_popup_open' => $field['browse_popup_open'] ? 1 : 0,
            'browse_popup_miao' => $field['browse_popup_miao'],
            'browse_popup_page' => $field['browse_popup_page'],
            'rule' => $field['rule'],
        ];
        $re = ContentPage::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 删除页面
     */
    public function content_page_del(){
        $id = input('id');
        $re = ContentPage::del(['id' => $id]);
        if ($re){
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    /**
     * 广告列表
     * @return \think\response\View
     */
    public function ad(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Ad::selAd($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 广告操作
     * @return \think\response\View
     */
    public function ad_operate(){
        $id = input('id');
        if ($id){
            $find = Ad::getAdInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    /**
     * 广告添加
     */
    public function ad_add(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Ad::add($arr);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 广告修改
     */
    public function ad_edit(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $img = Ad::getField(['id' => $data['id']], 'img');
            $arr['id'] = $data['id'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Ad::edit(['id' => $arr['id']], $arr);
            if ($result){
                if ($img != $arr['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 广告删除
     */
    public function ad_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Ad::getField(['id' => $id], 'img');
            $result = Ad::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 修改广告排序
     */
    public function ad_sort(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Ad::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 推送消息列表
     * @return \think\response\View
     */
    public function message(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Message::selMessage($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 推送消息操作
     * @return \think\response\View
     */
    public function message_operate(){
        $id = input('id');
        if ($id){
            $find = Message::getMessageInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    /**
     * 添加推送消息
     */
    public function message_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'title' => $field['title'],
                'img' => $field['img'],
                'describe' => $field['describe'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'obj' => $field['obj'],
                'user' => $field['remarks'],
            ];
            $result = Message::add($data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 修改推送消息
     */
    public function message_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $id = $field['id'];
            $img = Message::getField(['id' => $id], 'img');
            $data = [
                'title' => $field['title'],
                'img' => $field['img'],
                'describe' => $field['describe'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'obj' => $field['obj'],
                'user' => $field['remarks'],
            ];
            $result = Message::edit(['id' => $id], $data);
            if ($result){
                if ($img != $field['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 推送消息
     */
    public function message_send(){
        if (request()->isPost()){
            $id = input('id');
            $data = [
                'status' => 1,
                'time' => time(),
            ];
            $result = Message::edit(['id' => $id], $data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 推送消息删除
     */
    public function message_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Message::getField(['id' => $id], 'img');
            $result = Message::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 弹窗列表
     * @return \think\response\View
     */
    public function popup(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Popup::selPopupLimit($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 弹窗操作
     * @return \think\response\View
     */
    public function popup_operate(){
        $id = input('id');
        if ($id){
            $find = Popup::getPopupInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    /**
     * 弹窗添加
     */
    public function popup_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $data = [
                'name' => $field['name'],
                'img' => $field['img'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'object' => $field['object'],
                'rate' => $field['rate'],
                'remarks' => $field['remarks'],
                'status' => $field['status'],
            ];
            $result = Popup::add($data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 弹窗修改
     */
    public function popup_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $img = Popup::getField(['id' => $field['id']], 'img');
            $id = $field['id'];
            $data = [
                'name' => $field['name'],
                'img' => $field['img'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'object' => $field['object'],
                'rate' => $field['rate'],
                'remarks' => $field['remarks'],
                'status' => $field['status'],
            ];
            $result = Popup::edit(['id' => $id], $data);
            if ($result){
                if ($img != $field['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    /**
     * 弹窗删除
     */
    public function popup_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Popup::getField(['id' => $id], 'img');
            $result = Popup::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    /**
     * 修改弹窗排序
     */
    public function popup_sort(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Ad::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //新品上新
    public function foot(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Foot::selFoot($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //新品操作
    public function foot_operate(){
        $id = input('id');
        if ($id){
            $find = Foot::getFootInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    //新品添加
    public function foot_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $data = [
                'name' => $field['name'],
                'type' => $field['type'],
                'bg' => $field['bg'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'model' => $field['model'],
                'num' => $field['num'],
                'sort' => $field['sort'],
                'status' => $field['status'],
            ];
            $result = Foot::add($data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //新品修改
    public function foot_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $id = $field['id'];
            $data = [
                'name' => $field['name'],
                'type' => $field['type'],
                'bg' => $field['bg'],
                'link' => $field['link'],
                'link_column' => $field['link_column'],
                'model' => $field['model'],
                'num' => $field['num'],
                'sort' => $field['sort'],
                'status' => $field['status'],
            ];
            $img = Foot::getField(['id' => $id], 'bg');
            $result = Foot::edit(['id' => $id], $data);
            if ($result){
                if ($img != $data['bg']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //foot玩法删除
    public function foot_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Foot::getField(['id' => $id], 'bg');
            $result = Foot::del(['id' => $id]);
            if ($result){
                $bg = './uploads/' . $img;
                unlink($bg);
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //修改foot玩法排序
    public function foot_sort(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Foot::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //link管理
    public function link(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Link::selLink($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //link操作
    public function link_operate(){
        $id = input('id');
        if ($id){
            $find = Link::getLinkInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    //link添加
    public function link_add(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $arr['img'] = $data['img'];
            $arr['title'] = $data['title'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Link::add($arr);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //link修改
    public function link_edit(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $img = Link::getField(['id' => $data['id']], 'img');
            $arr['id'] = $data['id'];
            $arr['img'] = $data['img'];
            $arr['title'] = $data['title'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Link::edit(['id' => $arr['id']], $arr);
            if ($result){
                if ($img != $arr['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //link删除
    public function link_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Link::getField(['id' => $id], 'img');
            $result = Link::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改link排序
    public function link_sort(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Link::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //活动管理
    public function activity(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Activity::selActivity($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //活动操作
    public function activity_operate(){
        $id = input('id');
        if ($id){
            $find = Activity::getActivityInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    //活动添加
    public function activity_add(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $arr['title'] = $data['title'];
            $arr['synopsis'] = $data['synopsis'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Activity::add($arr);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //活动修改
    public function activity_edit(){
        if (request()->isPost()){
            $data = input('post.')['field'];
            $img = Activity::getField(['id' => $data['id']], 'img');
            $arr['id'] = $data['id'];
            $arr['title'] = $data['title'];
            $arr['synopsis'] = $data['synopsis'];
            $arr['img'] = $data['img'];
            $arr['link'] = $data['link'];
            $arr['link_column'] = $data['link_column'];
            $arr['sort'] = $data['sort'];
            $arr['status'] = $data['status'];
            $result = Activity::edit(['id' => $arr['id']], $arr);
            if ($result){
                if ($img != $arr['img']){
                    $imgs = './uploads/' . $img;
                    unlink($imgs);
                }
                echo 1;
            }else{
                echo 0;
            }
        }
    }

    //活动删除
    public function activity_del(){
        if (request()->isPost()){
            $id = input('id');
            $img = Activity::getField(['id' => $id], 'img');
            $result = Activity::del(['id' => $id]);
            if ($result){
                $imgs = './uploads/' . $img;
                unlink($imgs);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改活动排序
    public function activity_sort(){
        if (request()->isPost()){
            $id = input('id');
            $field = input('field');
            $value = input('value');
            $result = Activity::upField(['id' => $id], $field, $value);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //微信支付设置
    public function secret(){
        if (request()->isPost()){
            $field = input()['field'];
            $appid = $field['appid'];
            $secret_key = $field['secret_key'];
            $merchant = $field['merchant'];
            $merchant_key = $field['merchant_key'];
            $certificate = $field['certificate'];
            $certificate_key = $field['certificate_key'];
            if (empty($appid) || empty($merchant) || empty($secret_key) || empty($merchant_key)){
                echo 0;
                exit;
            }
            $data = [
                'appid' => $appid,
                'secret_key' => $secret_key,
                'merchant' => $merchant,
                'merchant_key' => $merchant_key,
                'certificate' => $certificate,
                'certificate_key' => $certificate_key,
            ];
            $yuan = Secret::getSecretInfo('certificate, certificate_key');
            $re = Secret::edit($data);
            if ($re){
                if (file_exists($yuan['certificate']) && $yuan['certificate'] != $certificate)
                    unlink('./' .$yuan['certificate']);
                if (file_exists($yuan['certificate_key']) && $yuan['certificate_key'] != $certificate_key)
                    unlink('./' .$yuan['certificate_key']);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            $find = Secret::getSecretInfo();
            $this->assign('find', $find);
            return view();
        }
    }

    //模板消息
    public function template(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Template::selTemplate($limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //模板操作
    public function template_operate(){
        $id = input('id');
        if ($id){
            $find = Template::getTemplateInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        return view();
    }

    //模板添加
    public function template_add(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'template_id' => $field['template_id'],
                'number' => $field['number'],
                'name' => $field['name'],
                'content' => $field['content'],
                'status' => $field['status'],
                'time' => time()
            ];
            $result = Template::add($data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //模板修改
    public function template_edit(){
        if (request()->isPost()){
            $field = input()['field'];
            $data = [
                'template_id' => $field['template_id'],
                'number' => $field['number'],
                'name' => $field['name'],
                'content' => $field['content'],
                'status' => $field['status']
            ];
            $result = Template::edit(['id' => $field['id']], $data);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //模板删除
    public function template_del(){
        if (request()->isPost()){
            $id = input('id');
            $result = Template::del(['id' => $id]);
            if ($result){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }


    //图片上传
    public function upload(){
        upload('site');
    }

    /**
     * 证书上传
     */
    public function certificate(){
        certificate('static/cert', 'uniqid');
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\user\UserCoupon;
use app\admin\model\user\UserGold;
use app\admin\model\user\UserRecord;
use app\admin\model\user\UserCash;
use app\admin\model\store\StoreProduct;
use app\admin\model\store\StoreOrder;

class Record extends Common{

    /**
     * 商品信息
     * @return \think\response\View
     */
    public function product_statistics(){
        //查出商品信息
        $status = input('status');
        $dateTime = input('dateTime');
        $data = StoreProduct::selProductECharts($status, $dateTime);
        $this->assign([
            'status' => $status,
            'dateTime' => $dateTime,
            'count' => $data['count'],
            'ranking' => $data['ranking'],
            'data' => json_encode($data['arr']),
        ]);
        return view();
    }

    /**
     * 订单统计
     * @return \think\response\View
     */
    public function order_statistics(){
        //查出商品信息
        $order = input('order'); //订单状态
        $status = input('status'); //时间状态
        $dateTime = input('dateTime'); //指定时间
        $data = StoreOrder::selOrderECharts($order, $status, $dateTime);
        $this->assign([
            'order' => $order,
            'status' => $status,
            'dateTime' => $dateTime,
            'count' => $data['count'],
            'data' => json_encode($data['arr']),
        ]);
        return view();
    }

    /**
     * 生成订单
     */
    public function order_excel(){
        $order = input('order'); //订单状态
        $status = input('status'); //时间状态
        $dateTime = input('dateTime'); //指定时间
        //导出excel
        //表头名称
        $headerName = ['订单号', '消费用户', '订单金额', '商品数量', '支付状态', '订单时间'];
        //保存文件名称
        $fileName = '订单_' . date('YmdH_i_s') . '.xlsx';
        //二维数组
        $data = StoreOrder::selOrderExcel($order, $status, $dateTime);
        exportExcel($headerName, $data, '订单信息', $fileName);
    }

    /**
     * 优惠券统计
     * @return \think\response\View
     */
    public function coupon_statistics(){
        $status = input('status'); //时间状态
        $dateTime = input('dateTime'); //指定时间
        $data = UserCoupon::selCouponECharts($status, $dateTime);
        $this->assign([
            'status' => $status,
            'dateTime' => $dateTime,
            'count' => $data['count'],
            'data' => json_encode($data['arr']),
        ]);
        return view();
    }

    /**
     * 数据报表
     * @return \think\response\View
     */
    public function report_forms(){
        //财务报表
        $status = input('status'); //时间状态
        $dateTime = input('dateTime'); //指定时间
        $data = StoreOrder::selOrderECharts2($status, $dateTime);
        $this->assign([
            'status' => $status,
            'dateTime' => $dateTime,
            'count' => $data['count'],
            'toDay' => $data['toDay'],
            'data' => json_encode($data['arr']),
        ]);
        return view();
    }

    /**
     * 提现统计
     * @return \think\response\View
     */
    public function cash_statistics(){
        //提现人数,笔数,金额
        $record = UserCash::countWholeCash();
        //充值笔数
        $status = input('status');
        $dateTime = input('dateTime');
        $data = UserCash::selCashECharts($status, $dateTime);
        $this->assign([
            'record' => $record,
            'status' => $status,
            'dateTime' => $dateTime,
            'data' => json_encode($data),
        ]);
        return view();
    }

    /**
     * 充值统计
     * @return \think\response\View
     */
    public function recharge_statistics(){
        //充值笔数
        $status = input('status');
        $dateTime = input('dateTime');
        $data = UserRecord::selRecordECharts($status, $dateTime);
        $this->assign([
            'status' => $status,
            'dateTime' => $dateTime,
            'data' => json_encode($data),
        ]);
        return view();
    }

    /**
     * 返佣统计
     * @return \think\response\View
     */
    public function anti_statistics(){
        if (request()->isPost()){
            //返佣记录
            $page = input('page');
            $limit = input('limit');
            $status = input('status');
            $dateTime = input('dateTime');
            $list = UserGold::selUserGold($status, $dateTime, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            //返佣数据
            $status = input('status');
            $dateTime = input('dateTime');
            $data = UserGold::countGold($status, $dateTime);
            $this->assign([
                'status' => $status,
                'dateTime' => $dateTime,
                'record' => $data['record'],
                'data' => json_encode($data['arr']),
            ]);
            return view();
        }
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\model\admin\Account;
use think\Controller;
class Login extends Controller{

    //登录
    public function login(){
        if (request()->isPost()){
            $field = input()['field'];
            $username = $field['username'];
            $password = $field['password'];
            $code = $field['code'];
            //验证码验证
            if(!captcha_check($code)) errorCode('验证码错误');
            if (!$username) errorCode('请输入用户名');
            if (!$password) errorCode('请输入密码');
            //查询账号

            $find = Account::getAccountInfo(['username' => $username], '');
            if (!$find) errorCode('账号不存在');
            if (md5($password) != $find['password']) errorCode('密码错误');
            session('NAME', $find['username']);
            session('TIME', time());
            successCode('登录成功');
        }else{
            return view();
        }
    }

    //退出登录
    public function logoutt(){
        if (request()->isPost()){
            if (session(null) === null){
                echo json_encode(array('info' => 1));
            }else{
                echo json_encode(array('info' => 0, 'msg' => '请刷新重试'));
            }
        }
    }
}
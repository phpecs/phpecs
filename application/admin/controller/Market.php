<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\model\market\MarketBack;
use app\admin\model\market\MarketBargain;
use app\admin\model\market\MarketLikes;
use app\admin\model\market\MarketPurchase;
use app\admin\model\market\MarketSeckill;
use app\admin\model\market\MarketSeckillTime;
use app\admin\model\market\MarketSpell;
use app\admin\model\store\StoreProduct;
use app\admin\model\store\StoreOrder;
use app\admin\model\user\User;

class Market extends Common{

    /**
     * 拼团活动
     * @return \think\response\View
     */
    public function spell(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $name = input('name');
            $status = input('status');
            if ($name) $where['name'] = ['like', "%$name%"];
            if ($status != '') $where['status'] = $status;
            $list = MarketSpell::selSpellLimit($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 拼团活动开关
     */
    public function spell_open(){
        $id = input('id');
        $field = input('field');
        $open = input('open');
        $re = MarketSpell::upField(['id' => $id], $field, $open ? 1 : 0);
        if ($re){
            successCode($open ? '已开启' : '已关闭');
        }else{
            errorCode('失败');
        }
    }

    /**
     * 拼团活动操作页面
     * @return \think\response\View
     */
    public function spell_operate(){
        $id = input('id');
        $pid = input('pid'); //商品id
        if ($id){
            $find = MarketSpell::getSpellInfo(['id' => $id]);
            $spell_time = explode(' ', gmstrftime('%j %H %M %S', $find['spell_time']));
            $find['days'] = $spell_time[0] - 1;
            $find['hours'] = $spell_time[1];
            $find['minutes'] = $spell_time[2];
            $find['seconds'] = $spell_time[3];
            $this->assign(compact('find'));
        }
        if ($pid){
            $pro = StoreProduct::getProductInfo(['id' => $pid], 'id, name, price original_cost');
            $this->assign(compact('pro'));
        }
        return view();
    }

    /**
     * 拼团活动添加
     */
    public function spell_add(){
        $field = input()['field'];
        $time = explode(' - ', $field['time']);
        $data = [
            'name' => $field['name'],
            'pid' => $field['pid'],
            'start_time' => strtotime($time[0]),
            'end_time' => strtotime($time[1]),
            'stock' => $field['stock'],
            'volume' => $field['volume'],
            'original_cost' => $field['original_cost'],
            'leader_price' => $field['leader_price'],
            'spell_price' => $field['spell_price'],
            'spell_number' => $field['spell_number'],
            'spell_time' => $field['days'] * 86400 + $field['hours'] * 3600 + $field['minutes'] * 60 + $field['seconds'],
            'postage_open' => $field['postage_open'] ? 1 : 0,
            'postage' => $field['postage_open'] == 0 ? $field['postage'] : 0,
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSpell::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 拼团活动编辑
     */
    public function spell_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $time = explode(' - ', $field['time']);
        $data = [
            'name' => $field['name'],
            'start_time' => strtotime($time[0]),
            'end_time' => strtotime($time[1]),
            'stock' => $field['stock'],
            'volume' => $field['volume'],
            'original_cost' => $field['original_cost'],
            'leader_price' => $field['leader_price'],
            'spell_price' => $field['spell_price'],
            'spell_number' => $field['spell_number'],
            'spell_time' => $field['days'] * 86400 + $field['hours'] * 3600 + $field['minutes'] * 60 + $field['seconds'],
            'postage_open' => $field['postage_open'] ? 1 : 0,
            'postage' => $field['postage_open'] == 0 ? $field['postage'] : 0,
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSpell::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 拼团活动删除
     */
    public function spell_del(){
        $id = input('id');
        $re = MarketSpell::del(['id' => $id]);
        if ($re){
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    /**
     * 拼团订单数据
     * @return \think\response\View
     */
    public function spell_order(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $time = input('time');
            $status = input('status');
            if ($time){
                $time = explode(' - ', $time);
                $where['o.time'] = [['egt', strtotime($time[0])], ['elt', strtotime($time[1])]];
            }
            $where['o.type'] = 3;
            $where['o.status'] = 1;
            $list = StoreOrder::selSpellOrderLimit($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 拼团订单数据
     * @return \think\response\View
     */
    public function spell_order_operate(){
        $spell_number = input('spell_number');
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $ids = StoreOrder::getField(['spell_number' => $spell_number, 'type' => 3], 'uid', 1);
            $list = User::selUserZhiLimit(['id' => ['in', $ids]], $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            $this->assign(compact('spell_number'));
            return view();
        }
    }

    /**
     * 秒杀活动
     * @return \think\response\View
     */
    public function seckill(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $name = input('name');
            $status = input('status');
            if ($name) $where['name'] = ['like', "%$name%"];
            if ($status != '') $where['status'] = $status;
            $list = MarketSeckill::selSeckillLimit($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 秒杀活动开关
     */
    public function seckill_open(){
        $id = input('id');
        $field = input('field');
        $open = input('open');
        $re = MarketSeckill::upField(['id' => $id], $field, $open ? 1 : 0);
        if ($re){
            successCode($open ? '已开启' : '已关闭');
        }else{
            errorCode('失败');
        }
    }

    /**
     * 秒杀活动操作页面
     * @return \think\response\View
     */
    public function seckill_operate(){
        $id = input('id');
        $pid = input('pid'); //商品id
        if ($id){
            $find = MarketSeckill::getSeckillInfo(['id' => $id]);
        }
        if ($pid){
            $pro = StoreProduct::getProductInfo(['id' => $pid], 'id, name, price original_cost');
        }
        $time_slot = MarketSeckillTime::selTime(['status' => 1]);
        foreach ($time_slot as $k => $v){
            $time_slot[$k]['duan'] = $v['start_time'] . '~' . ($v['start_time'] + $v['continue_time'] >= 24 ? $v['start_time'] + $v['continue_time'] - 24 : $v['start_time'] + $v['continue_time']);
        }
        $this->assign(compact('find', 'pro', 'time_slot'));
        return view();
    }

    /**
     * 秒杀活动添加
     */
    public function seckill_add(){
        $field = input()['field'];
        $time = explode(' - ', $field['time']);
        $data = [
            'name' => $field['name'],
            'pid' => $field['pid'],
            'start_time' => strtotime($time[0]),
            'end_time' => strtotime($time[1]),
            'time_slot' => $field['time_slot'],
            'original_cost' => $field['original_cost'],
            'price_spike' => $field['price_spike'],
            'stock' => $field['stock'],
            'volume' => $field['volume'],
            'once_num' => $field['once_num'],
            'postage_open' => $field['postage_open'] ? 1 : 0,
            'postage' => $field['postage_open'] == 0 ? $field['postage'] : 0,
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSeckill::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 秒杀活动编辑
     */
    public function seckill_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $time = explode(' - ', $field['time']);
        $data = [
            'name' => $field['name'],
            'start_time' => strtotime($time[0]),
            'end_time' => strtotime($time[1]),
            'time_slot' => $field['time_slot'],
            'original_cost' => $field['original_cost'],
            'price_spike' => $field['price_spike'],
            'stock' => $field['stock'],
            'volume' => $field['volume'],
            'once_num' => $field['once_num'],
            'postage_open' => $field['postage_open'] ? 1 : 0,
            'postage' => $field['postage_open'] == 0 ? $field['postage'] : 0,
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSeckill::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 秒杀活动删除
     */
    public function seckill_del(){
        $id = input('id');
        $re = MarketSeckill::del(['id' => $id]);
        if ($re){
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }

    /**
     * 秒杀时间段设置
     * @return \think\response\View
     */
    public function seckill_time(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $status = input('status');
            if ($status != '') $where['status'] = $status;
            $list = MarketSeckillTime::selTimeLimit($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    /**
     * 秒杀时间段开关
     */
    public function seckill_time_open(){
        $id = input('id');
        $field = input('field');
        $open = input('open');
        $re = MarketSeckillTime::upField(['id' => $id], $field, $open ? 1 : 0);
        if ($re){
            successCode($open ? '已开启' : '已关闭');
        }else{
            errorCode('失败');
        }
    }

    /**
     * 秒杀时间段操作页面
     * @return \think\response\View
     */
    public function seckill_time_operate(){
        $id = input('id');
        if ($id){
            $find = MarketSeckillTime::getTimeInfo(['id' => $id]);
            $this->assign(compact('find'));
        }
        return view();
    }

    /**
     * 秒杀时间段添加
     */
    public function seckill_time_add(){
        $field = input()['field'];
        $data = [
            'start_time' => $field['start_time'],
            'continue_time' => $field['continue_time'],
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSeckillTime::add($data);
        if ($re){
            successCode('添加成功');
        }else{
            errorCode('添加失败');
        }
    }

    /**
     * 秒杀时间段编辑
     */
    public function seckill_time_edit(){
        $field = input()['field'];
        $id = $field['id'];
        $data = [
            'start_time' => $field['start_time'],
            'continue_time' => $field['continue_time'],
            'status' => $field['status'] ? 1 : 0,
        ];
        $re = MarketSeckillTime::edit(['id' => $id], $data);
        if ($re){
            successCode('修改成功');
        }else{
            errorCode('修改失败');
        }
    }

    /**
     * 秒杀时间段删除
     */
    public function seckill_time_del(){
        $id = input('id');
        $re = MarketSeckillTime::del(['id' => $id]);
        if ($re){
            successCode('删除成功');
        }else{
            errorCode('删除失败');
        }
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\model\activity\ActivityClock;
use app\admin\model\admin\Account;
use app\admin\model\admin\AccountPower;
use app\admin\model\admin\AccountRole;
use app\admin\model\store\StoreProduct;
use app\admin\model\store\StoreProductCoupon;
use app\admin\model\system\ContentPage;
use app\admin\model\system\Help;
use app\admin\model\system\Page;
use app\admin\model\system\Site;
use think\Controller;
class Common extends Controller{

    /**
     * 构造方法
     */
    public function __construct(){
        //引入父类构造方法
        parent::__construct();
        if (!session('NAME')){
            $this->redirect('Login/login');
        }
        $this->info();
        $this->site_info();
    }

    /**
     * 小程序页面链接
     */
    public function page_links(){
        $page = Page::selPage('', 'sort asc');
        successCode($page);
    }

    /**
     * 页面数据
     */
    public function page_list(){
        $type = input('type');
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $list = '';
            switch ($type){
                case 1:
                    $where['name'] = ['like', "%$info%"];
                    $list = StoreProduct::selProductZhi($where, '', $limit*$page-$limit, $limit);
                    break;
                case 2:
                    $where['title'] = ['like', "%$info%"];
                    $list = Help::selHelp($where, $limit*$page-$limit, $limit);
                    break;
                case 3:
                    $where['name'] = ['like', "%$info%"];
                    $list = ContentPage::selPage('', $limit * $page - $limit, $limit);
                    break;
                case 4:
                    $where['name'] = ['like', "%$info%"];
                    $list = ActivityClock::selClock($where, $limit*$page-$limit, $limit);
                    break;
                case 5:
                    $where['name'] = ['like', "%$info%"];
                    $list = StoreProductCoupon::selCoupon($where, $limit*$page-$limit, $limit);
                    break;
            }
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            $this->assign(compact('type'));
            return view();
        }
    }

    /**
     * 查询系统信息
     */
    public function site_info(){
        $site = Site::getSiteInfo('title');
        $this->assign('site', $site);
    }

    /**
     * 判断用户是否有权限
     */
    public function info(){
        $p = explode('/', $_SERVER['PATH_INFO']);
        $path = $p[2] . '/' . $p[3];
        if (!strpos($path, '.')){
            $path = $path . '.html';
        }
        //查出当前地址的id
        $power_id = AccountPower::getField(['power_url' => $path], 'id');
        //查出当前用户的所有角色
        $role = Account::getField(['username' => session('NAME')], 'role');
        $roleArr = explode(',', $role);
        foreach ($roleArr as $k => $v){
            //查出当前角色的所有权限
            $rp = AccountRole::getField(['id' => $v], 'power');
            $rpArr = explode(',', $rp);
            foreach ($rpArr as $key => $val){
                if ($val == $power_id || session('NAME') == 'admin'){
                    $re = 1;
                }
            }
        }
        if ($re != 1){
            echo '<script>alert("你没有此权限!")</script>';
            exit;
        }
    }

    /**
     * 图片上传
     */
    public function upload(){
        $filename = input('filename');
        upload($filename);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\admin\Account;
use app\admin\model\admin\AccountPower;
use app\admin\model\admin\AccountRole;

class Rbac extends Common{

    //管理员列表
    public function admin(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $list = Account::selAccount($page*$limit-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //管理员操作
    public function admin_operate(){
        $id = input('id');
        if ($id){
            $find = Account::getAccountInfo(['id' => $id]);;
            $this->assign('find', $find);
        }
        //查询所有的角色
        $role = AccountRole::selRole('id, role');
        $this->assign('role', $role);
        return view();
    }

    //管理员添加
    public function admin_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $user = $field['username'];
            $password = $field['password'];
            $role = implode(',', $field['role']);
            if (!$user || !$password){
                echo 0;
                exit;
            }
            $cun = Account::getAccountInfo(['username' => $user]);
            if ($cun){
                echo 2;
            }else{
                $arr = array(
                    'username' => $user,
                    'password' => md5($password),
                    'role' => $role
                );
                $re = Account::add($arr);
                if ($re){
                    echo 1;
                }else{
                    echo 0;
                }
            }
        }else{
            echo 0;
        }
    }

    //修改
    public function admin_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $id = $field['id'];
            $user = $field['username'];
            $password = $field['password'];
            $role = implode(',', $field['role']);
            if (!$user){
                echo 0;
                exit;
            }
            $arr = array(
                'username' => $user,
                'role' => $role
            );
            if ($password){
                $arr['password'] = md5($password);
            }
            $re = Account::edit(['id' => $id], $arr);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //管理员删除
    public function admin_del(){
        if (request()->isPost()){
            $id = input('id');
            $re = Account::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //角色管理
    public function role(){
        if (request()->isPost()){
            $page = input('post.page');
            $limit = input('post.limit');
            $list = AccountRole::selRoleLimit($page*$limit-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //角色操作
    public function role_operate(){
        $id = input('id');
        if ($id){
            $find = AccountRole::getRoleInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        //查询所有权限
        $access = AccountPower::selPower(['classid' => 0]); //顶级
        $lower = AccountPower::selPower(['classid' => ['neq', 0]]); //下级
        if ($access) $access = $access->toArray();
        if ($lower) $lower = $lower->toArray();
        foreach ($access as $k => $v){
            foreach ($lower as $key => $val){
                if ($v['id'] == $val['classid']){
                    $access[$k]['lower'][] = $lower[$key];
                }
            }
        }
        $this->assign('accrss', $access);
        return view();
    }

    //角色添加
    public function role_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $role = $field['role'];
            $checked = implode(',', $field['checked']);
            $arr = array(
                'role' => $role,
                'power' => $checked
            );
            $re = AccountRole::add($arr);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //角色修改
    public function role_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $id = $field['id'];
            $role = $field['role'];
            $checked = implode(',', $field['checked']);
            $arr = array(
                'role' => $role,
                'power' => $checked
            );
            $re = AccountRole::edit(['id' => $id], $arr);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //角色删除
    public function role_del(){
        if (request()->isPost()){
            $id = input('id');
            $re = AccountRole::del(['id' => $id]);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //权限列表
    public function access(){
        $list = AccountPower::selPower(['classid' => 0]);
        if ($list) $list = $list->toArray();
        foreach ($list as $key => $val){
            $list[$key]['erji'] = AccountPower::selPower(['classid' => $val['id']]);
        }
        $this->assign('list', $list);
        return view();
    }

    //权限操作
    public function access_operate(){
        $id = input('id');
        if ($id){
            $find = AccountPower::getPowerInfo(['id' => $id]);
            $this->assign('find', $find);
        }
        $ding = AccountPower::selPower(['classid' => 0], 'id desc');
        $this->assign('ding', $ding);
        return view();
    }

    //添加权限
    public function access_add(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $classid = $field['classid'];
            if ($classid ==  ''){
                $data['classid'] = 0;
            }else{
                $data['classid'] = $classid;
            }
            $data['power_name'] = $field['power_name'];
            $data['power_url'] = $field['power_url'];
            $id = AccountPower::add($data);
            if ($id){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //修改权限
    public function access_edit(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $id = $field['id'];
            $classid = $field['classid'];
            if ($classid ==  ''){
                $data['classid'] = 0;
            }else{
                $data['classid'] = $classid;
            }
            $data['power_name'] = $field['power_name'];
            $data['power_url'] = $field['power_url'];
            $re = AccountPower::edit(['id' => $id], $data);
            if ($re){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //删除权限
    public function access_del(){
        if (request()->isPost()){
            $id = input('post.id');
            $re = AccountPower::getPowerInfo(['classid' => $id]);
            if ($re){
                echo 2;
                exit;
            }
            $del = AccountPower::del(['id' => $id]);
            if ($del){
                echo 1;
            }else{
                echo 0;
            }
        }else{
            echo 0;
        }
    }

    //个人信息修改
    public function personal(){
        if (request()->isPost()){
            $field = input('post.')['field'];
            $password = $field['password'];
            if ($password){
                $re = Account::upField(['username' => session('NAME')], 'password', md5($password));
                if ($re){
                    echo 1;
                }else{
                    echo 0;
                }
            }else{
                echo 0;
            }
        }else{
            $find = Account::getAccountInfo(['username' => session('NAME')]);
            $this->assign('find', $find);
            return view();
        }
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\user\User;
use app\admin\model\user\UserCash;
use app\admin\model\goods\GoodsGiftCodeRecord;

class Cash extends Common{

    //提现管理
    public function cash(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $info = input('info');
            $name = base64_decode($info);
            $date = explode(' - ', input('date')); //日期
            $start = strtotime($date[0]); //开始日期 转换为时间戳
            $end = strtotime($date[1]); //结束日期 转换为时间戳
            if ($start && $end){
                $where['time'] = [['gt', $start], ['lt', $end]];
            }
            if ($info){
                $uid1 = User::getField(['name' => ['like', "%$name%"]], 'id', 1);
                $uid2 = User::getField(['phone' => ['like', "%$info%"]], 'id', 1);
                $whole = array_merge($uid1, $uid2);
                $where['uid'] = ['in', $whole];
            }
            $list = UserCash::selUserCash($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            return view();
        }
    }

    //提现审核
    public function cash_operate(){
        if (request()->isPost()){
            $id = input('id');
            $type = input('type');
            $find = UserCash::getUserCashInfo(['id' => $id]);
            $openid = User::getField(['id' => $find['uid']], 'openid');
            if ($find){
                if ($type == 1){
                    $result = UserCash::upField(['id' => $id], 'status', 1);
                    //提现数据
                    $order = array(
                        'partner_trade_no' => $find['order'], //订单号
                        'amount' => $find['money'] * 100, //订单金额  以(分)为单位（需要根据自己的业务修改）
                        'desc' => '提现', //企业付款备注
                        'openid' => $openid, //获取到的openid
                        'check_name' => 'NO_CHECK',
                    );
                    import('WeChatPay.XcxPay');
                    $pay = new \XcxPay();
                    $transfers = $pay->transfers($order);
                    if ($result && $transfers['result_code'] == 'SUCCESS'){
                        User::decField(['id' => $find['uid']], 'gold', $find['money']);
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    $result = UserCash::upField(['id' => $id], 'status', 2);
                    if ($result){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }
            }else{
                echo 0;
            }
        }
    }

    //核销记录
    public function he(){
        if (request()->isPost()){
            $page = input('page');
            $limit = input('limit');
            $lid = input('id');
            $date = explode(' - ', input('date')); //日期
            $start = strtotime($date[0]); //开始日期 转换为时间戳
            $end = strtotime($date[1]); //结束日期 转换为时间戳
            if ($start && $end){
                $where['h_time'] = [['gt', $start], ['lt', $end]];
            }
            if ($lid){
                $where['lid'] = $lid;
                $this->assign('lid', $lid);
            }
            $list = GoodsGiftCodeRecord::selUserCodeRecord($where, $limit*$page-$limit, $limit);
            echo json_encode(array('code' => 0, 'count' => $list['count'], 'data' => $list['data']));
        }else{
            $id = input('id');
            $this->assign('id', $id);
            return view();
        }
    }
}
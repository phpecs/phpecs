<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\admin\controller;
use app\admin\controller;
use app\admin\model\store\StoreOrder;
use app\admin\model\store\StoreRefund;
use app\admin\model\user\User;
use app\admin\model\user\UserCarry;
use app\admin\model\user\UserCash;
use app\admin\model\user\UserLead;

class Index extends Common{

    //后台首页
    public function index(){
        return view('index');
    }

    //控制台
    public function console(){
        $today = strtotime(date('Y-m-d', time()));
        $count[0] = StoreOrder::countOrder(['time' => ['GT', $today]]);
        $count[1] = UserCarry::countCarry(['status' => 0]);
        $count[2] = UserLead::countLead(['status' => 0]);
        $count[3] = UserCash::countCash(['status' => 0]);
        $count[4] = StoreRefund::countRefund(['status' => 0]);
        //今天新增粉丝
        $time = strtotime(date('Y-m-d'));
        $count[5] = User::countUser(['time' => ['GT', $time]]);
        //当月新增粉丝
        $time2 = strtotime(date('Y-m-d', strtotime('-1 Month')));
        $count[6] = User::countUser(['time' => ['GT', $time2]]);
        //订单
        $data2 = StoreOrder::selOrderECharts3(1);
        //近一个月新增用户
        $data = User::selUserECharts();
        $this->assign([
            'count' => $count,
            'data2' => json_encode($data2),
            'data' => json_encode($data)
        ]);
        return view('console');
    }
}
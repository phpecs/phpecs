<?php
namespace app\index\controller;
use think\Controller;
class Index extends Controller{

    public function index(){
        return view();
    }

    public function join_us(){
        return view();
    }

    public function service_statement(){
        return view();
    }

    public function contact_us(){
        return view();
    }

    public function brand_service(){
        return view();
    }
}

<?php
namespace app\index\controller;
use think\Controller;
class Timing extends Controller{

    //凌晨更新数据
    public function dataUpload(){
        ignore_user_abort(); //即使Client断开(如关掉浏览器)，PHP脚本也可以继续执行.
        set_time_limit(0); // 执行时间为无限制，php默认执行时间是30秒，可以让程序无限制的执行下去
        $interval = 60 * 60; // 每隔一小时运行一次
        do{
            $run = include 'switch.php';
            if(!$run) exit;
            //这里可输入您要执行的代码
            //不是凌晨不执行
            $time = date('H', time());
            if ($time == 00){
                $order = db('order')->where(['type' => 1, 'status' => 6])->field('id, h_time')->select();
                foreach ($order as $k => $v){
                    //查询最后一天时间的时间
                    $h_time = db('qian')->where(['ch_id' => $v['id']])->order('time desc')->value('time');
                    $h_time = $h_time ? $h_time : $v['h_time'];
                    if (time() - $h_time > 3600 * 24 * 3){
                        $ord = [
                            'status' => 1,
                            'active_status' => 2
                        ];
                        db('order')->where('id', $v['id'])->update($ord);
                    }
                }
            }
            sleep($interval);
        }while(true);
    }
}


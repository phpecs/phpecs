<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\system;
use think\Model;
class HelpCategory extends Model{

    /**
     * 查出指定分类
     * @param $where
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selHelpCategory($where, $order = 'sort asc'){
        return self::where($where)->order($order)->select();
    }

    /**
     * 查询某一组字段
     * @return array
     */
    public static function columnField($where, $field){
        return self::where($where)->column($field);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\system;
use think\Model;
class Verify extends Model{

    /**
     * 查询一条指定数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getVerifyInfo($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改用户验证码数据集
     * @param $where
     * @param $data
     * @return $this
     */
    public static function upVerify($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 添加一条验证码数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
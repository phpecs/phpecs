<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\system;
use app\hgapi\model\goods\GoodsPackage;
use app\hgapi\model\store\StoreProduct;
use app\hgapi\model\user\User;
use think\Model;
class Foot extends Model{

    /**
     * 查出区块数据
     * @param $where
     * @param $field
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selFoot($where, $order = 'sort asc'){
        $data = self::where($where)->order($order)->select();
        foreach ($data as $k => $v){
            $where2 = []; //清空上次循环遗留的条件
            $where2['is_del'] = 0;
            $where2['status'] = 0;
            switch ($v['model']){
                case 1:
                    $where2['is_tui'] = 1;
                    $productArr = StoreProduct::selProduct($where2, '', $v['num']);
                    $data[$k]['InfoArr'] = $productArr;
                    break;
                case 2:
                    $where2['is_jing'] = 1;
                    $productArr = StoreProduct::selProduct($where2, '', $v['num']);
                    $data[$k]['InfoArr'] = $productArr;
                    break;
                case 3:
                    $where2['is_new'] = 1;
                    $productArr = StoreProduct::selProduct($where2, '', $v['num']);
                    $data[$k]['InfoArr'] = $productArr;
                    break;
                case 4:
                    $data[$k]['InfoArr'] = User::selStoreUser($v['num']);
                    break;
                case 5:
                    $data[$k]['InfoArr'] = GoodsPackage::selPackageLimit($v['num']);
                    break;
                case 6:
                    $data[$k]['InfoArr'] = Ad::selAd(['status' => 1], '');
                    break;
                default:
                    $where2['is_tui'] = 1;
                    $productArr = StoreProduct::selProduct($where2, '', $v['num']);
                    $data[$k]['InfoArr'] = $productArr;
                    break;
            }

        }
        return $data;
    }
}
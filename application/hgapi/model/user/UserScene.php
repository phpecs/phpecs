<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserScene extends Model{

    /**
     * 新增并返回一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insertGetId($data);
    }

    /**
     * 根据条件查询指定的小程序Scene
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getXcxSceneInfo($where, $field){
        return self::where($where)->field($field)->find();
    }
}
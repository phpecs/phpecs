<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserWriter extends Model{

    /**
     * 根据条件查出所有下级核销员
     * @param $where
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selWriter($where, $order = 'time desc'){
        return self::where($where)->order($order)->select();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 根据条件删除一条数据
     * @param $where
     * @return int
     */
    public static function delWriter($where){
        return self::where($where)->delete();
    }

    /**
     * 查询某一字段的一组数据
     * @param $where
     * @param $field
     * @return array
     */
    public static function columnWriter($where, $field){
        return self::where($where)->column($field);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserPackageRecord extends Model{

    /**
     * 获取订单
     * @param $ord_id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getPayPackageInfo($ord_id){
        return self::where('order', $ord_id)->find();
    }

    /**
     * 修改状态
     * @param $ord_id
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($ord_id, $field, $value){
        return self::where('order', $ord_id)->setField($field, $value);
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

}
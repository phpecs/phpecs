<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class User extends Model{

    /**
     * 查询用户指定信息
     * @param $id
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 查询某个字段值
     * @param string $where
     * @param null $field
     * @param int $type
     */
    public static function getField($where, $field, $type = 0){
        if ($type == 0){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }

    /**
     * 修改某个字段值
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询我的下级人员
     * @param $where
     * @param $field
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selMember($where, $field, $start, $limit, $order = 'time desc'){
        return self::where($where)->field($field)->limit($start, $limit)->order($order)->select();
    }

    /**
     * 字段增
     * @param $uid
     * @param $field
     * @param int $price
     * @return int|true
     */
    public static function incField($uid, $field, $price = 1){
        return self::where('id', $uid)->setInc($field, $price);
    }

    /**
     * 字段减
     * @param $uid
     * @param $field
     * @param int $price
     * @return int|true
     */
    public static function decField($uid, $field, $price = 1){
        return self::where('id', $uid)->setDec($field, $price);
    }

    /**
     * 修改数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function upUserInfo($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 新增用户并返回新增id
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insertGetId($data);
    }

    /**
     * 查询指定用户的和权限
     * @param $uid
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserInfoZhi($uid){
        return self::alias('u')->join('user_role r', 'u.id = r.uid')->where('u.id', $uid)->field('u.name, u.portrait, r.pf')->find();
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 查询指定条数的商家
     * @param $num
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selStoreUser($num, $order = 'r.id asc'){
        $data = self::alias('u')->join('user_role r', 'u.id = r.uid')->where('r.gift', 1)->whereOr('r.drainage', 1)->field('u.*')->order($order)->limit($num)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
        }
        return $data;
    }

    /**
     * 查出折扣
     * @param $uid
     */
    public static function selUserZhe($uid){
        return self::alias('u')->join('user_grade g', 'u.grade = g.grade')->where('u.id', $uid)->where('g.status', 1)->value('discount');
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserRole extends Model{

    /**
     * 查询用户权限
     * @param $id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getRole($id){
        return self::where('uid', $id)->find();
    }

    /**
     * 新增角色权限
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 修改字段
     * @param $id
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($uid, $field, $value){
        return self::where('uid', $uid)->setField($field, $value);
    }
}

<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserRecord extends Model{

    /**
     * 查询用户充值记录
     * @param $where
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selRecord($where, $start, $limit, $order = 'time desc'){
        return self::where($where)->order($order)->limit($start, $limit)->select();
    }

    /**
     * 查询一条记录
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getRecord($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改某个字段值
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 添加
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use app\hgapi\model\goods\GoodsPackage;
use think\Model;
class UserLead extends Model{

    /**
     * 查出引流商家指定审核数据
     * @param $where
     * @param $info
     * @param $nameId
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selLeadZhi($where, $info, $nameId, $start, $limit, $order = 'id desc'){
        $data = self::whereLead($where, $info, $nameId)->limit($start, $limit)->order($order)->select();
        foreach ($data as $k => $v){
            $name = User::getField(['id' => $v['uid']], 'name', 0);
            $name2 = User::getField(['id' => $v['recruit']], 'name', 0);
            //大礼包信息
            $data[$k]['name'] = base64_decode($name);
            $data[$k]['recruit_name'] = base64_decode($name2);
        }
        $count = self::whereLead($where, $info, $nameId)->count();
        $list = [
            'data' => $data,
            'count' => $count,
        ];
        return $list;
    }

    /**
     * 条件数据
     * @param $where
     * @param $info
     * @param $nameId
     * @return $this
     */
    public static function whereLead($where, $info, $nameId){
        return self::where($where)->where(function ($query)use($info, $nameId){
            $query->where('phone', 'like', "%$info%")->whereOr('uid', 'in', $nameId);
        });
    }

    /**
     * 查询引流商家某条数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getLead($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改某条字段数据
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
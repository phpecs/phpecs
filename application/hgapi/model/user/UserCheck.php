<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserCheck extends Model{

    /**
     * 查询用户买单记录
     * @param $where
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selCheck($where, $start, $limit, $order = 'time desc'){
        return self::where($where)->order($order)->limit($start, $limit)->select();
    }

    /**
     * 查询一条信息
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getCheck($where, $field){
        return self::where($where)->field($field)->find();
    }


    /**
     * 修改订单状态
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 添加
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserCoupon extends Model{

    /**
     * 查询我的优惠券
     * @param $where
     * @param $status
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selUserCoupon($where, $status){
        return self::where($where)->where('status', $status)->select();
    }

    /**
     * 修改用户优惠券信息
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询指定优惠券
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserCoupon($where){
        return self::where($where)->find();
    }

    /**
     * 统计优惠券数量
     * @param $where
     * @return int|string
     */
    public static function countCoupon($where){
        return self::where($where)->count();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserPackage extends Model{

    /**
     * 查询某一条字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 查出已拥有的大礼包
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getPackageY($id){
        return self::alias('u')->join('goods_package p', 'p.id = u.package')->where('u.uid', $id)->field('p.id, p.name, p.start, p.end, u.yid')->select();
    }

    /**
     * 添加一条用户大礼包数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查询用户指定的大礼包数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserPackageInfo($where, $field){
        return self::where($where)->field($field)->find();
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserLipin extends Model{

    /**
     * 查出礼品记录
     * @param $where
     * @param $field
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selLipin($where, $field){
        return self::where($where)->field($field)->select();
    }

    /**
     * 查出一条记录
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getLipin($where){
        return self::where($where)->find();
    }

    /**
     * 查出某个字段
     * @param string $where
     * @param null $field
     * @param string $order
     * @return mixed
     */
    public static function getField($where, $field, $order = 'time desc'){
        return self::where($where)->order($order)->value($field);
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }
}
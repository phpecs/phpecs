<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserCart extends Model{

    protected $pk = 'id';

    /**
     * 查出用户购物车里面的数据
     * @param $where
     * @param $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selCart($where, $order){
        return self::where($where)->order($order)->select();
    }

    /**
     * 查询指定购物车数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getCart($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改指定数据
     * @param $where
     * @param $field
     * @param $value
     * @return int|true
     */
    public static function incField($where, $field, $value){
        return self::where($where)->setInc($field, $value);
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查询购物车里面的数据信息
     * @param $id
     * @param $field
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selCartZhi($id, $field){
        return self::alias('c')->join('store_product p', 'c.pid = p.id')->where('c.uid', $id)->field($field)->order('c.id desc')->select();
    }

    /**
     * 从购物车中删除
     * @param $where
     * @return int
     */
    public static function delCart($where){
        return self::where($where)->delete();
    }
}
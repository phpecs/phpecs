<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserChoose extends Model{

    /**
     * 添加一条记录
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查出指定的数据
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selChooseZhi($id){
        return self::alias('c')->join('goods_package p', 'c.package = p.id')->where('c.uid', $id)->field('c.img, p.id, p.name, p.start, p.end, p.sort, p.gid, p.img pimg, c.status')->select();
    }

    /**
     * 查出申请的引流商家
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selYin($id){
        $data = self::where('package', $id)->select();
        if ($data) $data = $data->toArray();
        foreach ($data as $k => $v){
            $trade = UserTrade::getTrade(['uid' => $v['uid']]);
            if (!$trade) $trade = UserLead::getLead(['uid' => $v['uid']], '');
            $data[$k]['trade'] = $trade;
            $find = User::getUserInfo(['id' => $v['uid']]);
            $find['name'] = base64_decode($find['name']);
            $data[$k]['user'] = $find;
        }
        return $data;
    }

    /**
     * 查询一条字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 查询一条数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getUserChooseInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改一条数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 修改一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

}
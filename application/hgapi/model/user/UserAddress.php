<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\user;
use think\Model;
class UserAddress extends Model{

    /**
     * 查询用户所有地址
     * @param $uid
     * @param $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getAddress($uid, $order){
        return self::where('uid', $uid)->order($order)->select();
    }

    /**
     * 添加或修改地址
     * @param $field
     * @param $type
     * @return $this|int|string
     */
    public static function UpAddress($field, $type){
        $data = [
            'uid' => $field['uid'],
            'name' => $field['name'],
            'phone' => $field['phone'],
            'province' => $field['dizhi'][0],
            'city' => $field['dizhi'][1],
            'county' => $field['dizhi'][2],
            'address' => $field['address'],
            'is_default' => $field['is_default'] ? $field['is_default'] : 0,
        ];
        if ($field['default'] == 1){
            db('address')->where('uid', $field['uid'])->setField('default', 0);
        }
        if ($type == 0){
            return self::insert($data);
        }else{
            return self::where('id', $field['id'])->update($data);
        }
    }

    /**
     * 删除用户指定地址
     * @param $id
     * @param $uid
     * @return int
     */
    public static function DelAddress($where){
        return self::where($where)->delete();
    }
}

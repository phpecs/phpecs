<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\activity;
use think\Model;
class ActivityClockQian extends Model{

    /**
     * 获取签到记录
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getQian($where){
        return self::where($where)->order('time desc')->select();
    }

    /**
     * 查询某一条的签到记录
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getFindQian($where){
        return self::where($where)->find();
    }

    /**
     * 查询某一个字段值
     * @param string $where
     * @param null $field
     * @param string $order
     * @return mixed
     */
    public static function getField($where, $field, $order = 'time desc'){
        return self::where($where)->order($order)->value($field);
    }

    /**
     * 查询某一组值
     * @param $where
     * @param $field
     * @return array
     */
    public static function getColumn($where, $field){
        return self::where($where)->column($field);
    }

    /**
     * 新增数据
     * @param $data
     * @return int|string
     */
    public static function addQian($data){
        return self::insert($data);
    }

    /**
     * 修改数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function upQian($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 统计
     * @param $where
     * @return int|string
     */
    public static function countQian($where){
        return self::where($where)->count();
    }
}

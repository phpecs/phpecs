<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\activity;
use think\Model;
class ActivityRecharge extends Model{

    /**
     * 获取充值活动信息
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getRechargeInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }
}
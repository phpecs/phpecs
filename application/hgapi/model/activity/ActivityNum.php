<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\activity;
use think\Model;
class ActivityNum extends Model{

    /**
     * 查出一条记录
     * @param $where
     * @param string $field
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getActivityNumInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 添加一条记录
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 一个字段增加数据
     * @param $where
     * @param $field
     * @param $value
     * @return int|true
     */
    public static function incField($where, $field, $value){
        return self::where($where)->setInc($field, $value);
    }

    /**
     * 统计条数
     * @param $where
     * @return int|string
     * @throws \think\Exception
     */
    public static function countActivityNum($where){
        return self::where($where)->count();
    }

    /**
     * 查询出数据
     * @param $where
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selActivityNum($where, $field = '', $order = 'time desc'){
        return self::where($where)->field($field)->order($order)->select();
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\activity;
use think\Model;
class ActivityList extends Model{

    /**
     * 查询活动数据
     * @param $where
     * @param $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selActivityList($where, $field, $order = 'time desc'){
        return self::where($where)->field($field)->order($order)->select();
    }

    /**
     * 查询一条活动数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getActivityListInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 查询一个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 查出是否已使用
     */
    public static function is_id($id){
        return self::where('status', 1)->where(function ($query) use ($id){
            $query->whereOr('pid', 'like', "$id,%")->whereOr('pid', 'like', "%,$id,%")->whereOr('pid', 'like', "%,$id")->whereOr('pid', $id);
        })->value('id');
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\activity;
use think\Model;
class ActivityClock extends Model{

    /**
     * 获取某个值
     * @param $id
     * @return mixed
     */
    public static function getClockValue($id){
        return self::alias('c')->join('store_product p', 'p.id = c.pid')->where('c.id', $id)->value('p.type');
    }

    /**
     * 查询指定数据
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getClock($where){
        return self::where($where)->find();
    }

    /**
     * 查询指定活动数据
     * @param $id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selClockZhi($id){
        return self::alias('c')->join('store_product p', 'p.id = c.pid', 'LEFT')->where('c.id', $id)->field('c.*,p.name pname')->order('c.id desc')->select();
    }

    /**
     * 查询某一条指定数据
     * @param $id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getClockZhi($id){
        return self::alias('c')->join('store_product p', 'p.id = c.pid')->where('c.id', $id)->field('c.*, p.name pname, p.details pdetails,p.type')->find();
    }

    /**
     * 获取打卡活动
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selClockShuJu(){
        return self::alias('c')->join('store_product p', 'p.id = c.pid', 'LEFT')->where(['c.status' => 1, 'c.bao' => 1])->field('c.*,p.name shop')->order('c.id desc')->select();
    }
}
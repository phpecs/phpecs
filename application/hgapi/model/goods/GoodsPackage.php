<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\goods;
use think\Model;
class GoodsPackage extends Model{

    /**
     * 查询某一条字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field){
        return self::where($where)->value($field);
    }

    /**
     * 查询大礼包数据
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getPackage($where){
        return self::where($where)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查询指定的数据
     * @param $where
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selPackage($where, $order = 'sort asc'){
        return self::where($where)->order($order)->select();
    }

    /**
     * 查询指定条数大礼包信息
     * @param $num
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selPackageLimit($num, $order = 'sort asc'){
        return self::where('status', 1)->order($order)->limit($num)->select();
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\goods;
use think\Model;
class GoodsGift extends Model{

    /**
     * 查出数据
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selGift($where){
        return self::where($where)->select();
    }

    /**
     * 分页查询
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selGiftLimit($start, $limit, $order = 'id desc'){
        return self::order($order)->limit($start, $limit)->select();
    }

    /**
     * 查出一条数据
     * @param $where
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getGift($where){
        return self::where($where)->find();
    }

    /**
     * 添加一条数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insertGetId($data);
    }

    /**
     * 条件统计
     * @param $where
     * @return int|string
     */
    public static function countGift($where){
        return self::where($where)->count();
    }

    /**
     * 修改一个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 修改礼品
     * @param $where
     * @param $data
     * @return $this
     */
    public static function edit($where, $data){
        return self::where($where)->update($data);
    }
}
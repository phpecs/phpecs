<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\market;
use think\Model;
class MarketSeckillTime extends Model{

    /**
     * 查询数据
     * @param $where
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selTime($where, $field = '', $order = 'start_time asc'){
        $data = self::where($where)->field($field)->order($order)->select();
        $h = date('H'); //当前小时数
        foreach ($data as $k => $v){
            $start = $v['start_time']; //开始时间
            $end_time = $v['start_time'] + $v['continue_time']; //结束时间
            if ($start <= $h && $h < $end_time){
                $data[$k]['state'] = '抢购中';
                $data[$k]['status'] = 1;
                $active = $k;
            }elseif ($h < $start){
                $data[$k]['state'] = '即将开始';
                $data[$k]['status'] = 2;
            }elseif ($h >= $end_time){
                $data[$k]['state'] = '已结束';
                $data[$k]['status'] = 0;
            }
            $data[$k]['end_time'] = mktime($end_time, 0, 0);
        }
        return compact('data', 'active');
    }

    /**
     * 查询一条时间数据
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getTimeInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }
}
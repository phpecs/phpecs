<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\market;
use app\hgapi\model\store\StoreProductImg;
use think\Model;
class MarketLikes extends Model{

    /**
     * 查出数据
     * @param $where
     * @param $start
     * @param $limit
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selLikesLimit($where, $start, $limit, $field = '', $order = 'id desc'){
        $data = self::where($where)->field($field)->order($order)->limit($start, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['img'] = StoreProductImg::getImg($v['pid']);
            $data[$k]['people'] = 0;
        }
        return $data;
    }
    /**
     * 查出数据
     * @param $where
     * @param $start
     * @param $limit
     * @param string $field
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selLikes($where, $field = '', $order = 'id desc'){
        $data = self::where($where)->field($field)->order($order)->select();
        foreach ($data as $k => $v){
            $data[$k]['img'] = StoreProductImg::getImg($v['pid']);
            $data[$k]['people'] = 0;
        }
        return $data;
    }

    /**
     * 查询活动详情
     * @param $where
     * @param string $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getLikesInfo($where, $field = ''){
        return self::where($where)->field($field)->find();
    }

    /**
     * 增加字段值
     * @param $where
     * @param $field
     * @param $value
     * @return int|true
     */
    public static function incField($where, $field, $value){
        return self::where($where)->setInc($field, $value);
    }

    /**
     * 减少字段值
     * @param $where
     * @param $field
     * @param $value
     * @return int|true
     */
    public static function decField($where, $field, $value){
        return self::where($where)->setDec($field, $value);
    }
}
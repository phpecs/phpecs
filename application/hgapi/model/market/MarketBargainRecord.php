<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\market;
use think\Model;
class MarketBargainRecord extends Model{

    /**
     * 统计砍价金额
     * @param $where
     * @param $field
     * @return float|int
     */
    public static function sumBargain($where, $field){
        return self::where($where)->sum($field);
    }

    /**
     * 统计次数
     * @param $where
     * @return float|int
     */
    public static function countBargain($where){
        return self::where($where)->count();
    }

    /**
     * 查出砍价记录和用户
     * @param $uid
     * @param $bargain_id
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selRecordZhi($uid, $bargain_id, $start, $limit){
        $data = self::alias('r')->join('user u', 'r.bargain_uid = u.id')->where(['r.uid' => $uid, 'r.bargain_id' => $bargain_id])
            ->field('r.bargain_money, r.time, u.name, u.portrait, r.sword, r.envelopes')->order('r.time desc')->limit($start, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['name'] = base64_decode($v['name']);
            $data[$k]['time'] = date('Y-m-d H:i:s', $v['time']);
        }
        return $data;
    }

    /**
     * 统计宫有多少帮砍用户
     * @param $where
     * @return int|string
     */
    public static function groupBargainNum($where){
        return self::where($where)->group('bargain_uid')->count();
    }

    /**
     * 添加记录
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 获取某个字段
     * @param string $where
     * @param null $field
     * @return mixed
     */
    public static function getField($where, $field, $order = 'time asc'){
        return self::where($where)->order($order)->value($field);
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\store;
use think\Model;
class StoreOrder extends Model{

    /**
     * 查询签到订单
     * @param $uid
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function getOrderQian($uid){
        $data = self::alias('o')->join('activity_clock c', 'o.active_id = c.id')->join('store_product p', 'c.pid = p.id')
            ->where(['o.uid' => $uid, 'o.type' => 1, 'o.status' => ['neq', 0]])->field('o.id,o.h_time,p.time,c.name,p.id pid')->select();
        foreach ($data as $k => $v){
            $data[$k]['img'] = StoreProductImg::getImg($v['pid']);
            $data[$k]['h_time'] = date('Y-m-d H:i:s', $v['h_time']);
        }
        return $data;
    }

    /**
     * 查询订单详情
     * @param $uid
     * @param $id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getOrderQianXiang($uid, $id){
        $find = self::alias('o')->join('activity_clock c', 'o.active_id = c.id')->join('store_product p', 'c.pid = p.id')
            ->where(['o.uid' => $uid, 'o.id' => $id, 'o.type' => 1])->field('o.*, c.id clock_id,c.name,c.tian, c.img, c.color,c.rule,c.tui,p.id pid,p.genus')->find();
        $find['pimg'] = StoreProductImg::getImg($find['pid']);
        return $find;
    }

    /**
     * 查出用户头像
     * @param $spell_number
     * @param string $order
     * @return array
     */
    public static function columnOrderPin($spell_number, $order = 'o.time asc'){
        return self::alias('o')->join('user u', 'o.uid = u.id')->where(['o.spell_number' => $spell_number, 'o.status' => 1])->order($order)->column('portrait');
    }

    /**
     * 查询活动订单详情
     * @param $oid
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getOrderClock($oid){
        return self::alias('o')->join('activity_clock c', 'o.active_id = c.id')->where('o.id', $oid)->field('c.name, o.h_time, c.tian, c.rule')->find();
    }

    /**
     * 根据指定条件获取指定的一条数据
     * @param $oid
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getOrderClockZhi($oid){
        return self::alias('o')->join('activity_clock c', 'o.active_id = c.id')->where('o.id', $oid)->field('o.price,o.is_hx,c.tian')->find();
    }

    /**
     * 修改订单数据
     * @param $where
     * @param $data
     * @return $this
     */
    public static function upOrder($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 获取某个订单数据
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getOrder($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 获取二维码信息
     * @param $id
     * @param $uid
     */
    public static function getOrderGenus($id, $uid){
        return self::alias('o')->join('activity_clock c', 'o.active_id = c.id')->join('store_product p', 'p.id = c.pid')->where(['o.id' => $id, 'o.uid' => $uid])->value('genus');
    }

    /**
     * 获取订单数据
     * @param $where
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selOrder($where, $field, $start, $limit, $order = 'time desc'){
        return self::where($where)->field($field)->order($order)->limit($start, $limit)->select();
    }

    /**
     * 修改某条字段信息
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }


    /**
     * 查询某条字段信息
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function getField($where, $field, $type = 0){
        if ($type){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }

    /**
     * 添加一条订单并返回id
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insertGetId($data);
    }

    /**
     * 获取最后一条数据id
     * @param $where
     * @param $order
     * @return mixed
     */
    public static function valueId($where, $order){
        return self::where($where)->order($order)->value('id');
    }

    /**
     * 删除订单
     * @param $where
     * @return int
     */
    public static function delOrder($where){
        return self::where($where)->delete();
    }

    /**
     * 统计订单
     * @param $where
     * @return string
     */
    public static function countOrder($where){
        return self::where($where)->count();
    }

    /**
     * 查出未完成的订单
     * @param $market_id
     * @param $spell_number
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function conductOrder($market_id, $spell_time, $spell_number, $order = 'time asc', $limit = 10){
        $time = time() - $spell_time; //最低开始时间
        $field = 'spell_number, count(*) num, time';
        $data = self::where(['spell_id' => $market_id, 'time' => ['egt', $time], 'status' => 1])->field($field)
            ->group('spell_number')->having('count(*) < ' . $spell_number)->order($order)->limit($limit)->select();
        $arr = [];
        foreach ($data as $k => $v){
            $user_info = self::alias('o')->join('user u', 'o.uid = u.id')->where('o.order', $v['spell_number'])->field('o.id, u.name, u.portrait')->find();
            $arr[$k]['oid'] = $user_info['id'];
            $arr[$k]['name'] = base64_decode($user_info['name']);
            $arr[$k]['portrait'] = $user_info['portrait'];
            $arr[$k]['num'] = $spell_number - $v['num'];
            $arr[$k]['end_time'] = $v['time'] + $spell_time;
        }
        return $arr;
    }
}
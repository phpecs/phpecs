<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\store;
use think\Model;
class StoreProduct extends Model{

    /**
     * 查询商品信息
     * @param $where
     * @param $field
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getProductInfo($where, $field){
        return self::where($where)->field($field)->find();
    }

    /**
     * 修改商品信息
     * @param $where
     * @param $data
     * @return $this
     */
    public static function upProduct($where, $data){
        return self::where($where)->update($data);
    }

    /**
     * 查出数据和封面图片
     * @param $where
     * @param $tui
     * @param $field
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selProduct($where, $field, $limit, $order = 'time desc'){
        $data = self::where($where)->field($field)->order($order)->limit($limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['img'] = StoreProductImg::getImg($v['id']);
        }
        return $data;
    }

    /**
     * 分页查询
     * @param $where
     * @param $field
     * @param $start
     * @param $limit
     * @param string $order
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selProductLimit($where, $field, $start, $limit, $order = 'time desc'){
        $data = self::where($where)->field($field)->order($order)->limit($start, $limit)->select();
        foreach ($data as $k => $v){
            $data[$k]['img'] = StoreProductImg::getImg($v['id']);
        }
        return $data;
    }

    /**
     * 修改数据
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function upField($where, $field, $value){
        return self::where($where)->setField($field, $value);
    }

    /**
     * 查询最后一条id
     * @param $where
     * @param $order
     * @return mixed
     */
    public static function valueId($where, $order){
        return self::where($where)->order($order)->value('id');
    }

    /**
     * 查询某个字段
     * @param $where
     * @param $field
     * @param $value
     * @return int
     */
    public static function getField($where, $field, $type = 0){
        if ($type){
            return self::where($where)->value($field);
        }else{
            return self::where($where)->column($field);
        }
    }
}
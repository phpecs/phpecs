<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\model\store;
use think\Model;
class StoreOrderList extends Model{

    /**
     * 查询所有数据
     * @param $where
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selOrderList($where){
        return self::where($where)->select();
    }

    /**
     * 统计数据
     * @param $where
     * @return int|string
     */
    public static function countOrderList($where){
        return self::where($where)->count();
    }

    /**
     * 查询name
     * @param $oid
     * @return mixed
     */
    public static function valueName($oid){
        return self::alias('o')->join('store_product p', 'o.pid = p.id')->where('o.oid', $oid)->field('p.name')->order('o.id asc')->value('name');
    }

    /**
     * 添加订单详细数据
     * @param mixed|string $data
     * @return int|string
     */
    public static function add($data){
        return self::insert($data);
    }

    /**
     * 查询指定数据
     * @param $oid
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function selOrderListZhi($oid){
        return self::alias('o')->join('store_product p', 'o.pid = p.id')->where('o.oid', $oid)->field('p.name, o.*')->select();
    }

    /**
     * 删除订单详细数据
     * @param $where
     * @return int
     */
    public static function delOrderList($where){
        return self::where($where)->delete();
    }
}
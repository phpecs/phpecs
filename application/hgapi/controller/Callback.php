<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\controller;
use app\hgapi\model\market\MarketSpell;
use app\hgapi\model\activity\ActivityClock;
use app\hgapi\model\activity\ActivityRecharge;
use app\hgapi\model\goods\GoodsGift;
use app\hgapi\model\market\MarketBargain;
use app\hgapi\model\market\MarketSeckill;
use app\hgapi\model\store\StoreProduct;
use app\hgapi\model\store\StoreOrder;
use app\hgapi\model\store\StoreOrderList;
use app\hgapi\model\user\UserCarry;
use app\hgapi\model\user\UserGoldRecord;
use app\hgapi\model\user\UserPackageRecord;
use app\hgapi\model\user\UserRecord;
use app\hgapi\model\user\UserCheck;
use app\hgapi\model\user\UserCoupon;
use app\hgapi\model\user\User;
use think\Controller;
class Callback extends Controller{

    //充值余额回调
    public function notify(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $field = 'uid, money, give, cashback';
            $find = UserRecord::getRecord(['order' => $ord_id], $field);
            if ($find) $find = $find->toArray();
            $uid = $find['uid']; //付款人
            $moneys = $find['money']; //充值余额
            $give = $find['give']; //赠送余额
            $cashback = $find['cashback']; //返现余额
            //判断付款金额是否等于充值价格
            if ($money == $moneys){
                //修改订单状态
                UserRecord::upField(['order' => $ord_id], 'status', 1);
//                //添加充值记录
//                $ch = [
//                    'uid' => $uid,
//                    'money' => $moneys,
//                    'zeng' => $give,
//                    'fan' => $cashback,
//                    'time' => time()
//                ];
//                db('charge')->insert($ch);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback');
                if ($info) $info = $info->toArray();
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                if ($upper != 0){
                    gold($upper, $myName, $moneys); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                }
                //增加余额
                $data = [
                    'balance' => $info['balance'] + $moneys,
                    'give' => $info['give'] + $give,
                    'cashback' => $info['cashback'] + $cashback
                ];
                User::upUserInfo(['id' => $uid], $data);
            }
        }
    }

    //买单微信支付回调
    public function check(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = UserCheck::getCheck(['order' => $ord_id], 'shop_id, money, uid, shi, zeng, ti');
            $shop_id = $find['shop_id']; //收款商家
            $zong = $find['money']; //买单金额
            $uid = $find['uid']; //付款人
            $shi = $find['shi']; //实付金额
            $zeng = $find['zeng']; //抵扣的赠送余额
            $ti = $find['ti']; //可提现余额
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                UserCheck::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                if ($upper != 0){
                    gold($upper, $myName, $shi); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                }
                //改变余额
                $data = [
                    'give' => $info['give'] - $zeng, //剩余赠送余额
                    'cashback' => $info['cashback'] - $ti, //剩余返现余额
                    'sure' => $info['sure'] + $ti //增加可提现余额
                ];
                User::upUserInfo(['id' => $uid], $data);
                //给商家增加佣金
                User::incField($shop_id, 'gold', $zong);
            }
        }
    }

    //买单商品回调
    public function shop(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, cid, money, ling');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $cid = $find['cid']; //优惠券id
            $shi = $find['money']; //实付金额
            $ling = $find['ling']; //是否为领取商品
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                if ($cid){
                    UserCoupon::upField(['id' => $cid], 'status', 1);
                }
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'stock, volume, value, share, gold');
                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                    }
                    $value = json_decode($product['value'], true);
                    if ($value){ //如果有选项则减少选项的库存
                        foreach ($value as $key => $val){
                            if ($v['spec'] == $val['group']){ //相同的值减少
                                $value[$key]['stock'] = $val['stock'] - $v['num'];
                            }
                        }
                    }
                    $up = [
                        'stock' => $product['stock'] - $v['num'], //减少总库存
                        'volume' => $product['volume'] + 1, //增加销量
                        'value' => json_encode($value), //产品规格减少库存
                    ];
                    StoreProduct::upProduct(['id' => $v['pid']], $up);
                }
                if ($ling == 1){ //如果为充值领取则增加余额
                    //查询当前是否有充值活动
                    $re = ActivityRecharge::getRechargeInfo(['id' => 1, 'status' => 1, 'start' => ['lt', time()], 'end' => ['gt', time()], 'lowest' => ['elt', $shi]]);
                    if ($re){ //如果查询出来修改对应的金额
                        $qian = [
                            'balance' => $info['balance'] + $shi,
                            'give' => $info['give'] + ($shi * $re['give'] / 100),
                            'cashback' => $info['cashback'] + ($shi * $re['cashback'] / 100),
                        ];
                        User::upUserInfo(['id' => $uid], $qian);
                    }else{ //没有则直接增加我的余额
                        User::upField(['id' => $uid], 'balance', $shi);
                    }
                }
            }
        }
    }

    //充值押金回调
    public function gold(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = UserGoldRecord::getGoldRecordInfo(['order' => $ord_id], 'id, uid, num, gold, cid');
            $shi = $find['num'] * $find['gold']; //实付金额
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                UserGoldRecord::upField(['order' => $ord_id], 'status', 1);
                //修改审核条件改为待审核
                GoodsGift::upField(['id' => $find['cid']],'status', 0);
            }
        }
    }

    //打卡活动
    public function clock(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], '');
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            //判断付款金额是否等于实付金额
            if ($money == $find['money']){
                $type = ActivityClock::getClockValue($find['active_id']);
                $data = [
                    'status' => $type == 0 ? 6 : 7,
                    'h_time' => time()
                ];
                //修改订单状态
                StoreOrder::upOrder(['order' => $ord_id], $data);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
//                $upper = $info['upper']; //上级id
//                $myName = base64_decode($info['name']); //我的昵称
//                if ($upper != 0){
//                    gold($upper, $myName, $shi); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
//                }
                User::incField($info['upper'], 'card', 1);
            }
        }
    }

    //大礼包支付回调
    public function package(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = UserPackageRecord::getPayPackageInfo($ord_id);
            $yid = $find['yid']; //引流商家
            $shi = $find['money']; //实付金额
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                UserPackageRecord::upField($ord_id, 'status', 1);
                User::incField($yid, 'gold', $shi);
            }
        }
    }

    //砍价支付回调
    public function bargain(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, bargain_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $bargain_id = $find['bargain_id']; //砍价活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'share, gold');
                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                    }
                    MarketBargain::incField(['id' => $bargain_id], 'volume', $v['num']); //销量加
                }
            }
        }
    }

    //拼团支付回调
    public function spell(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, spell_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $spell_id = $find['spell_id']; //拼团活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'share, gold');
                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                    }
                    MarketSpell::incField(['id' => $spell_id], 'volume', $v['num']); //销量加
                }
            }
        }
    }

    //秒杀支付回调
    public function seckill(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, seckill_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $seckill_id = $find['seckill_id']; //活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'share, gold');
                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
                    }
                    MarketSeckill::incField(['id' => $seckill_id], 'volume', $v['num']); //销量加
                }
            }
        }
    }

    //全返回调
    public function back(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, back_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $back_id = $find['back_id']; //活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'stock, volume, value, share, gold');
//                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
//                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
//                    }
                    $value = json_decode($product['value'], true);
                    if ($value){ //如果有选项则减少选项的库存
                        foreach ($value as $key => $val){
                            if ($v['spec'] == $val['group']){ //相同的值减少
                                $value[$key]['stock'] = $val['stock'] - $v['num'];
                            }
                        }
                    }
                    $up = [
                        'stock' => $product['stock'] - $v['num'], //减少总库存
                        'volume' => $product['volume'] + 1, //增加销量
                        'value' => json_encode($value), //产品规格减少库存
                    ];
                    StoreProduct::upProduct(['id' => $v['pid']], $up);
                }
            }
        }
    }

    //N元购回调
    public function purchase(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, purchase_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $purchase_id = $find['purchase_id']; //活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'stock, volume, value, share, gold');
//                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
//                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
//                    }
                    $value = json_decode($product['value'], true);
                    if ($value){ //如果有选项则减少选项的库存
                        foreach ($value as $key => $val){
                            if ($v['spec'] == $val['group']){ //相同的值减少
                                $value[$key]['stock'] = $val['stock'] - $v['num'];
                            }
                        }
                    }
                    $up = [
                        'stock' => $product['stock'] - $v['num'], //减少总库存
                        'volume' => $product['volume'] + 1, //增加销量
                        'value' => json_encode($value), //产品规格减少库存
                    ];
                    StoreProduct::upProduct(['id' => $v['pid']], $up);
                }
            }
        }
    }

    //集赞支付回调
    public function likes(){
        // 导入微信支付sdk
        vendor('WeChatPay.XcxPay');
        $pay = new \XcxPay();
        $result = $pay->notify();
        if ($result) {
            //成功之后的数据操作
            $ord_id = $result['out_trade_no']; //微信返回的付款订单号
            $money = $result['total_fee'] / 100; //微信返回的付款金额
            //查询订单
            $find = StoreOrder::getOrder(['order' => $ord_id], 'id, uid, money, likes_id');
            $id = $find['id']; //订单id
            $uid = $find['uid']; //付款人
            $shi = $find['money']; //实付金额
            $likes_id = $find['likes_id']; //砍价活动id
            //判断付款金额是否等于实付金额
            if ($money == $shi){
                //修改订单状态
                StoreOrder::upField(['order' => $ord_id], 'status', 1);
                //查出我的信息
                $info = User::getUserInfo(['id' => $uid], 'upper, name, balance, give, cashback, sure');
                $upper = $info['upper']; //上级id
                $myName = base64_decode($info['name']); //我的昵称
                //查出购买订单数据并修改
                $orderlist = StoreOrderList::selOrderList(['oid' => $id]);
                foreach ($orderlist as $k => $v){
                    $product = StoreProduct::getProductInfo(['id' => $v['pid']], 'stock, volume, value, share, gold');
//                    if ($upper != 0 && $product['share'] == 1){ //商品开启了分享开关并且有上级用户
//                        gold($upper, $myName, $shi, $product['gold']); //@$upper 上级用户id @$myName 购买人名称 @$money 充值金额
//                    }
                    $value = json_decode($product['value'], true);
                    if ($value){ //如果有选项则减少选项的库存
                        foreach ($value as $key => $val){
                            if ($v['spec'] == $val['group']){ //相同的值减少
                                $value[$key]['stock'] = $val['stock'] - $v['num'];
                            }
                        }
                    }
                    $up = [
                        'stock' => $product['stock'] - $v['num'], //减少总库存
                        'volume' => $product['volume'] + 1, //增加销量
                        'value' => json_encode($value), //产品规格减少库存
                    ];
                    StoreProduct::upProduct(['id' => $v['pid']], $up);
                }
            }
        }
    }
}
<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\controller;
use think\Controller;
class Auth extends Controller{
    //初始化
    public function __construct(){
        parent::__construct();
        $token = md5(input('token'));
        $server_token = config("XCX_CONFIG.APP_TOKEN");
        if(empty($token)){
            echo json_encode(array('msg' => "你没有权限访问"));
            exit;
        }
        if($token != $server_token){
            echo json_encode(array('msg' => "证书无效"));
            exit;
        }
    }
}

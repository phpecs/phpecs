<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\controller;
use app\hgapi\model\market\MarketBack;
use app\hgapi\model\market\MarketBargain;
use app\hgapi\model\market\MarketBargainRecord;
use app\hgapi\model\market\MarketLikes;
use app\hgapi\model\market\MarketLikesRecord;
use app\hgapi\model\market\MarketPurchase;
use app\hgapi\model\market\MarketSeckill;
use app\hgapi\model\market\MarketSeckillTime;
use app\hgapi\model\market\MarketSpell;
use app\hgapi\model\store\StoreOrder;
use app\hgapi\model\store\StoreProduct;
use app\hgapi\model\store\StoreProductImg;
use app\hgapi\model\user\User;

class MarketApi extends Auth{

    /**
     * 拼团活动列表
     */
    public function spell_list(){
        $start = input('start');
        $limit = input('limit');
        $time = time();
        $list = MarketSpell::selSpellLimit(['start_time' => ['elt', $time], 'end_time' => ['egt', $time], 'status' => 1], $start, $limit);
        successCode($list);
    }

    /**
     * 拼团商品详情
     */
    public function spell_details(){
        $id = input('id'); //秒杀活动id
        $market = MarketSpell::getSpellInfo(['id' => $id]); //活动
        $product = StoreProduct::getProductInfo(['id' => $market['pid']], ''); //商品
        $spec = json_decode($product['spec'], true); //产品规格
        $can = [];
        foreach ($spec as $k => $v){
            foreach ($v as $key => $val){
                $can[$k]['spec'] = $key;
                $can[$k]['bei'] = $val['bei'];
                $can[$k]['value']['name'] = $val['name'];
                $can[$k]['value']['img'] = $val['img'];
            }
        }
        $product['can'] = $can;
        $product['value'] = json_decode($product['value'], true); //产品值
        $product['img'] = StoreProductImg::getImg($market['pid']); //商品图片
        $product['imgs'] = StoreProductImg::columnImg($market['pid']); //商品图片
        $count = StoreOrder::countOrder(['spell_id' => $market['id']]);
        $conduct = StoreOrder::conductOrder($market['id'], $market['spell_time'], $market['spell_number']); //未完成进行中的拼团
        if (!$market || $market['status'] != 1 || !$product || $product['status'] != 0 || $product['is_del'] != 0) errorCode('活动已结束或已下架');
        successCode(compact('market', 'product', 'count', 'conduct'));
    }

    /**
     * 拼团页面
     */
    public function spell_pin(){
        $id = input('id'); //订单id
        $order = StoreOrder::getOrder(['id' => $id], '');
        $market = MarketSpell::getSpellInfo(['id' => $order['spell_id']]); //活动
        $product = StoreProduct::getProductInfo(['id' => $market['pid']], ''); //商品
        $product['img'] = StoreProductImg::getImg($market['pid']); //商品图片
        $count = $market['spell_number'] - StoreOrder::countOrder(['spell_number' => $order['spell_number'], 'status' => 1]);
        $is_ok = 1;
        if (!$market || $market['status'] != 1 || !$product || $product['status'] != 0 || $product['is_del'] != 0) {
            $is_ok = 0;
        }elseif (time() > ($order['time'] + $market['spell_time'])){
            $is_ok = 0;
        };
        $pinkAll = StoreOrder::columnOrderPin($order['spell_number']);
        successCode(compact('order', 'market', 'product', 'count', 'is_ok', 'pinkAll'));
    }

    /**
     * 秒杀时间段
     */
    public function seckill_time(){
        $list = MarketSeckillTime::selTime(['status' => 1]);
        $time = $list['data'];
        $active = $list['active'];
        successCode(compact('time', 'active'));
    }

    /**
     * 秒杀活动列表
     */
    public function seckill_list(){
        $start = input('start');
        $limit = input('limit');
        $time = input('time');
        $date = MarketSeckillTime::getTimeInfo(['id' => $time, 'status' => 1]);
        if (!$date) errorCode('数据错误');
        $start_time = $date['start_time'];
        $end_time = $date['start_time'] + $date['continue_time'];
        $list = MarketSeckill::selSeckillLimit(['time_slot' => $time, 'start_time' => ['egt', $start_time], 'end_time' => ['egt', $end_time], 'status' => 1], $start, $limit);
        successCode($list);
    }

    /**
     * 秒杀活动详情
     */
    public function seckill_details(){
        $id = input('id'); //秒杀活动id
        $end_time = input('end_time'); //结束时间
        $market = MarketSeckill::getSeckillInfo(['id' => $id]); //活动
        $product = StoreProduct::getProductInfo(['id' => $market['pid']], ''); //商品
        $spec = json_decode($product['spec'], true); //产品规格
        $can = [];
        foreach ($spec as $k => $v){
            foreach ($v as $key => $val){
                $can[$k]['spec'] = $key;
                $can[$k]['bei'] = $val['bei'];
                $can[$k]['value']['name'] = $val['name'];
                $can[$k]['value']['img'] = $val['img'];
            }
        }
        $product['can'] = $can;
        $product['value'] = json_decode($product['value'], true); //产品值
        $product['imgs'] = StoreProductImg::columnImg($market['pid']); //商品图片
        if ($end_time < time()) errorCode('活动已结束');
        if (!$market || $market['status'] != 1 || !$product || $product['status'] != 0 || $product['is_del'] != 0) errorCode('活动已结束或已下架');
        successCode(compact('market', 'product'));
    }
}
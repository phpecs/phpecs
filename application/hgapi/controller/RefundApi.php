<?php
/**
 * PHPECS社交电商系统，使用thinkphp框架+MySQL数据库编写的小程序商城系统，基于LGPL协议开源授权
 * @package phpecs
 * @author qq(123865789)
 * @copyright 2018-2020 深圳塔灯网络科技有限公司
 * @version 2.0
 * @license http://www.phpecs.com/lgpl.html phpecs开源授权协议：GNU Lesser General Public License
 **/
namespace app\hgapi\controller;
use app\hgapi\model\store\StoreOrder;
use app\hgapi\model\store\StoreRefund;

class RefundApi extends Auth{

    /**
     * 申请退款
     */
    public function refund(){
        $id = input('id'); //订单id
        $uid = input('uid'); //用户id
        $reason = input('reason'); //退款原因
        //查询出属于自己的订单
        $re = StoreOrder::getOrder(['id' => $id, 'uid' => $uid, 'status' => 1], '');
        if ($re){
            $data = [
                'oid' => $id,
                'tui' => $re['money'] - $re['freight'] - $re['benefit'],
                'reason' => $reason,
                'time' => time(),
                'status' => 0,
            ];
            StoreRefund::add($data);
            StoreOrder::upField(['id' => $id, 'uid' => $uid, 'status' => 1], 'status', 5);
            successCode('申请成功');
        }else{
            errorCode('申请失败');
        }
    }
}
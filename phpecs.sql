/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.7.26 : Database - demodb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`demodb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `demodb`;

/*Table structure for table `phpecs_account` */

DROP TABLE IF EXISTS `phpecs_account`;

CREATE TABLE `phpecs_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL COMMENT '账号',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `role` text COMMENT '角色',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='管理员账号表';

/*Data for the table `phpecs_account` */

insert  into `phpecs_account`(`id`,`username`,`password`,`role`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','1');

/*Table structure for table `phpecs_account_power` */

DROP TABLE IF EXISTS `phpecs_account_power`;

CREATE TABLE `phpecs_account_power` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) DEFAULT NULL COMMENT '上级id',
  `power_name` varchar(30) DEFAULT NULL COMMENT '权限名称',
  `power_url` varchar(50) DEFAULT NULL COMMENT '权限地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COMMENT='权限表';

/*Data for the table `phpecs_account_power` */

insert  into `phpecs_account_power`(`id`,`classid`,`power_name`,`power_url`) values (1,0,'产品管理','#'),(2,1,'产品类别','product/category.html'),(3,1,'类别操作','product/category_operate.html'),(4,1,'添加分类','product/category_add.html'),(5,1,'修改分类','product/category_edit.html'),(6,1,'删除分类','product/category_del.html'),(7,1,'修改排序','product/sort_edit.html'),(8,1,'修改状态','product/status_edit.html'),(9,1,'商品管理','product/product.html'),(10,1,'商品上下架','product/up_and_down.html'),(11,1,'商品操作','product/product_operate.html'),(12,1,'商品添加','product/product_add.html'),(13,1,'商品修改','product/product_edit.html'),(14,1,'商品删除','product/product_del.html'),(15,1,'生成卡密','product/kalman.html'),(16,1,'商品二维码','product/sp_code.html'),(17,1,'订单列表','product/order.html'),(18,1,'订单详情','product/order_operate.html'),(19,1,'修改订单总价','product/order_edit.html'),(20,1,'添加订单备注','product/order_remark.html'),(21,1,'删除订单备注','product/remark_del.html'),(22,1,'订单发货','product/order_delivery.html'),(23,1,'退款列表','product/refund.html'),(24,1,'退款详情','product/refund_operate.html'),(25,1,'修改退款总额','product/refund_edit.html'),(26,1,'通过','product/refund_ok.html'),(27,1,'拒绝','product/refund_no.html'),(28,1,'退款','product/tui.html'),(29,1,'商品评价','product/product_evaluate.html'),(30,1,'评价操作','product/evaluate_operate.html'),(31,1,'评价添加','product/evaluate_add.html'),(32,1,'评价修改','product/evaluate_edit.html'),(33,1,'评价删除','product/evaluate_del.html'),(34,1,'优惠券','product/coupon.html'),(35,1,'优惠券处理','product/coupon_operate.html'),(36,1,'优惠券添加','product/coupon_add.html'),(37,1,'优惠券修改','product/coupon_edit.html'),(38,1,'使优惠券失效','product/coupon_shi.html'),(39,1,'商品多图上传','product/shopUpload.html'),(40,1,'普通图片上传','product/upload.html'),(41,1,'商品视频上传','product/videoUpload.html'),(42,1,'编辑器上传图片','product/editorUpload.html'),(43,1,'规格图片上传','product/specUpload.html'),(44,0,'主页','#'),(45,44,'主页','index/index.html'),(46,44,'控制台','index/console.html'),(47,0,'用户管理','#'),(48,47,'用户数据','user/index.html'),(49,47,'下级会员','user/lower.html'),(50,47,'业务员','user/business.html'),(51,47,'添加业务员','user/business_add.html'),(52,47,'更新业务员二维码','user/business_edit.html'),(53,47,'礼品商家','user/provide.html'),(54,47,'礼品商家审核操作','user/provide_operate.html'),(55,47,'礼品商家审核','user/provide_shenhe.html'),(56,47,'拒绝通过','user/provide_jujue.html'),(57,47,'引流商家','user/drainage.html'),(58,47,'引流商家审核操作','user/drainage_operate.html'),(59,47,'引流商家审核','user/drainage_shenhe.html'),(60,47,'批发会员','user/pifa.html'),(61,47,'批发会员审核操作','user/pifa_operate.html'),(62,47,'批发会员审核','user/pifa_shenhe.html'),(63,47,'图片上传','user/upload.html'),(64,0,'系统管理','#'),(65,64,'系统设置','system/site.html'),(66,64,'banner管理','system/banner.html'),(67,64,'banner操作','system/banner_operate.html'),(68,64,'banner添加','system/banner_add.html'),(69,64,'banner修改','system/banner_edit.html'),(70,64,'banner删除','system/banner_del.html'),(71,64,'修改banner排序','system/sort_edit.html'),(72,64,'新品上新','system/foot.html'),(73,64,'新品操作','system/foot_operate.html'),(74,64,'新品添加','system/foot_add.html'),(75,64,'新品修改','system/foot_edit.html'),(76,64,'foot玩法删除','system/foot_del.html'),(77,64,'修改foot玩法排序','system/foot_sort.html'),(78,64,'link管理','system/link.html'),(79,64,'link操作','system/link_operate.html'),(80,64,'link添加','system/link_add.html'),(81,64,'link修改','system/link_edit.html'),(82,64,'link删除','system/link_del.html'),(83,64,'修改link排序','system/link_sort.html'),(84,64,'微信支付设置','system/secret.html'),(85,64,'图片上传','system/upload.html'),(86,0,'权限管理','#'),(87,86,'管理员列表','rbac/admin.html'),(88,86,'管理员操作','rbac/admin_operate.html'),(89,86,'管理员添加','rbac/admin_add.html'),(90,86,'管理员修改','rbac/admin_edit.html'),(91,86,'管理员删除','rbac/admin_del.html'),(92,86,'角色管理','rbac/role.html'),(93,86,'角色操作','rbac/role_operate.html'),(94,86,'角色添加','rbac/role_add.html'),(95,86,'角色修改','rbac/role_edit.html'),(96,86,'角色删除','rbac/role_del.html'),(97,86,'权限列表','rbac/access.html'),(98,86,'权限操作','rbac/access_operate.html'),(99,86,'添加权限','rbac/access_add.html'),(100,86,'修改权限','rbac/access_edit.html'),(101,86,'删除权限','rbac/access_del.html'),(102,86,'个人信息修改','rbac/personal.html'),(103,0,'活动管理','#'),(104,103,'充值活动','activity/recharge.html'),(105,0,'财务管理','#'),(106,105,'提现管理','cash/cash.html'),(107,105,'提现审核','cash/cash_operate.html'),(108,105,'佣金记录','cash/gold.html'),(109,105,'核销记录','cash/he.html'),(110,0,'大礼包管理','#'),(111,110,'礼包数据','gift/index.html'),(112,110,'礼品操作','gift/gift_operate.html'),(113,110,'礼品添加','gift/gift_add.html'),(114,110,'礼品修改','gift/gift_edit.html'),(115,110,'礼品删除','gift/gift_del.html'),(116,110,'生成核销码','gift/create.html'),(117,110,'组合大礼包','gift/package.html'),(118,110,'大礼包操作','gift/package_operate.html'),(119,110,'大礼包添加','gift/package_add.html'),(120,110,'获取我的大礼包二维码','gift/package_img.html'),(121,110,'大礼包修改','gift/package_edit.html'),(122,110,'大礼包删除','gift/package_del.html'),(123,110,'修改大礼包排序','gift/package_sort.html'),(124,0,'帮助中心','#'),(125,124,'类别','help/help_category.html'),(126,124,'类别操作','help/help_category_operate.html'),(127,124,'添加分类','help/help_category_add.html'),(128,124,'修改分类','help/help_category_edit.html'),(129,124,'删除分类','help/help_category_del.html'),(130,124,'修改排序','help/sort_edit.html'),(131,124,'修改状态','help/status_edit.html'),(132,124,'帮助中心','help/help.html'),(133,124,'帮助操作','help/help_operate.html'),(134,124,'添加内容','help/help_add.html'),(135,124,'修改内容','help/help_edit.html'),(136,124,'删除内容','help/help_del.html'),(137,124,'图片上传','help/upload.html'),(138,64,'首页活动','system/activity.html'),(139,64,'活动添加','system/activity_add.html'),(140,64,'活动操作','system/activity_operate.html'),(141,64,'活动修改','system/activity_edit.html'),(142,64,'活动删除','system/activity_del.html'),(143,64,'修改活动排序','system/activity_sort.html'),(144,110,'上传图片','gift/upload.html'),(145,1,'查出所有商家','product/trader.html'),(146,103,'打卡','activity/clock.html'),(147,103,'打卡操作页面','activity/clock_operate.html'),(148,103,'打卡活动添加','activity/clock_add.html'),(149,103,'修改','activity/clock_edit.html'),(150,103,'关闭报名','activity/stop.html'),(151,103,'开关活动','activity/start_close.html'),(152,103,'图片上传','activity/upload.html'),(153,1,'收货','product/order_collect'),(154,47,'批发操作','user/pf.html');

/*Table structure for table `phpecs_account_role` */

DROP TABLE IF EXISTS `phpecs_account_role`;

CREATE TABLE `phpecs_account_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) DEFAULT NULL COMMENT '角色名称',
  `power` text COMMENT '角色权限 ,隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色权限表';

/*Data for the table `phpecs_account_role` */

insert  into `phpecs_account_role`(`id`,`role`,`power`) values (1,'超级管理员','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,138,139,140,141,142,143,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,144,124,125,126,127,128,129,130,131,132,133,134,135,136,137');

/*Table structure for table `phpecs_activity` */

DROP TABLE IF EXISTS `phpecs_activity`;

CREATE TABLE `phpecs_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL COMMENT '活动标题',
  `synopsis` varchar(20) DEFAULT NULL COMMENT '活动简介',
  `img` varchar(225) DEFAULT NULL COMMENT '图片地址',
  `link` varchar(225) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '链接打开方式 1普通 2tab',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '开关 0关闭 1开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='首页活动图';

/*Data for the table `phpecs_activity` */

/*Table structure for table `phpecs_activity_clock` */

DROP TABLE IF EXISTS `phpecs_activity_clock`;

CREATE TABLE `phpecs_activity_clock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '活动名称',
  `status` tinyint(1) DEFAULT NULL COMMENT '活动状态 0未开启 1开启 2已结束',
  `tian` int(11) DEFAULT NULL COMMENT '打卡时间(天)',
  `pid` int(11) DEFAULT NULL COMMENT '打卡商品id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `freight` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `img` varchar(225) DEFAULT NULL COMMENT '打卡海报',
  `color` varchar(10) DEFAULT NULL COMMENT '昵称颜色',
  `rule` tinyint(1) DEFAULT NULL COMMENT '打卡规则 1早上打卡 2早晚都要打卡',
  `remind` varchar(225) DEFAULT NULL COMMENT '挑战说明',
  `explain` text COMMENT '规则说明',
  `bao` tinyint(11) DEFAULT NULL COMMENT '报名 0关闭 1开启',
  `num` int(11) DEFAULT NULL COMMENT '参与人数',
  `tui` tinyint(1) DEFAULT NULL COMMENT '是否退回佣金',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='打卡活动';

/*Data for the table `phpecs_activity_clock` */

/*Table structure for table `phpecs_activity_clock_qian` */

DROP TABLE IF EXISTS `phpecs_activity_clock_qian`;

CREATE TABLE `phpecs_activity_clock_qian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ch_id` int(11) DEFAULT NULL COMMENT '关联记录',
  `Year` int(11) DEFAULT NULL COMMENT '年',
  `Month` int(11) DEFAULT NULL COMMENT '月',
  `Date` int(11) DEFAULT NULL COMMENT '日',
  `time` int(11) DEFAULT NULL COMMENT '时间',
  `zao` tinyint(1) DEFAULT '0' COMMENT '早上是否打卡',
  `wan` tinyint(1) DEFAULT '0' COMMENT '晚上是否打卡',
  `use` tinyint(1) DEFAULT '0' COMMENT '是否重签的',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='打卡活动签到记录表';

/*Data for the table `phpecs_activity_clock_qian` */

/*Table structure for table `phpecs_activity_clock_record` */

DROP TABLE IF EXISTS `phpecs_activity_clock_record`;

CREATE TABLE `phpecs_activity_clock_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `oid` int(11) DEFAULT NULL COMMENT '订单id',
  `hid` int(11) DEFAULT NULL COMMENT '核销用户id',
  `yid` int(11) DEFAULT NULL COMMENT '商家id',
  `time` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='活动购买记录';

/*Data for the table `phpecs_activity_clock_record` */

/*Table structure for table `phpecs_activity_list` */

DROP TABLE IF EXISTS `phpecs_activity_list`;

CREATE TABLE `phpecs_activity_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '活动名称',
  `image` varchar(225) DEFAULT NULL COMMENT '活动封面',
  `banner` varchar(225) DEFAULT NULL COMMENT '活动banner',
  `type` tinyint(1) DEFAULT NULL COMMENT '活动类型 1抽奖',
  `action` tinyint(1) DEFAULT NULL COMMENT '活动条件 1购买商品 2消耗积分',
  `pid` text COMMENT '相关商品id',
  `integral` decimal(10,2) DEFAULT NULL COMMENT '消耗积分数量',
  `draw` text COMMENT '活动选项',
  `content` text COMMENT '活动简介',
  `status` tinyint(4) DEFAULT '1' COMMENT '活动状态 0下线 1正常',
  `time` int(11) DEFAULT NULL COMMENT '活动添加时间',
  `num` int(11) DEFAULT '0' COMMENT '参与次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='活动列表';

/*Data for the table `phpecs_activity_list` */

/*Table structure for table `phpecs_activity_num` */

DROP TABLE IF EXISTS `phpecs_activity_num`;

CREATE TABLE `phpecs_activity_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `aid` int(11) DEFAULT NULL COMMENT '活动id',
  `name` varchar(50) DEFAULT NULL COMMENT '中奖礼品名称',
  `result` tinyint(1) DEFAULT NULL COMMENT '中奖结果',
  `time` int(11) DEFAULT NULL COMMENT '中奖时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='中奖记录表';

/*Data for the table `phpecs_activity_num` */

/*Table structure for table `phpecs_activity_recharge` */

DROP TABLE IF EXISTS `phpecs_activity_recharge`;

CREATE TABLE `phpecs_activity_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(11) DEFAULT NULL COMMENT '是否开启 0不开启 1开启',
  `name` varchar(225) DEFAULT NULL COMMENT '活动名称',
  `start` int(11) DEFAULT NULL COMMENT '活动起始时间',
  `end` int(11) DEFAULT NULL COMMENT '活动结束时间',
  `lowest` decimal(10,2) DEFAULT NULL COMMENT '最低充值金额返现',
  `give` decimal(10,2) DEFAULT NULL COMMENT '赠送余额比例',
  `cashback` decimal(10,2) DEFAULT NULL COMMENT '返现余额比例',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='充值活动表';

/*Data for the table `phpecs_activity_recharge` */

insert  into `phpecs_activity_recharge`(`id`,`status`,`name`,`start`,`end`,`lowest`,`give`,`cashback`) values (1,NULL,'充值',1559836800,1564502400,'10.00','100.00','100.00');

/*Table structure for table `phpecs_ad` */

DROP TABLE IF EXISTS `phpecs_ad`;

CREATE TABLE `phpecs_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(225) DEFAULT NULL COMMENT '图片',
  `link` varchar(225) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '链接打开方式 1普通 2tab',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否显示 0不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='广告表';

/*Data for the table `phpecs_ad` */

insert  into `phpecs_ad`(`id`,`img`,`link`,`link_column`,`sort`,`status`) values (1,'site/20190904/36ad99335616fa2eb59ea4df6dd93bd8.png','/pages/common/content_page/index/index?id=10',1,1,1),(2,'site/20190904/101dee2da5f7f9bc8138e22181e4aa00.jpg','/pages/common/content_page/index/index?id=8',1,2,1),(3,'site/20190816/f2e0744cfac9aeaeb3cb01672e70c479.jpg','/pages/common/content_page/index/index?id=5',0,3,1);

/*Table structure for table `phpecs_banner` */

DROP TABLE IF EXISTS `phpecs_banner`;

CREATE TABLE `phpecs_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(225) DEFAULT NULL COMMENT '图片',
  `link` varchar(225) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '链接打开方式 1普通 2tab',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否显示 0不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='banner';

/*Data for the table `phpecs_banner` */

insert  into `phpecs_banner`(`id`,`img`,`link`,`link_column`,`sort`,`status`) values (6,'site/20190905/7deb529ee010f29488e0c7edbe4a0bf2.jpg','/pages/shop/product/product?id=28',0,1,1),(7,'site/20190905/64e017ef2cb442f8d08693dcd44ff409.jpg','/pages/shop/product/product?id=15',0,2,1),(9,'site/20190905/23b1861b33f795b4e743e5902d4fe033.jpg','/pages/user/quan_operate/quan_operate?id=1',1,1,1);

/*Table structure for table `phpecs_content_page` */

DROP TABLE IF EXISTS `phpecs_content_page`;

CREATE TABLE `phpecs_content_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '页面名称',
  `background_image` varchar(225) DEFAULT NULL COMMENT '页面背景',
  `background_color` varchar(30) DEFAULT NULL COMMENT '背景颜色',
  `form` tinyint(1) DEFAULT '1' COMMENT '展现形式 1单列表 2双列表 3自定义',
  `type` tinyint(1) DEFAULT '1' COMMENT '调用数据 1砍价 2团购 3秒杀 4优惠券商品 5签到商品',
  `coupon` int(11) DEFAULT NULL COMMENT '调用优惠券',
  `shop` text COMMENT '调用多条商品',
  `num` int(11) DEFAULT NULL COMMENT '显示总数',
  `content` text COMMENT '自定义页面数据',
  `enter_popup_open` tinyint(1) DEFAULT '0' COMMENT '进入页面弹窗开关 1开启 0关闭',
  `enter_popup_page` int(11) DEFAULT NULL COMMENT '进入页面弹窗',
  `browse_popup_open` tinyint(1) DEFAULT '0' COMMENT '浏览页面弹窗开关 1开启 0关闭',
  `browse_popup_miao` int(11) DEFAULT NULL COMMENT '浏览秒数',
  `browse_popup_page` int(11) DEFAULT NULL COMMENT '浏览页面弹窗',
  `rule` text COMMENT '当前页面规则',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='活动承接页面';

/*Data for the table `phpecs_content_page` */

/*Table structure for table `phpecs_foot` */

DROP TABLE IF EXISTS `phpecs_foot`;

CREATE TABLE `phpecs_foot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '区块名称',
  `type` tinyint(1) DEFAULT NULL COMMENT '显示方法 1一行1条 2一行2条 3一行三条 4横向滚动',
  `bg` varchar(225) DEFAULT NULL COMMENT '背景图',
  `link` varchar(225) DEFAULT NULL COMMENT '背景图链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '链接打开方式 1普通 2tab',
  `model` tinyint(1) DEFAULT NULL COMMENT '选择显示商品属性 1推荐 2精品 3新品上新',
  `category` int(11) DEFAULT NULL COMMENT '商品分类',
  `num` int(11) DEFAULT NULL COMMENT '显示条数',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否显示 0不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='底部玩法';

/*Data for the table `phpecs_foot` */

insert  into `phpecs_foot`(`id`,`name`,`type`,`bg`,`link`,`link_column`,`model`,`category`,`num`,`sort`,`status`) values (1,'精品推荐',1,'site/20190629/b0f8c85266d89118886754703770e4ff.png','/pages/common/content_page/index/index?id=9',1,2,NULL,6,2,1),(2,'新品推荐',2,'site/20190629/5f61561c2175b6581d71655e40e92d15.png',NULL,1,3,NULL,6,4,1),(3,'促销',1,'site/20190629/fc50b8f222514756a16e21e7487b51de.png','/pages/common/content_page/index/index?id=7',1,1,NULL,4,3,1),(8,'活动区域',1,'','',0,12,4,3,5,1),(9,'全返专区',4,'site/20190905/874470650d7e3e3e2322910a370af766.jpg','/pages/common/content_page/index/index?id=2',1,1,NULL,100,1,1),(10,'日用品',1,'site/20190908/ce6d29bfe2e434ce53138be7821b2cf3.jpg','/pages/shop/index/index',2,12,3,10,6,1);

/*Table structure for table `phpecs_goods_gift` */

DROP TABLE IF EXISTS `phpecs_goods_gift`;

CREATE TABLE `phpecs_goods_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '提供礼品商家',
  `pid` int(11) DEFAULT NULL COMMENT '选择的产品',
  `lipin` varchar(225) DEFAULT NULL COMMENT '礼品名称',
  `img` varchar(225) DEFAULT NULL COMMENT '礼品封面',
  `num` int(11) DEFAULT NULL COMMENT '礼品数量',
  `stage` int(11) DEFAULT NULL COMMENT '期数',
  `ge` int(11) DEFAULT NULL COMMENT '每期间隔天数',
  `mode` int(11) DEFAULT NULL COMMENT '领取方法 0到店免费领取 1到店消费领取 2线上充值领取',
  `money` float DEFAULT NULL COMMENT '需要消费金额',
  `address` varchar(225) DEFAULT NULL COMMENT '商家地址',
  `gold` decimal(10,2) DEFAULT NULL COMMENT '每单佣金',
  `price` decimal(10,2) DEFAULT NULL COMMENT '礼品价值',
  `latitude` float DEFAULT NULL COMMENT '纬度',
  `longitude` float DEFAULT NULL COMMENT '经度',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0待审核 1审核通过 2拒绝通过 3待支付押金',
  `beizhu` varchar(225) DEFAULT NULL COMMENT '备注',
  `is_zu` tinyint(1) DEFAULT '0' COMMENT '是否允许别人组合 0不允许 1允许',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='礼品表';

/*Data for the table `phpecs_goods_gift` */

/*Table structure for table `phpecs_goods_gift_code` */

DROP TABLE IF EXISTS `phpecs_goods_gift_code`;

CREATE TABLE `phpecs_goods_gift_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lid` int(11) DEFAULT NULL COMMENT '所属礼品',
  `number` varchar(20) DEFAULT NULL COMMENT '核销码',
  `stage` int(11) DEFAULT NULL COMMENT '期数',
  `pi` int(11) DEFAULT NULL COMMENT '批数',
  `status` tinyint(1) DEFAULT NULL COMMENT '领取状态 0待领取 1已领取 2已失效',
  `time` int(11) DEFAULT NULL COMMENT '领取时间',
  `uid` int(11) DEFAULT NULL COMMENT '领取人',
  `yid` int(11) DEFAULT NULL COMMENT '引流商家',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='核销码表';

/*Data for the table `phpecs_goods_gift_code` */

/*Table structure for table `phpecs_goods_gift_code_record` */

DROP TABLE IF EXISTS `phpecs_goods_gift_code_record`;

CREATE TABLE `phpecs_goods_gift_code_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '领取人',
  `lid` int(11) DEFAULT NULL COMMENT '礼品id',
  `stage` int(11) DEFAULT NULL COMMENT '期数',
  `l_time` int(11) DEFAULT NULL COMMENT '领取时间',
  `h_time` int(11) DEFAULT NULL COMMENT '核销时间',
  `hid` int(11) DEFAULT NULL COMMENT '核销用户',
  `yid` int(11) DEFAULT NULL COMMENT '引流商家',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='核销记录表';

/*Data for the table `phpecs_goods_gift_code_record` */

/*Table structure for table `phpecs_goods_package` */

DROP TABLE IF EXISTS `phpecs_goods_package`;

CREATE TABLE `phpecs_goods_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zid` int(11) DEFAULT '0' COMMENT '组合人id 0为平台',
  `name` varchar(225) DEFAULT NULL COMMENT '大礼包名称',
  `img` varchar(225) DEFAULT NULL COMMENT '大礼包图片',
  `start` int(11) DEFAULT NULL COMMENT '开始日期',
  `end` int(11) DEFAULT NULL COMMENT '结束日期',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `gid` text COMMENT '选择的礼品 ,隔开',
  `money` decimal(10,2) DEFAULT NULL COMMENT '支付金额',
  `type` tinyint(1) DEFAULT NULL COMMENT '状态 1开启领取 0关闭领取',
  `status` tinyint(1) DEFAULT NULL COMMENT '审核状态 0未审核 1审核通过',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='大礼包表';

/*Data for the table `phpecs_goods_package` */

/*Table structure for table `phpecs_help` */

DROP TABLE IF EXISTS `phpecs_help`;

CREATE TABLE `phpecs_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL COMMENT '分类id',
  `title` varchar(225) DEFAULT NULL COMMENT '标题',
  `img` varchar(225) DEFAULT NULL COMMENT '文章封面',
  `jian` varchar(225) DEFAULT NULL COMMENT '文章简介',
  `content` text COMMENT '内容',
  `time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='帮助表';

/*Data for the table `phpecs_help` */

insert  into `phpecs_help`(`id`,`cid`,`title`,`img`,`jian`,`content`,`time`) values (1,2,'冲值失败','help/20190429/56804fc3581e94fc5426e5834f5b29a0.jpg','充值失败怎么办','<p>充值失败怎么办</p>',1556520816);

/*Table structure for table `phpecs_help_category` */

DROP TABLE IF EXISTS `phpecs_help_category`;

CREATE TABLE `phpecs_help_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `pid` int(11) DEFAULT NULL COMMENT '上级id',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否显示 0不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='帮助分类';

/*Data for the table `phpecs_help_category` */

insert  into `phpecs_help_category`(`id`,`name`,`pid`,`sort`,`status`) values (1,'常见问题',0,2,1),(2,'售后问题',0,1,1);

/*Table structure for table `phpecs_link` */

DROP TABLE IF EXISTS `phpecs_link`;

CREATE TABLE `phpecs_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL COMMENT '标题',
  `img` varchar(225) DEFAULT NULL COMMENT '图片',
  `link` varchar(225) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '链接打开方式 1普通 2tab',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='首页链接';

/*Data for the table `phpecs_link` */

insert  into `phpecs_link`(`id`,`title`,`img`,`link`,`link_column`,`sort`,`status`) values (1,'新手指南','site/20190430/eb2d741a405b112a3eff6ea816f0dbac.png','/pages/help/help',1,1,1),(2,'会员中心','site/20190430/99f6bbcbd2fd248cbf8f3fc3f5af9c77.png','/pages/user/index/index',2,4,1),(3,'免费拿','site/20190430/b7ce342e27a4dca3e84985835d1c9c8e.png','/pages/common/content_page/index/index?id=6',1,5,1),(4,'我的订单','site/20190430/8fdccb5790d3a7dec6c6b44d1e20e1e9.png','/pages/shop/order/order',1,2,1),(5,'如何获客','site/20190614/aaaf3d490b400d31b29b2796317a991c.png','/pages/help_operate/help_operate?id=1',0,3,1),(6,'9.9元专区','site/20190905/1aaf4e69e2354a6ed24c5aa182b9e5b4.png','/pages/common/content_page/index/index?id=4',1,6,1),(7,'签到免费拿','site/20190614/8d54ccba828f739b5a70536d4307350b.png','/pages/shop/show/show',1,7,1),(8,'抽个奖','site/20190629/ab4a03df6661d1b63d15455bf2f88dc6.png','/pages/shop/show/show',1,8,1);

/*Table structure for table `phpecs_market_back` */

DROP TABLE IF EXISTS `phpecs_market_back`;

CREATE TABLE `phpecs_market_back` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '全返活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '全返活动商品',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `back_cost` decimal(10,2) DEFAULT NULL COMMENT '返现金额',
  `status` tinyint(1) DEFAULT NULL COMMENT '活动状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='返现活动';

/*Data for the table `phpecs_market_back` */

/*Table structure for table `phpecs_market_bargain` */

DROP TABLE IF EXISTS `phpecs_market_bargain`;

CREATE TABLE `phpecs_market_bargain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '砍价活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '砍价活动商品',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `bottom_price` decimal(10,2) DEFAULT NULL COMMENT '最低价格',
  `maximum` decimal(10,2) DEFAULT NULL COMMENT '单次砍价最大金额',
  `minimum` decimal(10,2) DEFAULT NULL COMMENT '单次砍价最小金额',
  `number` int(11) DEFAULT NULL COMMENT '单次砍价次数',
  `complete_num` int(11) DEFAULT NULL COMMENT '最多几人砍完',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `volume` int(11) DEFAULT NULL COMMENT '销量',
  `once_num` int(11) DEFAULT NULL COMMENT '单次允许购买数量',
  `postage_open` tinyint(1) DEFAULT '1' COMMENT '邮费开关 1开启 0关闭',
  `postage` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `postage_alias` varchar(20) DEFAULT NULL COMMENT '邮费别称',
  `envelopes_open` tinyint(1) DEFAULT '0' COMMENT '间断红包砍',
  `envelopes_prev_rate` decimal(10,2) DEFAULT NULL COMMENT '前十次间隔频率',
  `envelopes_prev_money` decimal(10,2) DEFAULT NULL COMMENT '前十次砍剩余的百分比',
  `envelopes_next_rate` decimal(10,2) DEFAULT NULL COMMENT '十次后间隔频率',
  `envelopes_next_money` decimal(10,2) DEFAULT NULL COMMENT '十次后砍剩余的百分比',
  `sword_open` tinyint(1) DEFAULT '0' COMMENT '惊喜宝刀砍开关',
  `sword_money` decimal(10,2) DEFAULT NULL COMMENT '砍剩余的百分比',
  `status` tinyint(1) DEFAULT NULL COMMENT '活动状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='砍价表';

/*Data for the table `phpecs_market_bargain` */

/*Table structure for table `phpecs_market_bargain_record` */

DROP TABLE IF EXISTS `phpecs_market_bargain_record`;

CREATE TABLE `phpecs_market_bargain_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '所属用户',
  `bargain_uid` int(11) DEFAULT '0' COMMENT '砍价用户',
  `bargain_id` int(11) DEFAULT NULL COMMENT '砍价活动',
  `bargain_money` decimal(10,2) DEFAULT NULL COMMENT '砍价金额',
  `type` tinyint(1) DEFAULT '1' COMMENT '砍价类型 1自己砍价 2她人帮砍',
  `sword` tinyint(1) DEFAULT '0' COMMENT '惊喜宝刀砍 1是 0否',
  `envelopes` tinyint(1) DEFAULT '0' COMMENT '红包砍 1是 0否',
  `time` int(11) DEFAULT NULL COMMENT '砍价时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='砍价记录';

/*Data for the table `phpecs_market_bargain_record` */

/*Table structure for table `phpecs_market_likes` */

DROP TABLE IF EXISTS `phpecs_market_likes`;

CREATE TABLE `phpecs_market_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '活动商品',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `num` int(11) DEFAULT NULL COMMENT '需要点赞数量',
  `likes_time` int(11) DEFAULT NULL COMMENT '点赞时间',
  `postage_open` tinyint(1) DEFAULT '1' COMMENT '邮费开关 1开启 0关闭',
  `postage` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `postage_alias` varchar(20) DEFAULT NULL COMMENT '邮费别称',
  `status` tinyint(1) DEFAULT '1' COMMENT '活动状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='点赞活动';

/*Data for the table `phpecs_market_likes` */

/*Table structure for table `phpecs_market_likes_record` */

DROP TABLE IF EXISTS `phpecs_market_likes_record`;

CREATE TABLE `phpecs_market_likes_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '所属用户',
  `likes_uid` int(11) DEFAULT NULL COMMENT '点赞用户',
  `likes_id` int(11) DEFAULT NULL COMMENT '点赞活动',
  `time` int(11) DEFAULT NULL COMMENT '点赞时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='点赞记录表';

/*Data for the table `phpecs_market_likes_record` */

/*Table structure for table `phpecs_market_purchase` */

DROP TABLE IF EXISTS `phpecs_market_purchase`;

CREATE TABLE `phpecs_market_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '商品id',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `purchase` decimal(10,2) DEFAULT NULL COMMENT '现价',
  `type` tinyint(1) DEFAULT '1' COMMENT '可购买用户类型 1新人专属 2所有用户',
  `postage_open` tinyint(1) DEFAULT '1' COMMENT '邮费开关 1开启 0关闭',
  `postage` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `status` tinyint(1) DEFAULT '1' COMMENT '活动开关 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='N元购活动';

/*Data for the table `phpecs_market_purchase` */

/*Table structure for table `phpecs_market_seckill` */

DROP TABLE IF EXISTS `phpecs_market_seckill`;

CREATE TABLE `phpecs_market_seckill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '秒杀活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '秒杀活动商品',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `time_slot` int(11) DEFAULT NULL COMMENT '时间段',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `price_spike` decimal(10,2) DEFAULT NULL COMMENT '秒杀价',
  `stock` int(11) DEFAULT '0' COMMENT '秒杀库存',
  `volume` int(11) DEFAULT '0' COMMENT '销量',
  `once_num` int(11) DEFAULT '1' COMMENT '一次最大购买次数',
  `postage_open` tinyint(1) DEFAULT '1' COMMENT '邮费开关 1开启 0关闭',
  `postage` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `status` tinyint(1) DEFAULT '1' COMMENT '活动状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='秒杀活动表';

/*Data for the table `phpecs_market_seckill` */

/*Table structure for table `phpecs_market_seckill_time` */

DROP TABLE IF EXISTS `phpecs_market_seckill_time`;

CREATE TABLE `phpecs_market_seckill_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间段',
  `continue_time` int(11) DEFAULT NULL COMMENT '持续时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否开启 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='秒杀时间设置表';

/*Data for the table `phpecs_market_seckill_time` */

/*Table structure for table `phpecs_market_spell` */

DROP TABLE IF EXISTS `phpecs_market_spell`;

CREATE TABLE `phpecs_market_spell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '拼团活动名称',
  `pid` int(11) DEFAULT NULL COMMENT '拼团活动商品',
  `start_time` int(11) DEFAULT NULL COMMENT '开始时间',
  `end_time` int(11) DEFAULT NULL COMMENT '结束时间',
  `original_cost` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `leader_price` decimal(10,2) DEFAULT NULL COMMENT '发起拼团人价格',
  `spell_price` decimal(10,2) DEFAULT NULL COMMENT '拼团价格',
  `spell_number` int(11) DEFAULT NULL COMMENT '拼团人数',
  `spell_time` int(11) DEFAULT NULL COMMENT '拼团时间',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `volume` int(11) DEFAULT NULL COMMENT '销量',
  `postage_open` tinyint(1) DEFAULT '1' COMMENT '邮费开关 1开启 0关闭',
  `postage` decimal(10,2) DEFAULT NULL COMMENT '邮费',
  `status` tinyint(1) DEFAULT '1' COMMENT '活动状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='拼团活动表';

/*Data for the table `phpecs_market_spell` */

/*Table structure for table `phpecs_message` */

DROP TABLE IF EXISTS `phpecs_message`;

CREATE TABLE `phpecs_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `img` varchar(225) DEFAULT NULL COMMENT '图标',
  `describe` varchar(225) DEFAULT NULL COMMENT '简短描述',
  `link` varchar(50) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '打开方式 1普通页面 2tab页',
  `obj` tinyint(1) DEFAULT '1' COMMENT '发送对象 1全部 2指定用户',
  `user` text COMMENT '指定用户',
  `time` int(11) DEFAULT NULL COMMENT '发送时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0未发布 1已发布',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='消息推送';

/*Data for the table `phpecs_message` */

/*Table structure for table `phpecs_page` */

DROP TABLE IF EXISTS `phpecs_page`;

CREATE TABLE `phpecs_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark` varchar(50) DEFAULT NULL COMMENT '标识',
  `link` varchar(225) DEFAULT NULL COMMENT '当前页面链接',
  `c_link` varchar(225) DEFAULT NULL COMMENT '内容页面链接',
  `column` tinyint(1) DEFAULT '1' COMMENT '1普通页面 2tab页面',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='小程序页面表';

/*Data for the table `phpecs_page` */

insert  into `phpecs_page`(`id`,`mark`,`link`,`c_link`,`column`,`sort`) values (1,'首页','/pages/index/index',NULL,2,1),(2,'商品中心','/pages/shop/index/index','/pages/shop/product/product',2,2),(3,'会员中心','/pages/user/index/index',NULL,2,3),(4,'帮助中心','/pages/help/help','/pages/help_operate/help_operate',1,4),(5,'订单页面','/pages/shop/order/order',NULL,1,5),(6,'打卡活动','/pages/shop/show/show','/pages/shop/clock/clock',1,6),(7,'推广中心','/pages/user/leaguer/leaguer',NULL,1,7),(8,'优惠券','/pages/user/quan/quan','/pages/user/quan_operate/quan_operate',1,8),(18,'承接页','','/pages/common/content_page/index/index',1,9),(12,'商家中心','/pages/store_center/center/center',NULL,1,10),(21,'我的大礼包','/pages/user/preferential/preferential',NULL,1,11);

/*Table structure for table `phpecs_popup` */

DROP TABLE IF EXISTS `phpecs_popup`;

CREATE TABLE `phpecs_popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '弹出名称',
  `img` varchar(225) DEFAULT NULL COMMENT '图片',
  `link` varchar(225) DEFAULT NULL COMMENT '链接',
  `link_column` tinyint(1) DEFAULT '1' COMMENT '打开方式 1普通页面 2tab页面',
  `object` tinyint(1) DEFAULT '1' COMMENT '弹出对象 1新用户 2所有用户',
  `rate` tinyint(1) DEFAULT '1' COMMENT '弹出频率 1销毁小程序进程后进入的每一次 2确定链接未被点击一直弹',
  `remarks` varchar(225) DEFAULT NULL COMMENT '备注',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态 1开启 0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='弹窗集合';

/*Data for the table `phpecs_popup` */

/*Table structure for table `phpecs_popup_record` */

DROP TABLE IF EXISTS `phpecs_popup_record`;

CREATE TABLE `phpecs_popup_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '点击用户',
  `popup` int(11) DEFAULT NULL COMMENT '点击弹窗',
  `time` int(11) DEFAULT NULL COMMENT '点击时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='点击弹窗记录';

/*Data for the table `phpecs_popup_record` */

/*Table structure for table `phpecs_secret` */

DROP TABLE IF EXISTS `phpecs_secret`;

CREATE TABLE `phpecs_secret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appid` varchar(30) DEFAULT NULL COMMENT '小程序appid',
  `secret_key` varchar(50) DEFAULT NULL COMMENT '小程序密钥',
  `merchant` varchar(30) DEFAULT NULL COMMENT '商户号',
  `merchant_key` varchar(50) DEFAULT NULL COMMENT '商户号秘钥',
  `certificate` varchar(225) DEFAULT NULL COMMENT '支付证书',
  `certificate_key` varchar(225) DEFAULT NULL COMMENT '支付秘钥',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信秘钥';

/*Data for the table `phpecs_secret` */

insert  into `phpecs_secret`(`id`,`appid`,`secret_key`,`merchant`,`merchant_key`,`certificate`,`certificate_key`) values (1,'','','','','','');

/*Table structure for table `phpecs_site` */

DROP TABLE IF EXISTS `phpecs_site`;

CREATE TABLE `phpecs_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '网站title',
  `logo` varchar(225) DEFAULT NULL COMMENT 'logo',
  `ad` varchar(225) DEFAULT NULL COMMENT '首页广告',
  `tui1` varchar(225) DEFAULT NULL COMMENT '推广海报1',
  `tui2` varchar(225) DEFAULT NULL COMMENT '推广海报2',
  `tui3` varchar(225) DEFAULT NULL COMMENT '推广海报3',
  `color` varchar(10) DEFAULT NULL COMMENT '昵称颜色',
  `ratio` decimal(10,2) DEFAULT NULL COMMENT '推广下级购买奖励佣金比例 (%)',
  `reward` decimal(10,2) DEFAULT NULL COMMENT '核销引流商家奖励百分比',
  `reward2` decimal(10,2) DEFAULT NULL COMMENT '核销业务员奖励百分比',
  `give` decimal(10,2) DEFAULT NULL COMMENT '赠送余额使用比例',
  `cashback` decimal(10,2) DEFAULT NULL COMMENT '返现余额使用比例',
  `tel` varchar(30) DEFAULT NULL COMMENT '客服电话',
  `gong` varchar(225) DEFAULT NULL COMMENT '公告',
  `huo` text COMMENT '获客页面',
  `yin` text COMMENT '引流页面',
  `is_fei` tinyint(1) DEFAULT '0' COMMENT '引流商家是否可设置价格',
  `open` tinyint(1) DEFAULT '1' COMMENT '充值开关 1开启 0关闭',
  `retail_open` tinyint(1) DEFAULT '0' COMMENT '分销开关 1开启 0关闭',
  `retail_type` tinyint(1) DEFAULT '1' COMMENT '分销类型 1指定分销 2全民分销',
  `popup_open` tinyint(1) DEFAULT '0' COMMENT '弹窗开关 1开启 0关闭',
  `popup_page` int(11) DEFAULT NULL COMMENT '弹出页面',
  `banner_open` tinyint(1) DEFAULT '1' COMMENT '首页横幅区块开关',
  `nav_open` tinyint(1) DEFAULT '1' COMMENT '首页导航区块开关',
  `ad_open` tinyint(1) DEFAULT '1' COMMENT '首页广告区块开关',
  `activity_open` tinyint(1) DEFAULT '1' COMMENT '首页活动区块开关',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='网站设置';

/*Data for the table `phpecs_site` */

insert  into `phpecs_site`(`id`,`title`,`logo`,`ad`,`tui1`,`tui2`,`tui3`,`color`,`ratio`,`reward`,`reward2`,`give`,`cashback`,`tel`,`gong`,`huo`,`yin`,`is_fei`,`open`,`retail_open`,`retail_type`,`popup_open`,`popup_page`,`banner_open`,`nav_open`,`ad_open`,`activity_open`) values (1,'小程序后台',NULL,'site/20190318/18c17b9b122e25d2efd108dfdd59eb44.jpg','site/20190610/3f0a026a46d23ddf42e2a42ca6c36775.jpg','site/20190610/44881de1e28f89ed8965cf85c1da5c4c.jpg','site/20190610/09ca8f3ee8cf5ea28a3ad6634c2fd8cc.jpg','','1.00','20.00','20.00','5.00','5.00','0755-29185426','九州嗨购是线上线下异业引流神器','<p><span style=\"font-weight: bold; color: rgb(194, 79, 74);\">获客流程</span></p><p><img src=\"/uploads/editor/20190526/5875ec673f1ff1a7643ddfde665c8501.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/b73d33bac7f0b2d2bc8cedc13359dd1f.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/773e28296648e8839ded4e077b843025.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/341a57216fdef96990026083c0a447a1.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/98b1444e40f07f699c791433eba9a7e0.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/bc0be96ff4880e25ebb3d7f2e824cbb4.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/af6d236f35a6f91f5b5244a837058d1f.jpg\" style=\"max-width:100%;\"><br></p><p><br></p>','<p><span style=\"font-weight: bold;\">促销流程</span></p><p><img src=\"/uploads/editor/20190526/5a7add03ce5ad7bead9993b14da3e43f.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/cd3abd5cba32bfc50fd58083f8d2a55c.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/f25305d6cfe76a02c3bb94bb6bd6da35.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/ce77fe5fc16e9fca327b91073fb8ad21.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190526/defb0d3ccf1d491780e57171b2768681.jpg\" style=\"max-width:100%;\"><br></p><p><img src=\"/uploads/editor/20190614/17300184870075b63c2d81e6bfa1ba59.jpg\" style=\"max-width:100%;\"><br></p><p><br></p>',0,NULL,1,1,1,9,1,1,1,1);

/*Table structure for table `phpecs_speechcraft` */

DROP TABLE IF EXISTS `phpecs_speechcraft`;

CREATE TABLE `phpecs_speechcraft` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(225) DEFAULT NULL COMMENT '内容',
  `type` tinyint(1) DEFAULT '1' COMMENT '所属模块 1砍价 2点赞',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否启用 1启用 0不启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='话术模板';

/*Data for the table `phpecs_speechcraft` */

insert  into `phpecs_speechcraft`(`id`,`content`,`type`,`status`) values (1,'我在砍价免费拿##就差你1刀了  ',1,1),(2,'只差一点点了，我真的很想要这个宝贝，麻烦你帮帮我',1,1),(3,'急急急！帮我砍一刀，我真的很想要，爱你哟！',1,1),(4,'帮我砍个价好不好？我真的特别想要##',1,1),(5,'不是套路砍，这个真可以砍到免费拿，你帮我一下好吗？',1,1),(6,'进来帮我点个赞，就差你一个赞我就可以免费拿到了，拜托！',2,1),(7,'小GG or  小姐姐！帮我点个赞好吗，谢谢你啦！',2,1),(8,'很好的产品，点赞就可以免费拿到，非套路，不信你试试',2,1),(9,'你一定会进来帮我点个赞的，对吗？么么哒！',2,1),(10,'来不急解释了，进来帮我点个赞！',2,1);

/*Table structure for table `phpecs_store_category` */

DROP TABLE IF EXISTS `phpecs_store_category`;

CREATE TABLE `phpecs_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `pid` int(11) DEFAULT '0' COMMENT '上级id',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否显示 0不显示 1显示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='产品分类表';

/*Data for the table `phpecs_store_category` */

insert  into `phpecs_store_category`(`id`,`name`,`pid`,`sort`,`status`) values (1,'数码科技',0,1,1),(2,'居家日用',0,2,1),(3,'纸品',2,2,1),(4,'玩具',0,3,1),(5,'护理',2,2,1),(6,'厨房',2,3,1),(7,'特色',0,4,1);

/*Table structure for table `phpecs_store_order` */

DROP TABLE IF EXISTS `phpecs_store_order`;

CREATE TABLE `phpecs_store_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(35) DEFAULT NULL COMMENT '订单号',
  `order_number` varchar(225) DEFAULT NULL COMMENT '物流单号',
  `uid` int(11) DEFAULT NULL COMMENT '下单用户id',
  `time` int(11) DEFAULT NULL COMMENT '订单创建时间',
  `username` varchar(50) DEFAULT NULL COMMENT '收货人',
  `address` varchar(225) DEFAULT NULL COMMENT '收货地址',
  `phone` varchar(15) DEFAULT NULL COMMENT '收货手机号',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '订单总额',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '商品价格',
  `benefit` decimal(10,2) DEFAULT '0.00' COMMENT '优惠金额',
  `freight` decimal(10,2) DEFAULT '0.00' COMMENT '运费',
  `status` tinyint(1) DEFAULT NULL COMMENT '订单状态 0待付款 1待发货 2待收货 3已完成 4已关闭 5退款 6活动中 7线下发货',
  `cid` int(11) DEFAULT NULL COMMENT '优惠券id',
  `message` varchar(225) DEFAULT NULL COMMENT '用户留言',
  `shipment` varchar(20) DEFAULT NULL COMMENT '承运来源',
  `remark` varchar(225) DEFAULT NULL COMMENT '备注信息',
  `ling` tinyint(1) DEFAULT '0' COMMENT '是否为领取订单 0否 1是',
  `type` int(11) DEFAULT '0' COMMENT '订单类型 0正常订单 1打卡订单 2砍价订单 3拼团订单 4秒杀订单 5全返订单 6N元购订单',
  `active_id` int(11) DEFAULT NULL COMMENT '活动id',
  `active_status` tinyint(1) DEFAULT '0' COMMENT '活动状态 0进行中 1成功 2失败',
  `h_time` int(11) DEFAULT NULL COMMENT '活动开始时间',
  `use` int(11) DEFAULT '0' COMMENT '是否已使用过重签卡 0未使用 1已使用',
  `hexiao` varchar(225) DEFAULT NULL COMMENT '订单核销码',
  `is_hx` int(11) DEFAULT '0' COMMENT '是否核销过',
  `bargain_id` int(11) DEFAULT NULL COMMENT '砍价活动id',
  `spell_id` int(11) DEFAULT NULL COMMENT '拼团活动id',
  `spell_number` varchar(35) DEFAULT NULL COMMENT '开团订单号',
  `seckill_id` int(11) DEFAULT NULL COMMENT '秒杀活动id',
  `back_id` int(11) DEFAULT NULL COMMENT '全返活动id',
  `purchase_id` int(11) DEFAULT NULL COMMENT 'N元购活动id',
  `likes_id` int(11) DEFAULT NULL COMMENT '集赞活动id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单管理';

/*Data for the table `phpecs_store_order` */

/*Table structure for table `phpecs_store_order_list` */

DROP TABLE IF EXISTS `phpecs_store_order_list`;

CREATE TABLE `phpecs_store_order_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL COMMENT '关联订单数据id',
  `pid` int(11) DEFAULT NULL COMMENT '商品id',
  `spec` text COMMENT '所选规格',
  `unit` decimal(10,2) DEFAULT NULL COMMENT '单价',
  `num` int(11) DEFAULT NULL COMMENT '购买数量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单关联数据表';

/*Data for the table `phpecs_store_order_list` */

/*Table structure for table `phpecs_store_order_remark` */

DROP TABLE IF EXISTS `phpecs_store_order_remark`;

CREATE TABLE `phpecs_store_order_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL COMMENT '关联订单数据id',
  `user` varchar(20) DEFAULT NULL COMMENT '操作人',
  `time` int(11) DEFAULT NULL COMMENT '操作时间',
  `info` varchar(225) DEFAULT NULL COMMENT '变动',
  `remark` varchar(225) DEFAULT NULL COMMENT '原因',
  `type` tinyint(1) DEFAULT NULL COMMENT '备注类型 0价格变动 1备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='备注信息表';

/*Data for the table `phpecs_store_order_remark` */

/*Table structure for table `phpecs_store_product` */

DROP TABLE IF EXISTS `phpecs_store_product`;

CREATE TABLE `phpecs_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '产品名称',
  `cid` int(11) DEFAULT NULL COMMENT '分类id',
  `video` varchar(225) DEFAULT NULL COMMENT '视频',
  `price` decimal(10,2) DEFAULT NULL COMMENT '产品价格',
  `weight` decimal(10,2) DEFAULT NULL COMMENT '产品重量',
  `stock` int(11) DEFAULT NULL COMMENT '总库存数量',
  `volume` int(11) DEFAULT NULL COMMENT '产品销量',
  `spec` text COMMENT '产品规格',
  `value` text COMMENT '规格值',
  `details` text COMMENT '产品详情',
  `genus` int(11) DEFAULT '0' COMMENT '所属商家',
  `type` tinyint(1) DEFAULT '0' COMMENT '0线上 1线下',
  `ftype` tinyint(1) DEFAULT NULL COMMENT '运费类型 0包邮 1统一邮费',
  `freight` decimal(10,2) DEFAULT '0.00' COMMENT '运费价格',
  `time` int(11) DEFAULT NULL COMMENT '添加时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '0正常 1下架',
  `service` varchar(225) DEFAULT NULL COMMENT '服务',
  `p_price` decimal(10,2) DEFAULT NULL COMMENT '批发价格',
  `d_price` decimal(10,2) DEFAULT NULL COMMENT '代发价格',
  `liang` int(11) DEFAULT NULL COMMENT '起批量',
  `sp_code` varchar(225) DEFAULT NULL COMMENT '商品二维码',
  `is_open` tinyint(1) DEFAULT NULL COMMENT '是否开启卡密 0未开启 1已开启',
  `kalman` text COMMENT '卡密',
  `offset` decimal(10,2) DEFAULT NULL COMMENT '抵消金额',
  `share` tinyint(1) DEFAULT '0' COMMENT '分享开关 1开启 0未开启',
  `poster` varchar(225) DEFAULT NULL COMMENT '分享图',
  `gold` decimal(10,2) DEFAULT NULL COMMENT '分享佣金',
  `color` varchar(10) DEFAULT NULL COMMENT '名字颜色',
  `is_tui` tinyint(1) DEFAULT '0' COMMENT '是否推荐 0否 1是',
  `is_jing` tinyint(1) DEFAULT '0' COMMENT '是否精品 0否 1是',
  `is_new` tinyint(1) DEFAULT '0' COMMENT '是否新品 0否 1是',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '0正常 1回收站',
  `replace_open` tinyint(1) DEFAULT '0' COMMENT '替换开关',
  `replace_name` varchar(20) DEFAULT NULL COMMENT '替换收费名称',
  `replace_price` decimal(10,2) DEFAULT NULL COMMENT '替换收费金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='产品表';

/*Data for the table `phpecs_store_product` */

insert  into `phpecs_store_product`(`id`,`name`,`cid`,`video`,`price`,`weight`,`stock`,`volume`,`spec`,`value`,`details`,`genus`,`type`,`ftype`,`freight`,`time`,`status`,`service`,`p_price`,`d_price`,`liang`,`sp_code`,`is_open`,`kalman`,`offset`,`share`,`poster`,`gold`,`color`,`is_tui`,`is_jing`,`is_new`,`is_del`,`replace_open`,`replace_name`,`replace_price`) values (3,'10提 无芯卷纸4层加厚12卷/700克/提  ',3,'','99.00','7.00',99998,346,'null','null','<p><img src=\"/uploads/editor/20190315/f8a8b34ccb1764081244cdfc56d644ca.jpg\"/><img src=\"/uploads/editor/20190315/e551fafe524bcd6c81ba4f5b844c475c.jpg\"/><img src=\"/uploads/editor/20190315/6520d695ad0e854da2e47ba58985595c.jpg\"/><img src=\"/uploads/editor/20190315/b4f61717881c633e33f34432f58d7b9d.jpg\"/><img src=\"/uploads/editor/20190315/d1398254baab3369f7a4343c02c43235.jpg\"/><img src=\"/uploads/editor/20190315/f69969969eb4df39105b5e8f6b1d7911.jpg\"/><br/></p><p><br/></p>',0,0,0,'0.00',1565075122,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'','1.00','',1,1,1,0,0,NULL,NULL),(4,'5盒 一盒40片装   善芳一次性洗脸巾卸妆巾 全棉洁面美容巾',3,NULL,'40.00','0.70',9999,345,'null','null','<div><p>特点：<br>1.善芳全棉柔巾棉花，产于日照阳光充足的新疆，经专业水刺无纺布工艺以及引进先进技术制成<br>2.干湿两用：棉柔巾良好吸水及释水性能，适合干湿两用。打湿后，柔顺依然</p><p>3.针对敏感肌肤，有效预防痘痘</p><p>4.不含甲醛  不含荧光剂  不含芳香胺</p></div><p>非一般传统材料，不含人造纤维，全面脱脂，层层精梳，采用 生态工艺制造而成，全程净化，轻盈丝滑，如蝉翼一样薄轻巧，但却有锁水与释水性，不起毛掉屑，给您娇嫩肌肤柔情呵护。</p><p><img src=\"/uploads/editor/20190317/565dd7379f78753c44e66e3163f820f4.jpg\"><img src=\"/uploads/editor/20190317/515e1819c1189b6ed4706d5030023453.jpg\"><img src=\"/uploads/editor/20190317/7eb91da5d09489dd0378f023d693f8f8.jpg\"><img src=\"/uploads/editor/20190317/5e2cca84a174c1326ee0ac37822b943e.jpg\"><img src=\"/uploads/editor/20190317/2fdd67833f3f750a908c489657c1a9eb.jpg\"><img src=\"/uploads/editor/20190317/729b73657e9cd6487bded63775baee65.jpg\"><img src=\"/uploads/editor/20190317/5d96a843d5054d81f7e9bea3283d5c8a.jpg\"><img src=\"/uploads/editor/20190317/040ce9062cb836b2b040d28e62d3ed1b.jpg\"><img src=\"/uploads/editor/20190317/1320dc2ec76aab8e4561d80a35b386da.jpg\"><img src=\"/uploads/editor/20190317/04a1d7816624a65b54c4a911abebbb3b.jpg\"><img src=\"/uploads/editor/20190317/89d1ed1a8c94e8a0e2b1af8accbe57f1.jpg\"><img src=\"/uploads/editor/20190317/3a752e39365ddef792fc494a378a65cd.jpg\"><img src=\"/uploads/editor/20190317/4a061250cbf28170f9facd8cadc8b14b.jpg\"><img src=\"/uploads/editor/20190317/b042bc8e09d3f73304eab557005527b4.jpg\"><img src=\"/uploads/editor/20190317/fd8c27f5f13a1f5f0891ffa6e7f67771.jpg\"><br></p><p>一盒40片装  5盒</p>',0,0,0,'0.00',1552804936,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,1,0,0,0,0,NULL,NULL),(5,'30包恬羽本色抽纸整箱无漂白原色抽取式面巾纸',3,NULL,'45.00','2.50',9999,4534,'null','null','<p>纸巾尺寸：115mm*180mm*270张 &nbsp;&nbsp;&nbsp;纸巾包装尺寸：115mm*90mm&nbsp; &nbsp; 3层&nbsp; &nbsp;90抽</p><p>默认包邮，除偏远地区（西藏、新疆、内蒙、甘肃、宁夏、青海，海南）不包邮；</p><p><img src=\"/uploads/editor/20190317/52d9358e12fcfe9b231e89dc0beb5d49.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/9a7d76d59a925cf2f7c9a2c0b5febb3c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/1da3993f5e716566725245c2a70563fe.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/5b38f01f02e115267e1bddd1c5b27cf7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/aef0729dde850bff42cdde0e343966f8.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1552804912,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,1,0,0,0,0,NULL,NULL),(6,'4提 一提12卷700g 一晨竹浆本色卷纸',3,NULL,'48.00','2.60',9999,341,'null','null','<p><img src=\"/uploads/editor/20190317/81df1246473aff08aed2a7855cb272cd.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/5d1119aaed6b6ea2b45cac8e09a948f4.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/503f00f1c8bf1fdd2874f946fda034f4.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/aafee765e70027a5f06f5afe5f268fe7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/8d0bd39532fa9c4f7d5aaadd398ac1f2.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/e59c21fd0b3f3619050f2cc3cebfc5bf.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1552804878,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL),(7,'30包整箱良布竹浆本色抽纸',3,'','50.00','2.60',9999,452,'null','null','<table><tbody><tr><td>&nbsp; &nbsp;产品规格</td><td>120mm*180mm*300张</td></tr></tbody></table><table><tbody><tr><td>&nbsp; 纸巾尺寸（cm）</td><td>12*18<br></td></tr></tbody></table><table><tbody><tr><td>&nbsp; 包装尺寸（cm）</td><td>12*9*4.5</td></tr></tbody></table><p><img src=\"/uploads/editor/20190317/d6ab41a922d33cb9933518f40fee1581.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/6f82fa1a42a6852ca7d1976677d0d51c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/ba6a5d8142798748581d9fde5d1dbce8.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/00f42b5cde22027b11ca8638013b14f5.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/ace6192f9f2a874c58e0086c53c6b683.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1560226558,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'','5.00','',1,0,0,0,0,NULL,NULL),(8,'30包整箱植护抽纸 红黄蓝三色 48元包邮',3,NULL,'48.00','2.50',9999,456,'null','null','<p><img src=\"/uploads/editor/20190317/3ad47c793865f5f52e6a5a9c35c0e4ee.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/7f4122316c87a7c0ef08d6612e386120.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/e28b47c02689535f9c3f9504a0b013ab.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/19ad4b62a88890b71034784b3e11b411.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/1a35a530de2b30989f5833502e846b24.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/f082bbf4765578fb28a12898e0d647d5.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/f3239aece202b374e575140ca7886768.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/e6db3557933e3d9e8e4b9627d16d681b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/e4e17e3412187804796657568865d9a5.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1552804814,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,1,0,0,0,0,NULL,NULL),(9,'24包一箱 100抽 植护抽纸 竹浆餐巾',3,'','48.00','2.60',9999,324,'null','null','<p><img src=\"/uploads/editor/20190317/85eb81569e9a4160ea8557dc512848a2.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/7f92cb244fab8f9212e44331940ba492.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/4b628c46df72894df62b53307f3b79fd.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/1c2e9591d7545fcc06d7549a3321b659.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/9b0c3a91b72b91f796d176f3af78079c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/9c804cf2465e24af52aa0f356c3c5718.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/e690dd702691c1f92662f3e9dfb5af5c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/30b17ce385ff318467e3392af0c1b3bf.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/4cc6b8cbbc16b7a15a6badc88f3b4208.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/0294fa4f5a33b17d2b0db8e8e4c2499f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/da1c419845d2abc2535a9c8b2872ff6d.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1562658296,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'','0.00','',1,NULL,1,0,0,NULL,NULL),(10,'3提 39元 植护无芯卷纸36卷4层加厚',3,NULL,'39.00','2.53',9999,353,'null','null','<p>【主要原料】：原生木浆 &nbsp;【包&nbsp;&nbsp;&nbsp;装】：12卷/提3提/件</p><p>【单提规格】：4层*70g/卷12卷/提&nbsp;</p><p>【价 &nbsp; 格】：28.5元/件，3提&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【单提重量】：840g</p><p>【保质期】：3年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;【特&nbsp;&nbsp;点】：柔厚四层，舒适亲肤</p><p><img src=\"/uploads/editor/20190317/dcd3ae5f16bfda5f64fe54517488628e.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190317/b1d23f68972862da950ece642fe3e99a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/4ccf87c827cec5d170e23a9def492414.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/1b621f04d4f972ee28d63e35350a946f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/d62919d727cc3061e36399d365e0d517.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190317/281fdc50554b9606bb0efbb496aa809b.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1552805042,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,1,0,0,0,0,NULL,NULL),(13,'4提共48卷整箱 采琪采本色擦手家用无芯卷纸700克1*12卷',3,NULL,'50.00','2.60',9999,324,'null','null','<p><img src=\"/uploads/editor/20190317/cd25f3442c3945f1cc22457d11eacdd1.jpg\"><img src=\"/uploads/editor/20190317/39c2c2d1c0d5967c606b80605e946ebd.jpg\"><img src=\"/uploads/editor/20190317/32f329c0fcdba67d6dfec08bd2333869.jpg\"><img src=\"/uploads/editor/20190317/cdcde52c2c090d62ce6c4c1f2bf6c324.jpg\"><img src=\"/uploads/editor/20190317/581df7bf56d08d7d2406b3e42da237a0.jpg\"><img src=\"/uploads/editor/20190317/1984bdf5a1cf3f474769c1f7c6cb0ce4.jpg\"><img src=\"/uploads/editor/20190317/e3351fc9bd5d822d2c9dd5a2d962459f.jpg\"><img src=\"/uploads/editor/20190317/159655a7b582ae0099b72e1ba1325d87.jpg\"><br></p><p><br></p>',0,0,0,'0.00',1552920812,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL),(14,'对打小人',4,'video/20190412/7831ab208f65887312fa53f7ee865425.mp4','29.00','1.00',99,34,'null','null','<p><img src=\"/uploads/editor/20190412/e046baa806b1b970afbfe38508ef1192.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190412/fb00d4c61c91d589f95cd2ad2f09d6b5.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/23b6fdfd683ee2d755376296789eef85.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/810772bcb38e315dde1d681a8b1f8946.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/74b418e4492c89c079db8b014e0d4904.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/1d61c9037e6312dc4542ffeee66c6deb.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/8a8eafab35ba7cf26e657d8c411b0537.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/19530f9ddfb2b9e3ab54c00f52299a8f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/8685a219073c7ac6294f83e2d993fc42.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/6cb65b374a4d8d2210629a6ae645d01b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/b5be69c08a561c4b5ada5a6bbad2d4eb.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/8cbc1fa761d58a118b77add5679bd03c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/adbf24e10232c99e76e0a7abe2132e72.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/41e466d26795556bf0343cf133ed9e19.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1555080393,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,1,0,0,0,0,NULL,NULL),(15,'感应UFO',4,'video/20190412/1b1c4535bb15e005ed44a4ec7579cd29.mp4','79.00','0.50',9999,76,'null','null','<p><img src=\"/uploads/editor/20190412/86485399c719cda3c2fce5e978b8c623.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190412/a5e76f7814dcd4e73c3b731924dafa23.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/db1b8964d3ade7326fb22324afd5053e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/8211e70780a19837e9a85224c8accf3e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/23b129a4ffaa151f8e0ba6b2fdc6d4d9.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/2e76cde2f2d373d76d06edf85bd3efca.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/5946e068412d3bbd17afe5acf4108310.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/014d7f6016ed451e14c98101ca1112bf.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/590e5e4e9a984c1224bd74f9f0ae6a2d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/41db323b6a12b5a7ffccfdf6ce3b5a96.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/60c48103cd8e9319d344d84db58aa471.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1562658122,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'poster/20190601/4527a8210249a8619135539ea3d9e9d0.jpg','10.00','#0e0303',1,1,NULL,0,0,NULL,NULL),(16,'竹鸳纸鸢飞鸟',4,'video/20190412/6a7a80a1c2e58a94286b3aa9c444d7dc.mp4','10.00','0.10',9998,568,'null','null','<p><img src=\"/uploads/editor/20190412/c6e7eda17f5a1d6dff425eae84234df5.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190412/5ff69d0fd886b3613534d5239ef74ddf.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/44d091d7f0620929a89fa13cd346d4b2.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/c10a5a4dd8b92e18b7571f81d24147c1.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/1625e08b02d5403994d7bf50d806a11e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/a17cd4805c70caca9adf35ab211b7ddd.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/be0cc500a63f80249ee0e6c404779484.jpg\" style=\"max-width: 100%;\"><br></p><p>6</p>',0,0,0,'0.00',1559289301,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'','0.01','',1,0,0,0,0,NULL,NULL),(17,'感应小仙女',4,'video/20190412/a104d8c54f74406a37c1ff217598468c.mp4','35.00','1.00',9999,456,'null','null','<p><img src=\"/uploads/editor/20190412/970a4d3bbb7dd0ed112cbce7e14c1e17.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190412/b4fc8ae5274b069eba990d8daca5b247.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/3be4d731dd2f3a7a0e28e737550e8448.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/a96e5f5590c9d6e59072b93839be3b6d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/8c2bbbbbb087d1d5cd8a94aeeada70f7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/26ee1d533f649588435c19d54531b72a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/dc3560664a5fa36026f85358771a09af.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/e411047bebe93ab745fa338f62fda726.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1562658160,0,'','0.00',NULL,0,'',NULL,'','0.00',0,'poster/20190601/0512fdf0035240c0a8c6b78f76da8d4b.jpg','5.00','#140808',1,1,NULL,0,0,NULL,NULL),(18,'88轨道球 注意力训练',4,'video/20190412/a15702334c28aaf09a9c24e4f6a3750f.mp4','21.00','1.00',9999,34,'null','null','<p>每天训练半小时，可明显提高小孩注意力</p><p><img src=\"/uploads/editor/20190412/2cb4de027c5e15a091799aaf5d728a94.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190412/50a47fd99f1bb2be849206311a1af335.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/28e4c5b05370163c966ac4b0d7397651.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/91e3578af46fe767e06ea98df4d660b0.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/1cf4a2488cddd59b0cda846d28a607ec.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/a88352aa637b48d65980876383b2f0ce.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/b60f071f67760061d3a92a1bc35f6a92.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/99e05ef5e5a7804a95c1222221024c4e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190412/25f67e2920787670dd79d1ce61c2e548.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1559373829,0,'','5.00',NULL,50,'',NULL,'','0.00',0,'poster/20190601/68fde77a1206359a624cd4f3b89741e5.jpg','3.00','#070202',1,0,0,0,0,NULL,NULL),(19,'云南本草去狐臭1瓶',5,'','19.00','0.10',9998,453,'null','null','<p><img src=\"/uploads/editor/20190413/5f3999b7034ebca385517297182b7ede.gif\"/><img src=\"/uploads/editor/20190413/c65eba9e80916b44bde07580b92c85d0.gif\"/><img src=\"/uploads/editor/20190413/e2ed92aa1fd5f00e5ebc85fc7973db41.gif\"/><img src=\"/uploads/editor/20190413/3b301199311a4d90dc7bd6492518fe46.gif\"/><br/></p><p><br/></p>',0,0,0,'0.00',1567419815,0,'','7.00',NULL,100,'',NULL,'V502856946347690\nN502856946347636\nN502856946347613\nW502856946347602\nW502856946347698\nW502856946347662\nY502856946347616\nF502856946347682\nX502856946347699\nZ502856946347679\nI502856946347649\nY502856946347660\nC502856946347645\nW502856946347675\nC502856946347625\nS502856946347633\nR502856946347604\nY502856946347625\nH502856946347694\nF502856946347668\nZ502856946347684\nU502856946347656\nL502856946347668\nH502856946347611\nB502856946347655\nB502856946347612\nR502856946347657\nB502856946347618\nS502856946347697\nI502856946347641\nG502856946347619\nH502856946347697\nM502856946347654\nG502856946347689\nN502856946347620\nR502856946347651\nY502856946347602\nD502856946347606\nT502856946347608\nN502856946347654\nY502856946347627\nI502856946347672\nP502856946347699\nG502856946347618\nC502856946347619\nS502856946347626\nA502856946347678\nI502856946347689\nF502856946347678\nY502856946347613\nX502856946347607\nA502856946347644\nH502856946347629\nW502856946347642\nH502856946347653\nR502856946347611\nX502856946347655\nW502856946347658\nG502856946347618\nN502856946347690\nP502856946347638\nM502856946347622\nW502856946347695\nH502856946347698\nO502856946347614\nR502856946347611\nH502856946347625\nM502856946347616\nC502856946347642\nX502856946347622\nN502856946347672\nW502856946347658\nV502856946347680\nS502856946347697\nI502856946347608\nF502856946347643\nD502856946347684\nY502856946347651\nG502856946347616\nB502856946347690\nR502856946347608\nN502856946347624\nR502856946347694\nP502856946347616\nG502856946347623\nA502856946347632\nD502856946347688\nS502856946347650\nG502856946347648\nT502856946347644\nE502856946347619\nE502856946347664\nK502856946347691\nG502856946347664\nH502856946347640\nX502856946347609\nP502856946347605\nG502856946347621\nH502856946347691\nG502856946347668\nE502856946347658\nY502856946347685\nA502856946347677\nE502856946347620\nS502856946347606\nT502856946347684\nO502856946347653\nO502856946347618\nS502856946347694\nW502856946347637\nN502856946347620\nN502856946347677\nV502856946347656\nX502856946347600\nK502856946347644\nG502856946347678\nZ502856946347633\nH502856946347624\nB502856946347669\nP502856946347680\nV502856946347606\nM502856946347628\nL502856946347663\nA502856946347648\nY502856946347634\nP502856946347678\nD502856946347662\nY502856946347695\nQ502856946347613\nW502856946347687\nN502856946347633\nZ502856946347628\nM502856946347642\nO502856946347619\nM502856946347644\nV502856946347618\nH502856946347602\nM502856946347615\nW502856946347658\nO502856946347676\nT502856946347653\nL502856946347621\nG502856946347624\nR502856946347640\nY502856946347632\nU502856946347635\nG502856946347693\nV502856946347678\nC502856946347677\nD502856946347669\nO502856946347640\nZ502856946347603\nG502856946347653\nG502856946347614\nA502856946347600\nO502856946347602\nK502856946347697\nH502856946347627\nO502856946347619\nP502856946347670\nH502856946347672\nD502856946347627\nG502856946347613\nG502856946347667\nY502856946347636\nK502856946347699\nT502856946347643\nS502856946347680\nR502856946347617\nM502856946347620\nI502856946347601\nV502856946347602\nU502856946347615\nY502856946347689\nH502856946347610\nB502856946347619\nW502856946347696\nI502856946347672\nX502856946347632\nR502856946347612\nS502856946347619\nO502856946347665\nQ502856946347695\nA502856946347673\nB502856946347660\nG502856946347658\nG502856946347630\nR502856946347634\nN502856946347659\nC502856946347624\nG502856946347616\nT502856946347600\nN502856946347669\nQ502856946347610\nW502856946347645\nD502856946347607\nW502856946347631\nU502856946347649\nA502856946347692\nH502856946347684\nP502856946347649\nV502856946347680\nN502856946347607\nS502856946347630\nU502856946347684\nI502856946347678\nG502856946347609\nT502856946347603\nV502856946347659\nF502856946347605\nO502856946347698\nQ502856946347625\nL502856946347677\nC502856946347662\nU502856946347681\nG502856946347692\nE502856946347652\nK502856946347666\nY502856946347606\nX502856946347602\nN502856946347648\nE502856946347628\nH502856946347612\nQ502856946347699\nY502856946347638\nG502856946347653\nK502856946347692\nK502856946347619\nE502856946347669\nM502856946347644\nC502856946347662\nL502856946347656\nF502856946347644\nB502856946347625\nO502856946347652\nV502856946347642\nG502856946347614\nZ502856946347676\nO502856946347604\nH502856946347634\nS502856946347681\nR502856946347676\nZ502856946347680\nB502856946347616\nK502856946347618\nS502856946347660\nC502856946347690\nE502856946347612\nZ502856946347601\nS502856946347694\nL502856946347627\nF502856946347617\nE502856946347645\nB502856946347654\nI502856946347614\nA502856946347633\nP502856946347665\nL502856946347625\nZ502856946347668\nS502856946347624\nE502856946347642\nR502856946347607\nF502856946347672\nP502856946347674\nF502856946347602\nG502856946347637\nZ502856946347663\nL502856946347673\nD502856946347609\nS502856946347620\nD502856946347627\nQ502856946347601\nT502856946347672\nK502856946347679\nI502856946347616\nV502856946347628\nE502856946347658\nY502856946347608\nA502856946347669\nC502856946347635\nL502856946347629\nL502856946347646\nO502856946347645\nB502856946347636\nR502856946347677\nZ502856946347662\nO502856946347679\nF502856946347618\nZ502856946347685\nF502856946347644\nA502856946347637\nF502856946347693\nS502856946347691\nE502856946347614\nF502856946347693\nP502856946347666\nT502856946347669\nY502856946347645\nW502856946347614\nG502856946347607\n','0.00',NULL,'poster/20190601/a601ebf233a06c3d43449344ba9f87f6.jpg','0.00','#0c6315',1,1,1,0,1,'空间费',NULL),(20,'小白洗鞋神器清洗剂2瓶',5,'','20.00','0.10',9999,676,'null','null','<p><img src=\"/uploads/editor/20190413/d694200dcc70e268235ecf0ef049f8f7.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190413/2616cc95127b2e09625f043c11cdb495.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190413/c918f2d13ff0c2a14afeaea3b6f39521.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190413/5ce2ed9be521f307c307e95d0bb26d3f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190413/9a60280b2d6be050cbcfbf9f2c0618cf.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190413/67eeb0700d41da1b647b61178a2561ee.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190413/9315ae83c0dfd7c00b1f15905b99996b.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,1,'12.00',1556786759,0,'','5.00','12.00',NULL,'',1,'Z502861379003924\nX502861379003995\nK502861379003960\nG502861379003943\nT502861379003992\nN502861379003956\nH502861379003996\nF502861379003960\nW502861379003964\nU502861379003904\nP502861379003911\nI502861379003915\nY502861379003910\nK502861379003980\nQ502861379003961\nL502861379003968\nV502861379003925\nM502861379003964\nO502861379003914\nA502861379003916\nF502861379003984\nX502861379003978\nN502861379003911\nM502861379003949\nF502861379003907\nE502861379003988\nH502861379003943\nY502861379003910\nM502861379003944\nG502861379003930\nV502861379003932\nN502861379003978\nS502861379003962\nN502861379003940\nF502861379003936\nN502861379003952\nB502861379003953\nT502861379003901\nD502861379003956\nD502861379003957\nD502861379003952\nR502861379003900\nG502861379003962\nM502861379003954\nM502861379003966\nX502861379003900\nN502861379003926\nT502861379003973\nU502861379003917\nT502861379003923\nN502861379003977\nR502861379003945\nU502861379003996\nM502861379003945\nW502861379003955\nQ502861379003961\nN502861379003933\nM502861379003904\nF502861379003993\nM502861379003932\nQ502861379003975\nS502861379003924\nY502861379003994\nX502861379003969\nV502861379003977\nV502861379003941\nZ502861379003937\nQ502861379003967\nL502861379003914\nI502861379003957\nN502861379003987\nX502861379003915\nB502861379003989\nS502861379003994\nI502861379003916\nT502861379003905\nK502861379003971\nV502861379003962\nN502861379003941\nB502861379003958\nI502861379003933\nG502861379003950\nB502861379003974\nA502861379003969\nT502861379003908\nL502861379003913\nB502861379003982\nG502861379003952\nZ502861379003960\nG502861379003987\nI502861379003962\nP502861379003911\nV502861379003959\nG502861379003906\nT502861379003924\nS502861379003913\nG502861379003975\nU502861379003935\nO502861379003995\nT502861379003978\nE502861379003986\nW502861379003918\nR502861379003926\nG502861379003982\nG502861379003992\nI502861379003935\nZ502861379003917\nT502861379003969\nR502861379003946\nK502861379003972\nL502861379003956\nU502861379003983\nS502861379003900\nM502861379003907\nI502861379003963\nG502861379003902\nS502861379003955\nQ502861379003902\nO502861379003955\nG502861379003993\nL502861379003930\nN502861379003912\nH502861379003920\nH502861379003974\nH502861379003944\nV502861379003908\nK502861379003900\nP502861379003996\nU502861379003908\nT502861379003908\nK502861379003992\nG502861379003925\nC502861379003928\nQ502861379003925\nG502861379003962\nW502861379003952\nV502861379003926\nP502861379003923\nG502861379003962\nS502861379003900\nR502861379003966\nM502861379003986\nQ502861379003973\nW502861379003957\nQ502861379003900\nS502861379003907\nZ502861379003951\nG502861379003911\nR502861379003922\nP502861379003977\nI502861379003988\nR502861379003904\nH502861379003934\nF502861379003901\nL502861379003987\nX502861379003999\nQ502861379003929\nC502861379003909\nK502861379003946\nU502861379003950\nA502861379003981\nO502861379003999\nK502861379003961\nN502861379003999\nL502861379003953\nG502861379003974\nN502861379003933\nG502861379003973\nK502861379003909\nY502861379003907\nZ502861379003994\nF502861379003900\nK502861379003952\nG502861379003909\nZ502861379003943\nV502861379003938\nG502861379003960\nF502861379003910\nD502861379003996\nG502861379003909\nE502861379003944\nF502861379003959\nU502861379003945\nG502861379003958\nG502861379003913\nF502861379003930\nO502861379003932\nI502861379003912\nB502861379003960\nV502861379003942\nW502861379003940\nA502861379003960\nQ502861379003948\nQ502861379003993\nW502861379003983\nU502861379003939\nC502861379003919\nY502861379003906\nE502861379003927\nW502861379003940\nC502861379003908\nG502861379003987\nW502861379003907\nL502861379003922\nG502861379003979\nX502861379003996\nS502861379003966\nQ502861379003934\nP502861379003960\nP502861379003929\nL502861379003904\nA502861379003963\nH502861379003906\nX502861379003915\nX502861379003938\nQ502861379003976\nB502861379003960\nT502861379003924\nW502861379003976\nB502861379003992\nG502861379003937\nA502861379003986\nG502861379003906\nN502861379003958\nO502861379003914\nV502861379003928\nU502861379003992\nD502861379003935\nI502861379003937\nK502861379003915\nG502861379003984\nO502861379003900\nM502861379003939\nL502861379003913\nN502861379003948\nB502861379003967\nC502861379003967\nG502861379003954\nT502861379003995\nT502861379003906\nP502861379003929\nY502861379003926\nB502861379003933\nW502861379003986\nO502861379003945\nQ502861379003963\nD502861379003931\nZ502861379003928\nF502861379003916\nP502861379003956\nD502861379003987\nX502861379003967\nG502861379003906\nF502861379003972\nZ502861379003921\nZ502861379003930\nI502861379003931\nN502861379003990\nN502861379003968\nE502861379003927\nY502861379003967\nU502861379003944\nW502861379003992\nR502861379003950\nS502861379003968\nD502861379003982\nZ502861379003910\nG502861379003990\nK502861379003991\nQ502861379003938\nG502861379003928\nD502861379003961\nC502861379003946\nY502861379003913\nP502861379003990\nO502861379003945\nB502861379003945\nW502861379003971\nL502861379003938\nV502861379003936\nE502861379003973\nZ502861379003951\nQ502861379003987\nI502861379003913\nD502861379003965\nO502861379003981\nI502861379003911\nQ502861379003909\nW502861379003905\nP502861379003917\nX502861379003945\nW502861379003906\nE502861379003921\nV502861379003986\nV502861379003989\nB502861379003935\nK502861379003912\nY502861379003946\nT502861379003939\nQ502861379003991\n','0.00',0,'poster/20190502/cc9bad1b477757cb9559f2573b5e76a7.jpg','0.00','#ffffff',1,0,0,0,0,NULL,NULL),(21,'过家家早教贩卖机',4,'','199.00','1.00',9889,23,'null','null','<p><img src=\"/uploads/editor/20190414/23b02b5a8e5550d16debae04a4aa69a6.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190414/35397d9d9c44064cdfbb5db2489a6e54.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/26db377fbfc301db9dd5f6b8b22afff7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/0e74e48f5d7a312908d14fd80e426124.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/5bde77b8615f1beb9e56f99149956f5f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/5cf0124706ea264e153ae3599a549197.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/a3942ff8805b6c8be4c73a05c2c39392.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/437f64dcbb516cc6d96c41cad7c4ed9e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/639d8b005d353ae7ad0c686a96b4763a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/e17eb67daa525bf271b45fa52bf9a81a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/8140ccb51f2ab3565e2a17750cc7cf50.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/b3b6dd728f797aa0658e3942de34785a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/f146d4e0b14ebe3405a66d7c470e094d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/2cd2ef12bcc3db4b81ddee2cd96231f1.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/20a662ac8c179f2e4e9c9c2e95462a8c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/ec27e671d2bc62b0b4793ba83e1ed36f.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/e31c55094fd91cf6ca1de8d46781e5bf.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/e688459343b3a56134ee431f73fe0da1.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/7d27a0d6fcc47c252c312a516167f78b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/43cae1466f308d13e6785e5798776644.jpg\" style=\"max-width: 100%;\"><br></p>',0,0,0,'0.00',1555232960,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL),(22,'吃豆青蛙  升级加厚版',4,'','39.00','1.00',2664,655,'[{\"\\u89c4\\u683c\":{\"name\":[\"2\\u4eba\\u6b3e\",\"3\\u4eba\\u6b3e\",\"4\\u4eba\\u6b3e\"]}}]','[{\"group\":\"2\\u4eba\\u6b3e\",\"price\":\"39\",\"stock\":\"888\",\"cost\":\"21\",\"weight\":\"1\"},{\"group\":\"3\\u4eba\\u6b3e\",\"price\":\"49\",\"stock\":\"888\",\"cost\":\"26\",\"weight\":\"1\"},{\"group\":\"4\\u4eba\\u6b3e\",\"price\":\"59\",\"stock\":\"888\",\"cost\":\"30\",\"weight\":\"1\"}]','<p><img src=\"/uploads/editor/20190414/c95c04b2d7d94d47cb75bf37e5c6b3f3.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190414/26e58165ef6cea7934d7156fa6a088dc.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/968e63c969fc1c35888beaf9c5cd4446.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/464b95920947d8393ab89dcdcaae9845.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/52faa4fe2f483a69e3b94c51d15f2a21.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/4839b5e3409f3338488926fdba813a79.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/8e4120d54c5f345a38447359f74bc551.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/a67bc85a8d650a10184749b15077564d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/5205a220593cd4c03fcb5a23839a0802.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1562658075,0,'','18.00',NULL,20,'',NULL,'','0.00',0,'poster/20190601/bbf46b68c26487e092b1e1e606e9ad63.jpg','3.00','#020a01',NULL,1,NULL,0,0,NULL,NULL),(23,'多功能厨房切菜神器',6,'','119.00','2.50',9999,32,'null','null','<p><img src=\"/uploads/editor/20190414/fa9189af01c7eefd4e98c73afe162f9c.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190414/1114066332ba2574487513cc0ede09c7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/d44a193396c7f0e8098fb1320c87d3d3.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/b1bd7cdeef9d5dadb634efaecf06f8cd.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/12fd74d87fb8f213aaf38793bb14c3f1.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/fdf41e3348c5d1afce900f6c55a303bc.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/ea92b228faaae7c4971d9a3bcd8f9516.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/4fc389d26ed7234f568db7bfaaa91b6b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/449d6fa362a944b315aa3cdd3bc39c14.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/9200f7d9fae8e48f0bab4f82afd98120.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/74e420a50e0d56939a0078755c5e32c3.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/624cfa7aee48808d96d08242c5c9a3df.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/4d17a130aede451252794846707f5790.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/474902505190401f3e4403b6d9b7e351.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/11273899a5a5903765fafbc9f79070ed.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/2714e193e89e5da5da49c5aa35e9cb0e.jpg\" style=\"max-width: 100%;\"><br></p>',0,0,0,'0.00',1555235777,0,'',NULL,NULL,NULL,'',NULL,NULL,NULL,0,NULL,NULL,NULL,0,0,0,0,0,NULL,NULL),(24,'贝恩声光音乐不倒翁',1,'','129.00','2.00',9998,17,'null','null','<p><img src=\"/uploads/editor/20190414/76aa7a4f2c030349ea39d4ee2531eae5.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190414/27d87414720d1b6881432f8ed81eb77b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/ae7d756a6a0994cb7e63c3ba74273986.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/dc0e574da230e803d5733c20401debb3.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/f2cf704b850a38adef9c030411bc4416.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/72828865975610da523d387a8a162f1a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/8908fdc91fc430eac808f943957501b4.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/49e5caea6182f53cb5f8382345d3dd9d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/ad6753d0d8ae7f652fe4728c8b402c47.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/c829c5707af33e56994ab53d986a8abc.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/273d44a9171793fe55751d2f7595e997.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/ade7ca31b1f1f7f94dfe9b0e4be8aced.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,1,'12.00',1559318532,0,'','89.00',NULL,3,'',NULL,'','0.00',0,'poster/20190601/98ca7e176a67e4d7fef4229d3af1ba8e.jpg','10.00','',0,0,0,0,0,NULL,NULL),(25,'特技翻斗车',4,'','79.00','1.00',9987,32,'null','null','<p><img src=\"/uploads/editor/20190414/d1cb418045c13b2bf9c89987038507b6.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190414/342a0ef8e02bbb243d7b5b9ab931c741.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/16d4976915678bf9a774de61da83513a.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/6bb86d98970ea6bb3b11c5715ec0084d.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/9d758abcc9c68f3d58afe6dcfd69c8aa.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/a322fa000a3bc770676c3727accfcc6b.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/951f466a2681eb17e88964d8297c2a34.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/a6ad1d2f2360e3e394d2aa3fd30fa3eb.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190414/407fd8c640323f722adf6d7f1d4acd57.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',0,0,0,'0.00',1562658201,0,'','70.00',NULL,0,'',1,'O426649443535154\nS426649443535152\nF426649443535193\nR426649443535151\nC426649443535164\nK426649443535184\nV426649443535102\nZ426649443535106\nG426649443535178\nM426649443535120\nV426649443535142\nG426649443535119\nY426649443535164\n','0.00',0,'poster/20190601/3f3c00a375d1247013d38e4d94b8aabe.jpg','10.00','#120101',NULL,1,1,0,0,NULL,NULL),(26,'余味酒糟鱼',7,'','35.00','0.10',500,345,'null','null','<p><img src=\"/uploads/editor/20190430/3241ca171171b2e284c978d712f967a3.jpg\" style=\"max-width: 100%;\"></p><p><img src=\"/uploads/editor/20190610/97f2ba437fca3b7f19f961fc2e608e8c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190610/ae1ca62a7c590bfd391d4a50faa7d40c.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190610/1e050604ad0806ee1ad832c356c146a7.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190430/bacf00e22fbd39b452d8070de7c2a6c4.jpg\" style=\"max-width: 100%;\"></p><p><br></p>',0,0,1,'8.00',1560157769,1,'','35.00',NULL,0,NULL,NULL,'','0.00',0,'','20.00','#000000',0,0,0,0,0,NULL,NULL),(27,'不限速4G流量卡',1,'','0.01','0.10',484,252,'null','null','<p><img src=\"/uploads/editor/20190430/ff02aefca95f965db4397a7793364c9f.jpg\" style=\"max-width:100%;\"><br></p><p><br></p>',0,0,1,'0.01',1564105060,0,'123','2.00',NULL,10,NULL,1,'A430052412753926\nL430052412753910\nZ430052412753999\nU430052412753997\nF430052412753935\nK430052412753909\nV430052412753979\nM430052412753970\nI430052412753972\nW430052412753960\nP430052412753927\nG430052412753911\nO430052412753963\nV430052412753945\nO430052412753972\nZ430052412753900\nP430052412753925\nP430052412753912\nB430052412753996\nD430052412753919\nG430052412753994\nE430052412753966\nT430052412753910\nM430052412753942\nE430052412753948\nF430052412753967\nW430052412753945\nG430052412753937\nL430052412753963\nS430052412753943\nU430052412753953\nF430052412753988\nK430052412753965\nI430052412753909\nH430052412753920\nD430052412753922\nC430052412753902\nA430052412753908\nL430052412753996\nD430052412753954\nB430052412753928\nV430052412753959\nG430052412753932\nI430052412753939\nQ430052412753998\nU430052412753917\nS430052412753933\nI430052412753944\nC430052412753978\nW430052412753945\nQ430052412753923\nL430052412753922\nD430052412753962\nE430052412753907\nC430052412753920\nC430052412753912\nC430052412753908\nC430052412753959\nG430052412753927\nK430052412753936\nV430052412753984\nV430052412753965\nQ430052412753935\nY430052412753924\nE430052412753925\nR430052412753920\nK430052412753934\nK430052412753940\nC430052412753903\nZ430052412753986\nF430052412753944\nV430052412753923\nG430052412753913\nN430052412753940\nE430052412753951\nF430052412753949\nP430052412753965\nS430052412753937\nG430052412753961\nF430052412753989\nD430052412753995\nI430052412753983\nI430052412753963\nB430052412753997\nN430052412753985\nX430052412753920\nS430052412753909\nH430052412753900\nN430052412753996\nT430052412753929\nD430052412753900\nB430052412753902\nN430052412753913\nN430052412753936\nZ430052412753989\nA430052412753901\nH430052412753905\nO430052412753938\nN430052412753995\nB430052412753922\nY430052412753994\nQ430052412753942\nA430052412753934\nH430052412753957\nP430052412753939\nQ430052412753908\nD430052412753950\nY430052412753923\nY430052412753930\nN430052412753918\nC430052412753977\nY430052412753926\nY430052412753901\nG430052412753943\nN430052412753995\nS430052412753942\nR430052412753945\nY430052412753932\nW430052412753913\nC430052412753912\nH430052412753950\nC430052412753911\nI430052412753954\nN430052412753945\nF430052412753945\nB430052412753994\nQ430052412753961\nC430052412753934\nH430052412753956\nL430052412753908\nH430052412753906\nZ430052412753991\nZ430052412753981\nU430052412753927\nK430052412753923\nT430052412753985\nF430052412753968\nH430052412753923\nK430052412753945\nC430052412753999\nO430052412753913\nS430052412753925\nA430052412753953\nG430052412753997\nI430052412753971\nG430052412753997\nD430052412753947\nN430052412753905\nB430052412753972\nI430052412753917\nF430052412753915\nY430052412753901\nW430052412753957\nM430052412753985\nK430052412753930\nQ430052412753925\nD430052412753921\nD430052412753928\nZ430052412753947\nF430052412753991\nD430052412753977\nG430052412753925\nV430052412753918\nP430052412753921\nA430052412753919\nV430052412753944\nI430052412753921\nZ430052412753986\nR430052412753911\nD430052412753991\nU430052412753999\nS430052412753962\nY430052412753982\nF430052412753971\nS430052412753911\nW430052412753994\nN430052412753930\nZ430052412753963\nN430052412753995\nF430052412753983\nW430052412753947\nR430052412753902\nB430052412753949\nV430052412753971\nB430052412753953\nU430052412753985\nK430052412753917\nQ430052412753953\nX430052412753936\nY430052412753976\nV430052412753993\nB430052412753988\nI430052412753916\nX430052412753933\nV430052412753934\nW430052412753934\nK430052412753913\nN430052412753942\nF430052412753986\nE430052412753989\nE430052412753931\nH430052412753969\nO430052412753929\nI430052412753946\nY430052412753953\nN430052412753919\nH430052412753920\nH430052412753958\nG430052412753950\nX430052412753911\nR430052412753967\nX430052412753924\nN430052412753900\nB430052412753963\nA430052412753998\nH430052412753977\nG430052412753925\nM430052412753993\nW430052412753956\nI430052412753989\nU430052412753932\nF430052412753927\nN430052412753997\nS430052412753992\nX430052412753974\nZ430052412753927\nL430052412753907\nE430052412753904\nY430052412753920\nF430052412753922\nC430052412753928\nC430052412753974\nH430052412753940\nL430052412753964\nM430052412753999\nZ430052412753937\nB430052412753997\nN430052412753982\nG430052412753904\nA430052412753981\nV430052412753957\nH430052412753962\nY430052412753941\nI430052412753927\nF430052412753913\nA430052412753926\nK430052412753997\nG430052412753922\nI430052412753911\nK430052412753941\nW430052412753936\nC430052412753944\nK430052412753902\nC430052412753919\nY430052412753903\nT430052412753919\nG430052412753999\nE430052412753951\nI430052412753939\nM430052412753952\nX430052412753946\nT430052412753936\nK430052412753905\nZ430052412753968\nE430052412753999\nS430052412753934\nW430052412753924\nF430052412753970\nG430052412753930\nT430052412753925\nZ430052412753932\nY430052412753904\nX430052412753926\nA430052412753984\nG430052412753945\nP430052412753939\nL430052412753915\nS430052412753967\nX430052412753929\nG430052412753961\nR430052412753997\nY430052412753910\nW430052412753997\nQ430052412753978\nZ430052412753991\nG430052412753941\nL430052412753909\nI430052412753936\nR430052412753992\nL430052412753901\nR430052412753983\nE430052412753952\nD430052412753945\nL430052412753985\nD430052412753925\nO430052412753952\nL430052412753921\nT430052412753986\nM430052412753969\nD430052412753932\nX430052412753922\nC430052412753960\nU430052412753927\nI430052412753919\nV430052412753980\nH430052412753998\nA430052412753955\nL430052412753958\nG430052412753987\nR430052412753911\nS430052412753927\nF430052412753997\nD430052412753920\nZ430052412753934\nY430052412753930\nH430052412753901\nF430052412753990\nE430052412753931\nV430052412753960\nA430052412753960\nK430052412753902\nK430052412753921\nC430052412753941\nF430052412753926\nY430052412753923\nE430052412753919\nS430052412753948\nB430052412753955\nG430052412753967\nR430052412753978\nQ430052412753950\nG430052412753956\nZ430052412753989\nA430052412753952\nV430052412753960\nD430052412753968\nI430052412753995\nW430052412753973\nD430052412753999\nA430052412753931\nH430052412753961\nZ430052412753936\nP430052412753977\nR430052412753986\nD430052412753967\nZ430052412753994\nP430052412753975\nW430052412753973\nH430052412753943\nF430052412753977\nM430052412753954\nZ430052412753956\nL430052412753927\nZ430052412753907\nT430052412753966\nZ430052412753948\nE430052412753937\nF430052412753940\nE430052412753982\nG430052412753955\nE430052412753949\nE430052412753915\nC430052412753931\nP430052412753929\nY430052412753901\nN430052412753913\nT430052412753951\nR430052412753942\nP430052412753994\nC430052412753929\nA430052412753956\nZ430052412753957\nN430052412753916\nF430052412753966\nT430052412753911\nQ430052412753930\nO430052412753974\nT430052412753924\nX430052412753921\nQ430052412753906\nW430052412753943\nT430052412753918\nE430052412753936\nV430052412753943\nK430052412753918\nT430052412753901\nI430052412753961\nL430052412753941\nC430052412753991\nK430052412753998\nY430052412753987\nI430052412753981\nG430052412753930\nI430052412753956\nD430052412753927\nT430052412753986\nS430052412753976\nA430052412753906\nQ430052412753972\nU430052412753983\nM430052412753952\nQ430052412753936\nO430052412753952\nN430052412753984\nO430052412753983\nL430052412753977\nW430052412753913\nN430052412753984\nS430052412753930\nS430052412753904\nG430052412753909\nK430052412753958\nD430052412753934\nM430052412753943\nG430052412753902\nP430052412753986\nG430052412753974\nR430052412753900\nZ430052412753978\nZ430052412753976\nO430052412753942\nC430052412753995\nA430052412753970\nB430052412753914\nM430052412753920\nV430052412753909\nO430052412753904\nT430052412753912\nS430052412753981\nM430052412753947\nB430052412753945\nX430052412753982\nD430052412753993\nV430052412753914\nF430052412753965\nH430052412753909\nM430052412753944\nH430052412753997\nF430052412753917\nG430052412753952\nN430052412753971\nU430052412753967\nA430052412753929\nN430052412753942\nV430052412753908\nO430052412753990\nV430052412753999\nZ430052412753902\nL430052412753944\nI430052412753949\nI430052412753919\nO430052412753978\nV430052412753995\nC430052412753981\nV430052412753991\nQ430052412753981\nP430052412753982\nU430052412753929\nP430052412753977\nD430052412753943\nX430052412753935\nG430052412753997\nD430052412753941\nV430052412753981\nK430052412753992\nL430052412753931\nV430052412753923\nN430052412753981\nW430052412753928\nU430052412753900\nN430052412753907\nR430052412753989\nG430052412753983\nI430052412753996\nL430052412753920\nS430052412753916\nD430052412753909\nS430052412753947\nX430052412753924\nB430052412753957\nB430052412753928\nR430052412753941\nO430052412753937\nP430052412753918\nU430052412753906\nT430052412753958\nG430052412753959\nW430052412753963\nG430052412753929\nX430052412753924\nC430052412753909\nZ430052412753964\nS430052412753906\nQ430052412753971\nL430052412753981\nY430052412753971\n','0.00',0,'poster/20190601/236f8ec581009776144f6bae6f248e36.jpg','1.00','#292929',1,NULL,NULL,1,0,NULL,NULL),(28,'车载摇头公仔 6个装',1,'','49.00','1.00',8888,453,'null','null','<p><img src=\"/uploads/editor/20190525/a5834b9fe12cc8e86f2fc817f4d22794.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190525/9f1fc2f0a2ae5cb8755c1f3f7483525e.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190525/5435f573387c25e97926de2edfc49cc2.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190525/3b172e72bc8cf449d03fb1de8d6a41cd.jpg\" style=\"max-width: 100%;\"><img src=\"/uploads/editor/20190525/1b451a38c91234cd381ff0b61457ea34.jpg\" style=\"max-width: 100%;\"><br></p><p><br></p>',1,1,1,'10.00',1559121080,0,'','3.50','10.00',NULL,NULL,NULL,'','0.00',0,'','5.00','',1,0,0,0,0,NULL,NULL),(29,'萝卜丁',7,'','15.00','1.00',999,237,'null','null','<p><img src=\"/uploads/editor/20190611/e663060e16c62934f2d218e2b697aaf5.jpg\" style=\"max-width:100%;\"><img src=\"/uploads/editor/20190611/72d5b70a70c1efe60713559f16a660e6.jpg\" style=\"max-width: 100%;\"><br></p>',0,0,0,'0.00',1560225275,1,'','15.00',NULL,10,NULL,NULL,'','0.00',0,'','0.00','#000000',0,0,0,0,0,NULL,NULL),(30,'测试',1,'','0.01','1.00',5989,6,'[{\"\\u5c3a\\u5bf8\":{\"name\":[\"1\",\"2\",\"3\"]}},{\"\\u989c\\u8272\":{\"name\":[\"\\u767d\",\"\\u9ed1\"]}}]','[{\"group\":\"1,\\u767d\",\"price\":\"0.01\",\"stock\":994,\"cost\":\"1.00\",\"weight\":\"1.00\"},{\"group\":\"1,\\u9ed1\",\"price\":\"1.00\",\"stock\":\"999\",\"cost\":\"1.00\",\"weight\":\"1.00\"},{\"group\":\"2,\\u767d\",\"price\":\"1.00\",\"stock\":\"999\",\"cost\":\"1.00\",\"weight\":\"1.00\"},{\"group\":\"2,\\u9ed1\",\"price\":\"1.00\",\"stock\":\"999\",\"cost\":\"1.00\",\"weight\":\"1.00\"},{\"group\":\"3,\\u767d\",\"price\":\"1.00\",\"stock\":\"999\",\"cost\":\"1.00\",\"weight\":\"1.00\"},{\"group\":\"3,\\u9ed1\",\"price\":\"1.00\",\"stock\":\"999\",\"cost\":\"1.00\",\"weight\":\"1.00\"}]','<p>收到</p><p><br/></p>',0,0,0,'0.00',1567059573,1,'','1.00',NULL,1,NULL,1,'K628014369683791\nF628014369683741\nH628014369683765\nU628014369683781\nM628014369683765\nU628014369683734\nA628014369683779\nC628014369683779\nG628014369683779\n','0.00',NULL,'poster/20190627/da5c1d6ea057a053d8d91ce284b32839.jpg','1.00','#dec4c4',NULL,NULL,NULL,0,0,NULL,NULL);

/*Table structure for table `phpecs_store_product_coupon` */

DROP TABLE IF EXISTS `phpecs_store_product_coupon`;

CREATE TABLE `phpecs_store_product_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL COMMENT '优惠券名称',
  `total` int(11) DEFAULT NULL COMMENT '发放总量 0为不限',
  `form` tinyint(1) DEFAULT NULL COMMENT '优惠形式 0指定金额 1折扣',
  `discount` decimal(10,2) DEFAULT NULL COMMENT '优惠(折扣或者金额)',
  `doorsill` tinyint(1) DEFAULT NULL COMMENT '是否有无使用门槛 0否 1是',
  `full` decimal(10,2) DEFAULT NULL COMMENT '满xx元可用',
  `limit` int(11) DEFAULT NULL COMMENT '每人限领张数 0为不限',
  `term` tinyint(1) DEFAULT NULL COMMENT '有效期限类型 0为指定开始结束时间 1为领券当时起多少天内有效',
  `valid` varchar(50) DEFAULT NULL COMMENT '有效时间',
  `apply` tinyint(1) DEFAULT NULL COMMENT '适用范围 0全店通用 1指定商品',
  `shop` text COMMENT '指定商品',
  `explain` varchar(500) DEFAULT NULL COMMENT '使用说明',
  `receive` int(11) DEFAULT NULL COMMENT '已领取数量',
  `use` int(11) DEFAULT NULL COMMENT '已使用数量',
  `status` tinyint(1) DEFAULT '1' COMMENT '优惠券状态 0失效 1正常',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='优惠券';

/*Data for the table `phpecs_store_product_coupon` */

/*Table structure for table `phpecs_store_product_evaluate` */

DROP TABLE IF EXISTS `phpecs_store_product_evaluate`;

CREATE TABLE `phpecs_store_product_evaluate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `pid` int(11) DEFAULT NULL COMMENT '评价产品',
  `content` text COMMENT '评价内容',
  `time` int(11) DEFAULT NULL COMMENT '评价时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评价表';

/*Data for the table `phpecs_store_product_evaluate` */

/*Table structure for table `phpecs_store_product_evaluate_img` */

DROP TABLE IF EXISTS `phpecs_store_product_evaluate_img`;

CREATE TABLE `phpecs_store_product_evaluate_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL COMMENT '关联评价内容',
  `img` varchar(225) DEFAULT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='关联评论图片表';

/*Data for the table `phpecs_store_product_evaluate_img` */

/*Table structure for table `phpecs_store_product_img` */

DROP TABLE IF EXISTS `phpecs_store_product_img`;

CREATE TABLE `phpecs_store_product_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL COMMENT '关联产品id',
  `img` varchar(225) DEFAULT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=331 DEFAULT CHARSET=utf8;

/*Data for the table `phpecs_store_product_img` */

insert  into `phpecs_store_product_img`(`id`,`pid`,`img`) values (301,27,'p_img/20190611/8138d7bdf1781785aabd23c4bfd1961a.jpg'),(32,6,'p_img/20190317/b7cc099433a019e4b14e8b11a9c7cb7c.jpg'),(37,5,'p_img/20190317/dd9f00dd26dff319d68840f7922369cd.jpg'),(36,5,'p_img/20190317/bdde276bfc3dd022be46b0e9cb21721a.jpg'),(303,3,'p_img/20190709/e9d2e348fbf6b6f897af8b7f195db941.jpg'),(38,4,'p_img/20190317/901b9371eac33a6ed4544cda8b73ca56.jpg'),(31,6,'p_img/20190317/086a747bed88f077da844e395f8b4892.jpg'),(269,7,'p_img/20190317/4dd8e80f16660103a61df1f9250a6004.jpg'),(268,7,'p_img/20190317/4ccb9f09489e5c3241e9123c4f7206e3.jpg'),(267,7,'p_img/20190317/8486222bd5e5b91a550936f8a9b88ae6.jpg'),(30,8,'p_img/20190317/bea6d3e6aa8dc0b59a3c2547145e439a.jpg'),(29,8,'p_img/20190317/17ac598fd62cb8024826e2ee451b8100.jpg'),(297,9,'p_img/20190317/689b3e895fe77ae9599adc1ebe3590bb.jpg'),(296,9,'p_img/20190317/dfaf04560a354edeeb49e32dc77d9fca.jpg'),(295,9,'p_img/20190317/f19abef51ecf2b6bbad7ce788a0f1ac8.jpg'),(43,10,'p_img/20190317/f55a387dd103ee0462e5fbad4f58903b.jpg'),(42,10,'p_img/20190317/629ebbee0ac6d619875dbb4b415edfbd.jpg'),(41,10,'p_img/20190317/e5ee73faa34d44c78dce79c43227ff79.jpg'),(46,13,'p_img/20190318/99e52956370b7c0c4efc69ef8f345f00.jpg'),(213,28,'p_img/20190525/6bd8ef247c2ef2770dd10e2aeba34b4f.jpg'),(70,14,'p_img/20190412/2b068fa2c465b9d5e90fc7d88a22a492.jpg'),(69,14,'p_img/20190412/39bcf43e4eb26419fc65b1fa5eb2fb23.jpg'),(68,14,'p_img/20190412/bd737f0ae6515751ce0d422c89237a80.jpg'),(221,16,'p_img/20190412/9a8918ec6a57c0c9fa3a55832e64edc1.jpg'),(283,15,'p_img/20190412/999481e6c2e945c3eb29bee8b74ed351.jpg'),(282,15,'p_img/20190412/39c0a23613bd067217323c4403c120ca.jpg'),(220,16,'p_img/20190412/22c389a2f06ac0c932396b1dbbfc4762.jpg'),(219,16,'p_img/20190412/30762b43b0f5b6afa76788ecca658768.jpg'),(286,17,'p_img/20190412/e075b9c9926bb392098b972d60a29c8c.jpg'),(285,17,'p_img/20190412/dcfe2370aca9eeb5ae58c53274391736.jpg'),(284,17,'p_img/20190412/9cf9e13224a6a719110e6af0aede6ebd.jpg'),(238,18,'p_img/20190412/fe7e7ef5372b5b1d4ce3702c089f2dc1.jpg'),(237,18,'p_img/20190412/b245dee72f3873975c9b72b5b3ffb680.jpg'),(330,19,'p_img/20190413/e05c1b78c67daeb817d0fbd2d3fae040.jpg'),(329,19,'p_img/20190413/13965267fef5a8d9d47bc128e68cfb09.jpg'),(328,19,'p_img/20190413/279b2caae4640b19482bff77b71f0a02.jpg'),(327,19,'p_img/20190413/0cf5a3d85c16f7596a0516acbf5837ca.jpg'),(185,20,'p_img/20190413/cffa818c433deb0a30204e12099f7124.jpg'),(184,20,'p_img/20190413/95fe2041c89aca1508a30f2a64ae77b5.jpg'),(183,20,'p_img/20190413/7ebbcf0137d3e9da3ee9b3887d201d19.jpg'),(309,21,'p_img/20190414/1b6392ce50dfd18c0d8b216a000d3a20.jpg'),(308,21,'p_img/20190414/ed753066cfbfca8c446d6d87a1b92ac6.jpg'),(281,22,'p_img/20190414/2ac995c16aeb55b4dba13d09ddcb1f4e.jpg'),(280,22,'p_img/20190414/0d90e78babb05dce27977d06a98f6d58.jpg'),(117,23,'p_img/20190414/1bd2b9e128e7cfc8710ca407352696d0.jpg'),(118,23,'p_img/20190414/42a486bf9316f1d5b8069f37d3e992be.jpg'),(225,24,'p_img/20190414/42fedbfbef5d41bede57e2e24877835e.jpg'),(224,24,'p_img/20190414/b3ea80b253b597d73c0566ed0182f5b2.jpg'),(289,25,'p_img/20190414/58025003b342f39eadeb4611804d54ae.jpg'),(288,25,'p_img/20190414/9a50e3e09dbe4a7620531291e6120a54.jpg'),(287,25,'p_img/20190414/1b620743644dce00aa919bfa842d3166.jpg'),(310,30,'p_img/20190627/171eda99f6449b343a7898ae4fbd5759.jpg'),(266,29,'p_img/20190611/e784a57058ba740cf2167fc291aa731f.jpg'),(261,26,'p_img/20190610/30564a9a727bacac99aa7fe22d96c18e.jpg'),(212,28,'p_img/20190525/2b9e0519f88cdc9af6c470eeaea96b36.jpg');

/*Table structure for table `phpecs_store_refund` */

DROP TABLE IF EXISTS `phpecs_store_refund`;

CREATE TABLE `phpecs_store_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL COMMENT '退款订单id',
  `tui` decimal(10,2) DEFAULT NULL COMMENT '退款金额',
  `reason` varchar(225) DEFAULT NULL COMMENT '退款原因',
  `time` int(11) DEFAULT NULL COMMENT '申请时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0未审核 1已通过 2已拒绝',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='退款表';

/*Data for the table `phpecs_store_refund` */

/*Table structure for table `phpecs_template` */

DROP TABLE IF EXISTS `phpecs_template`;

CREATE TABLE `phpecs_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` varchar(225) DEFAULT NULL COMMENT '模板id',
  `number` varchar(20) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '模板名称',
  `content` text COMMENT '模板内容',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='模板表';

/*Data for the table `phpecs_template` */

insert  into `phpecs_template`(`id`,`template_id`,`number`,`name`,`content`,`status`,`time`) values (1,'bQDIbVnScXL5w8qkS2VIv0ORYUfVptZKRQMcM-yloU0','AT0004','交易提醒','订单编号{{keyword1.DATA}}\n商品信息{{keyword2.DATA}}\n交易类型{{keyword3.DATA}}\n交易时间{{keyword4.DATA}}\n交易金额{{keyword5.DATA}}\n温馨提示{{keyword6.DATA}}',1,1559528632),(2,'sqGQOCWQ9ZwxmlDk8wyJkO205RRkOuKfVzB_oc_0Vfo','AT1824','核销失败通知','商品名称: {{keyword1.DATA}}\n核销时间: {{keyword2.DATA}}\n失败原因: {{keyword3.DATA}}',1,1559531509),(3,'jWlgr5a0HRzfQoAwl3YafYBDkOCpz6LWA78odWdIec4','AT0423','核销成功通知','商家名称: {{keyword1.DATA}}\n商品名称: {{keyword2.DATA}}\n核销时间: {{keyword3.DATA}}\n核销卡号: {{keyword4.DATA}}\n温馨提示: {{keyword5.DATA}}',1,1559531558),(4,'8JyGDlALQfKuixlCz35mwgksSmVNSgmRY7ycoTyuvsw','AT1615','零钱入账通知','商家名称: {{keyword1.DATA}}\n入账金额: {{keyword2.DATA}}\n入账时间: {{keyword3.DATA}}\n备注: {{keyword4.DATA}}',1,1559531598),(5,'baZYrxAqUwA2JG5yyHebsknkz8u20KFLaWyLK7YFb_0','AT1297','打卡失败通知','活动名称: {{keyword1.DATA}}\n失败原因: {{keyword2.DATA}}\n提示语: {{keyword3.DATA}}',1,1559531645),(6,'lJJ_lk_-7j7bHCJw-03zPz5op1aHbbJBzqBKTyZWQ48','AT1350','打卡成功通知','活动名称: {{keyword1.DATA}}\n打卡内容: {{keyword2.DATA}}\n提示语: {{keyword3.DATA}}',1,1559531691),(7,'-9pB3d93sICfizhvDb6uze4h4iiandzyoi3-EIMy6tE','AT1313','交易成功提醒','交易类型: {{keyword1.DATA}}\n商家名称: {{keyword2.DATA}}\n交易金额: {{keyword3.DATA}}\n交易时间: {{keyword4.DATA}}\n付款方式: {{keyword5.DATA}}\n交易编号: {{keyword6.DATA}}\n获得积分: {{keyword7.DATA}}',1,1559531754),(8,'jsAYLzq3srpGJZ4vSF0vj2gaDAt-ziEYNCYhNb2ZQVE','AT0035','收益到账通知','获得佣金: {{keyword1.DATA}}\n到账时间: {{keyword2.DATA}}\n收益来源: {{keyword3.DATA}}\n被邀请人: {{keyword4.DATA}}\n温馨提示: {{keyword5.DATA}}',1,1559531807),(9,'vl2SzY7-JUyXDJn2JvqsN_Hdh-V2CckFyBjg-mu6AqA','AT0325','兑换成功通知','兑换商品: {{keyword1.DATA}}\n兑换时间: {{keyword2.DATA}}\n兑换价值: {{keyword3.DATA}}\n订单编号: {{keyword4.DATA}}\n兑换码: {{keyword5.DATA}}\n备注信息: {{keyword6.DATA}}\n支付金额: {{keyword7.DATA}}\n商家名称: {{keyword8.DATA}}\n提取方式: {{keyword9.DATA}}',1,1559531898),(10,'xCkd6y0-1jsw6JGoisLtJVxs9wMdjZKvFuqz0G58V14','AT0533','礼品领取成功通知','礼品名称: {{keyword1.DATA}}\n赠礼商家: {{keyword2.DATA}}\n联系方式: {{keyword3.DATA}}\n领取时间: {{keyword4.DATA}}\n使用期限: {{keyword5.DATA}}\n领取人: {{keyword6.DATA}}\n使用规则: {{keyword7.DATA}}',1,1559531975),(11,'bQDIbVnScXL5w8qkS2VIv7tHi94Ol_d5gCVxicyIoI8','AT0004','交易提醒','订单编号: {{keyword1.DATA}}\n商品信息: {{keyword2.DATA}}\n下单时间: {{keyword3.DATA}}\n商品价格: {{keyword4.DATA}}\n优惠信息: {{keyword5.DATA}}\n实付金额: {{keyword6.DATA}}\n订单状态: {{keyword7.DATA}}\n备注: {{keyword8.DATA}}',1,1559532055),(12,'STGS-_yakLQbeDiMjPsezPsgKLO7X-J4pPvDvz1bwwU','AT0007','订单发货提醒','订单编号: {{keyword1.DATA}}\n发货时间: {{keyword2.DATA}}\n温馨提示: {{keyword3.DATA}}\n商品清单: {{keyword4.DATA}}',1,1559532103),(13,'MsNVlRzTHd3ygHg6WpiOEF4VrphdCxmvSVdWK49WPWo','AT0016','充值成功通知','订单号: {{keyword1.DATA}}\n充值类型: {{keyword2.DATA}}\n充值时间: {{keyword3.DATA}}\n充值金额: {{keyword4.DATA}}\n赠送金额: {{keyword5.DATA}}\n充值用户: {{keyword6.DATA}}\n充值结果: {{keyword7.DATA}}',1,1559532150),(14,'eAhO7Xe2oLJdP32-yGBpBnDp00Z4rji8YXDq3VAcrmc','AT0407','打卡提醒','活动名称: {{keyword1.DATA}}\n活动日期: {{keyword2.DATA}}\n任务说明: {{keyword3.DATA}}',1,1560132224);

/*Table structure for table `phpecs_user` */

DROP TABLE IF EXISTS `phpecs_user`;

CREATE TABLE `phpecs_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(32) DEFAULT NULL COMMENT '小程序openid',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `name` varchar(225) DEFAULT NULL COMMENT '微信编译名称',
  `real_name` varchar(225) DEFAULT NULL COMMENT '微信名称',
  `portrait` varchar(225) DEFAULT NULL COMMENT '用户头像',
  `grade` int(11) DEFAULT '0' COMMENT '会员等级',
  `upper` int(11) DEFAULT '0' COMMENT '上级推广账户',
  `recruit` int(11) DEFAULT '0' COMMENT '上级招商账户',
  `card` int(11) DEFAULT '0' COMMENT '重签卡',
  `balance` decimal(10,2) DEFAULT '0.00' COMMENT '我的余额',
  `give` decimal(10,2) DEFAULT '0.00' COMMENT '赠送余额',
  `cashback` decimal(10,2) DEFAULT '0.00' COMMENT '返现余额',
  `sure` decimal(10,2) DEFAULT '0.00' COMMENT '可提现',
  `gold` decimal(10,2) DEFAULT '0.00' COMMENT '我的佣金',
  `integral` decimal(10,2) DEFAULT '0.00' COMMENT '我的积分',
  `qr_code` varchar(225) DEFAULT NULL COMMENT '我的推广码',
  `collect` varchar(225) DEFAULT NULL COMMENT '我的收款码',
  `carry` varchar(225) DEFAULT NULL COMMENT '招商提供礼品商家码',
  `lead` varchar(225) DEFAULT NULL COMMENT '招商引流商家码',
  `writer` varchar(225) DEFAULT NULL COMMENT '添加下级核销员码',
  `time` int(11) DEFAULT NULL COMMENT '第一次加入时间',
  `is_jin` tinyint(11) DEFAULT '0' COMMENT '是否禁用用户 为1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='小程序凭证';

/*Data for the table `phpecs_user` */

/*Table structure for table `phpecs_user_address` */

DROP TABLE IF EXISTS `phpecs_user_address`;

CREATE TABLE `phpecs_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `name` varchar(50) DEFAULT NULL COMMENT '收货人',
  `phone` varchar(30) DEFAULT NULL COMMENT '联系手机号',
  `province` varchar(50) DEFAULT NULL COMMENT '省',
  `city` varchar(50) DEFAULT NULL COMMENT '市',
  `county` varchar(50) DEFAULT NULL COMMENT '县',
  `address` varchar(225) DEFAULT NULL COMMENT '详细地址',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否为默认地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户地址';

/*Data for the table `phpecs_user_address` */

/*Table structure for table `phpecs_user_carry` */

DROP TABLE IF EXISTS `phpecs_user_carry`;

CREATE TABLE `phpecs_user_carry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '提交的用户id',
  `recruit` int(11) DEFAULT NULL COMMENT '上级招商业务员',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) DEFAULT NULL COMMENT '审核 0未审核 1审核通过 2拒绝通过 3暂未提供押金',
  `name` varchar(30) DEFAULT NULL COMMENT '店铺名称',
  `address` varchar(225) DEFAULT NULL COMMENT '商家地址',
  `longitude` float DEFAULT NULL COMMENT '经度',
  `latitude` float DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='提供礼品商家审核';

/*Data for the table `phpecs_user_carry` */

/*Table structure for table `phpecs_user_cart` */

DROP TABLE IF EXISTS `phpecs_user_cart`;

CREATE TABLE `phpecs_user_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `pid` int(11) DEFAULT NULL COMMENT '产品id',
  `spec` varchar(225) DEFAULT NULL COMMENT '产品规格',
  `num` int(11) DEFAULT NULL COMMENT '选择数量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='购物车表';

/*Data for the table `phpecs_user_cart` */

/*Table structure for table `phpecs_user_cash` */

DROP TABLE IF EXISTS `phpecs_user_cash`;

CREATE TABLE `phpecs_user_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(50) DEFAULT NULL COMMENT '订单号',
  `money` decimal(10,2) DEFAULT NULL COMMENT '提现金额',
  `uid` int(11) DEFAULT NULL COMMENT '提现用户',
  `time` int(11) DEFAULT NULL COMMENT '提现时间',
  `type` tinyint(1) DEFAULT NULL COMMENT '提现类型 0佣金 1余额',
  `status` tinyint(1) DEFAULT NULL COMMENT '提现状态 0未审核 1审核通过 2拒绝通过',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='提现管理';

/*Data for the table `phpecs_user_cash` */

/*Table structure for table `phpecs_user_check` */

DROP TABLE IF EXISTS `phpecs_user_check`;

CREATE TABLE `phpecs_user_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(50) DEFAULT NULL COMMENT '订单号',
  `shop_id` int(11) DEFAULT NULL COMMENT '付款给商家',
  `uid` int(11) DEFAULT NULL COMMENT '买单用户',
  `time` int(11) DEFAULT NULL COMMENT '买单时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '买单状态 0待买单 1已买单',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '买单金额',
  `zeng` decimal(10,2) DEFAULT '0.00' COMMENT '抵扣赠送余额',
  `shi` decimal(10,2) DEFAULT '0.00' COMMENT '实付金额',
  `ti` decimal(10,2) DEFAULT '0.00' COMMENT '增加提现余额',
  `type` tinyint(1) DEFAULT NULL COMMENT '付款方式 0余额买单 1微信买单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='买单记录表';

/*Data for the table `phpecs_user_check` */

/*Table structure for table `phpecs_user_choose` */

DROP TABLE IF EXISTS `phpecs_user_choose`;

CREATE TABLE `phpecs_user_choose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '关联引流商家',
  `package` int(11) DEFAULT NULL COMMENT '选择引流的大礼包',
  `img` varchar(225) DEFAULT NULL COMMENT '大礼包二维码',
  `status` tinyint(1) DEFAULT '0' COMMENT '审核状态',
  `money` decimal(10,2) DEFAULT NULL COMMENT '礼包所需价格',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='引流商家选择的大礼包';

/*Data for the table `phpecs_user_choose` */

/*Table structure for table `phpecs_user_coupon` */

DROP TABLE IF EXISTS `phpecs_user_coupon`;

CREATE TABLE `phpecs_user_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '领取用户',
  `cid` int(11) DEFAULT NULL COMMENT '领取优惠券',
  `time` int(11) DEFAULT NULL COMMENT '领取时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0已领取 1已使用',
  `type` tinyint(1) DEFAULT '0' COMMENT '0自己领取 1系统赠送',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户领取优惠券表';

/*Data for the table `phpecs_user_coupon` */

/*Table structure for table `phpecs_user_gold` */

DROP TABLE IF EXISTS `phpecs_user_gold`;

CREATE TABLE `phpecs_user_gold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(10,2) DEFAULT NULL COMMENT '总消费金额',
  `gold` decimal(10,2) DEFAULT NULL COMMENT '产生佣金',
  `uid` int(11) DEFAULT NULL COMMENT '上级用户',
  `user` varchar(20) DEFAULT NULL COMMENT '购买用户',
  `content` varchar(225) DEFAULT NULL COMMENT '具体信息',
  `time` int(11) DEFAULT NULL COMMENT '产生时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='佣金记录表';

/*Data for the table `phpecs_user_gold` */

/*Table structure for table `phpecs_user_gold_record` */

DROP TABLE IF EXISTS `phpecs_user_gold_record`;

CREATE TABLE `phpecs_user_gold_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(35) DEFAULT NULL COMMENT '订单号',
  `uid` int(11) DEFAULT NULL COMMENT '充值用户',
  `num` int(11) DEFAULT NULL COMMENT '礼品数量',
  `gold` decimal(10,2) DEFAULT NULL COMMENT '每单佣金',
  `time` int(11) DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否支付成功 0未支付 1已支付',
  `cid` int(11) DEFAULT NULL COMMENT '充值审核id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='礼品商家充值佣金记录';

/*Data for the table `phpecs_user_gold_record` */

/*Table structure for table `phpecs_user_grade` */

DROP TABLE IF EXISTS `phpecs_user_grade`;

CREATE TABLE `phpecs_user_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL COMMENT '等级名称',
  `grade` int(11) DEFAULT NULL COMMENT '等级',
  `img` varchar(225) DEFAULT NULL COMMENT '等级图片',
  `discount` decimal(10,2) DEFAULT NULL COMMENT '享受折扣',
  `status` tinyint(1) DEFAULT '1' COMMENT '是否启用 1启用 0不启用',
  `explain` varchar(225) DEFAULT NULL COMMENT '等级说明',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='会员等级表';

/*Data for the table `phpecs_user_grade` */

insert  into `phpecs_user_grade`(`id`,`name`,`grade`,`img`,`discount`,`status`,`explain`) values (1,'银卡会员',1,'grade/20190905/f6246ca5df4cd9a8eebc2a5c60d99a5c.png','99.00',1,'银卡会员'),(3,'金卡会员',2,'grade/20190905/3b3d6981a58c1124fc5a3293dc6e5321.png','85.00',1,'金卡会员'),(4,'黑卡会员',3,'grade/20190905/124925abd0da4c71fd49fb9f771671cf.png','75.00',1,'黑卡会员'),(5,'钻石会员',4,'grade/20190905/bd47929c386b263c27066d4f0e4967c1.png','65.00',1,'钻石会员'),(6,'至尊会员',5,'grade/20190905/36358825d0d9b6f1abd3589eaa0d35a6.png','50.00',1,'至尊会员');

/*Table structure for table `phpecs_user_lead` */

DROP TABLE IF EXISTS `phpecs_user_lead`;

CREATE TABLE `phpecs_user_lead` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '提交的用户id',
  `recruit` int(11) DEFAULT NULL COMMENT '上级招商业务员',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) DEFAULT NULL COMMENT '审核 0未审核 1审核通过 2拒绝通过',
  `name` varchar(30) DEFAULT NULL COMMENT '店铺名称',
  `address` varchar(225) DEFAULT NULL COMMENT '商家地址',
  `longitude` float DEFAULT NULL COMMENT '经度',
  `latitude` float DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='引流商家申请';

/*Data for the table `phpecs_user_lead` */

/*Table structure for table `phpecs_user_lipin` */

DROP TABLE IF EXISTS `phpecs_user_lipin`;

CREATE TABLE `phpecs_user_lipin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户',
  `lid` int(11) DEFAULT NULL COMMENT '礼品id',
  `img` varchar(225) DEFAULT NULL COMMENT '核销码',
  `stage` int(11) DEFAULT NULL COMMENT '领取期数',
  `time` int(11) DEFAULT NULL COMMENT '领取时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户领取礼品记录表';

/*Data for the table `phpecs_user_lipin` */

/*Table structure for table `phpecs_user_package` */

DROP TABLE IF EXISTS `phpecs_user_package`;

CREATE TABLE `phpecs_user_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `package` int(11) DEFAULT NULL COMMENT '已领取的大礼包id',
  `yid` int(11) DEFAULT NULL COMMENT '引流商家id',
  `formId` varchar(50) DEFAULT NULL COMMENT 'formid',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='大礼包表';

/*Data for the table `phpecs_user_package` */

/*Table structure for table `phpecs_user_package_record` */

DROP TABLE IF EXISTS `phpecs_user_package_record`;

CREATE TABLE `phpecs_user_package_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(35) DEFAULT NULL COMMENT '订单号',
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `pid` int(11) DEFAULT NULL COMMENT '大礼包id',
  `yid` int(11) DEFAULT NULL COMMENT '引流商家id',
  `money` decimal(10,2) DEFAULT NULL COMMENT '总金额',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `time` int(11) DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='购买大礼包';

/*Data for the table `phpecs_user_package_record` */

/*Table structure for table `phpecs_user_pifa` */

DROP TABLE IF EXISTS `phpecs_user_pifa`;

CREATE TABLE `phpecs_user_pifa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '申请人',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态 0待审核 1审核通过 2未通过',
  `time` int(11) DEFAULT NULL COMMENT '提交时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='批发会员申请';

/*Data for the table `phpecs_user_pifa` */

/*Table structure for table `phpecs_user_record` */

DROP TABLE IF EXISTS `phpecs_user_record`;

CREATE TABLE `phpecs_user_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` varchar(50) DEFAULT NULL COMMENT '订单号',
  `uid` int(11) DEFAULT NULL COMMENT '充值用户',
  `time` int(11) DEFAULT NULL COMMENT '充值时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '充值状态 0未支付 1已支付',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '充值余额',
  `give` decimal(10,2) DEFAULT '0.00' COMMENT '赠送余额',
  `cashback` decimal(10,2) DEFAULT '0.00' COMMENT '返现余额',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='充值记录';

/*Data for the table `phpecs_user_record` */

/*Table structure for table `phpecs_user_role` */

DROP TABLE IF EXISTS `phpecs_user_role`;

CREATE TABLE `phpecs_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '关联用户id',
  `business` tinyint(1) DEFAULT '0' COMMENT '业务员0否 1是',
  `gift` tinyint(1) DEFAULT '0' COMMENT '礼品商家',
  `drainage` tinyint(1) DEFAULT '0' COMMENT '引流商家',
  `pf` tinyint(1) DEFAULT '0' COMMENT '批发会员',
  `super` tinyint(1) DEFAULT '0' COMMENT '超级管理员',
  `retail` tinyint(1) DEFAULT '0' COMMENT '分销',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色表';

/*Data for the table `phpecs_user_role` */

/*Table structure for table `phpecs_user_scene` */

DROP TABLE IF EXISTS `phpecs_user_scene`;

CREATE TABLE `phpecs_user_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `lid` int(11) DEFAULT NULL COMMENT '礼品id',
  `stage` int(11) DEFAULT NULL COMMENT '期数id',
  `yid` int(11) DEFAULT NULL COMMENT '引流商家id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='小程序转发参数记录表';

/*Data for the table `phpecs_user_scene` */

/*Table structure for table `phpecs_user_trade` */

DROP TABLE IF EXISTS `phpecs_user_trade`;

CREATE TABLE `phpecs_user_trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '关联用户表',
  `name` varchar(30) DEFAULT NULL COMMENT '店铺名称',
  `address` varchar(225) DEFAULT NULL COMMENT '商家地址',
  `latitude` float DEFAULT NULL COMMENT '纬度',
  `longitude` float DEFAULT NULL COMMENT '经度',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商家店铺信息';

/*Data for the table `phpecs_user_trade` */

/*Table structure for table `phpecs_user_writer` */

DROP TABLE IF EXISTS `phpecs_user_writer`;

CREATE TABLE `phpecs_user_writer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '所属商家',
  `xid` int(11) DEFAULT NULL COMMENT '下级核销员id',
  `time` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='下级核销员';

/*Data for the table `phpecs_user_writer` */

/*Table structure for table `phpecs_verify` */

DROP TABLE IF EXISTS `phpecs_verify`;

CREATE TABLE `phpecs_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `code` varchar(10) DEFAULT NULL COMMENT '验证码',
  `time` int(11) DEFAULT NULL COMMENT '生成时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='验证码';

/*Data for the table `phpecs_verify` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

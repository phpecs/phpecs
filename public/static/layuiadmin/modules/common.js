/**

 @Name：layuiAdmin 公共业务
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL

 */

layui.define(['tableSelect'], function (exports) {
    var $ = layui.$
        , layer = layui.layer
        , laytpl = layui.laytpl
        , setter = layui.setter
        , view = layui.view
        , admin = layui.admin
        , tableSelect = layui.tableSelect;

    //退出
    admin.events.logout = function () {
        //执行退出接口
        layer.confirm('是否退出', {icon: 3, title: '提示'}, function (index) {
            $.post('/admin/Login/logoutt', '', function (e) {
                if (e.info == 1) {
                    //登入成功的提示与跳转
                    layer.msg('退出成功', {
                        icon: 1
                        , time: 1000
                    }, function () {
                        //清空本地记录的 token，并跳转到登入页
                        admin.exit(function () {
                            location.href = '/admin/Login/login';
                        });
                    });
                } else {
                    layer.msg(e.msg, {
                        icon: 2
                        , time: 1000
                    });
                }
            }, 'json');
        });
    };

    //获取页面数据
    admin.events.page = function(){
        var index = '';
        var that = $(this);
        $.post("/admin/Common/page_links", function(e){
            var data = e.data;
            var html = '<div class="layui-card"><div class="layui-card-body">' +
                '<table class="layui-table"><colgroup><col width="80%"><col width="20%"><col></colgroup><thead><tr><th>页面名称</th><th>操作</th></thead><tbody>';
            for (var i = 0; i < data.length; i++){
                var zhi = data[i];
                html += '<tr><td>' + zhi.mark + '</td><td>';
                if (zhi.c_link){
                    html += '<button style="position: absolute;left: 15px;top: 8px;" data-mark="' + zhi.mark + '" data-c_link="' + zhi.c_link + '" class="layui-btn layui-btn-xs choice-content">内容</button>';
                }
                if (zhi.link){
                    html += '<button style="position: absolute;left: 60px;top: 8px;margin-left: 0;" data-link="' + zhi.link + '" data-column="' + zhi.column + '" class="layui-btn layui-btn-normal layui-btn-xs choice">选择</button>';
                }
                html += '</td></tr>';
            }
            html += '</tbody></table></div></div>';
            index = parent.parent.layer.open({
                title: '选择链接',
                type: 1,
                shade: 0.1,
                area: ['600px', '500px'],
                move: false,
                content: html //这里content是一个普通的String
            });
        }, 'json');
        //链接选择
        $(parent.parent.document).off('click').on('click', '.choice', function (){
            var link = $(this).data('link');
            var column = $(this).data('column');
            that.val(link);
            that.next().val(column);
            parent.parent.layer.close(index);
        });
        $(parent.parent.document).on('click', '.choice-content', function () {
            var mark = $(this).data('mark');
            var c_link = $(this).data('c_link');
            var type = 1;
            switch (mark) {
                case '商品中心':
                    type = 1;
                    break;
                case '帮助中心':
                    type = 2;
                    break;
                case '承接页':
                    type = 3;
                    break;
                case '打卡活动':
                    type = 4;
                    break;
                case '优惠券':
                    type = 5;
                    break;
                default :
                    type = 1;
                    break;
            }
            parent.parent.layer.open({
                type: 2,
                title: '选择链接',
                content: "/admin/Common/page_list/type/" + type,
                shade: 0.1,
                area: ['600px', '500px'],
                move: false,
                btnAlign: 'c',
                success: function (layero, index2) {
                    // 获取子页面的iframe
                    var iframe = parent.parent['layui-layer-iframe' + index2];
                    // 向子页面的全局函数child传参
                    iframe.child(that, index, index2, c_link);
                }
            });
        });
    };


    //点击操作显示下拉工具
    $(document).on('click', '.operation', function (e){
        $(this).next().show();
        //按钮阻止冒泡
        e.stopPropagation();
    });

    //点击任何地方隐藏下拉工具
    $(document).click(function(){
        $(".layui-nav-child.layui-anim.layui-anim-upbit").hide();
    });

    var obj = {
        //php时间戳用js转换为yyyy-mm-dd
        formatDateTime: function(timeStamp) {
            var date = new Date();
            date.setTime(timeStamp * 1000);
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            m = m < 10 ? ('0' + m) : m;
            var d = date.getDate();
            d = d < 10 ? ('0' + d) : d;
            var h = date.getHours();
            h = h < 10 ? ('0' + h) : h;
            var minute = date.getMinutes();
            var second = date.getSeconds();
            minute = minute < 10 ? ('0' + minute) : minute;
            second = second < 10 ? ('0' + second) : second;
            return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
        },
        //补零
        returnFloat: function(value){
            value = Math.round(parseFloat(value)*100)/100;
            var xsd = value.toString().split(".");
            if(xsd.length == 1){
                value = value.toString()+".00";
                return value;
            }
            if(xsd.length>1){
                if(xsd[1].length<2){
                    value = value.toString()+"0";
                }
                return value;
            }
        },
        //秒数转时分秒
        to_time: function(second_time){
            var time = parseInt(second_time) + "秒";
            if (parseInt(second_time) > 60){
                var second = parseInt(second_time) % 60;
                var minutes = parseInt(second_time / 60);
                time = minutes + "分" + second + "秒";
                if (minutes > 60){
                    minutes = parseInt(second_time / 60) % 60;
                    var hours = parseInt(parseInt(second_time / 60) /60);
                    time = hours + "小时" + minutes + "分" + second + "秒";
                    if ( hours > 24 ){
                        hours = parseInt(parseInt(second_time / 60) /60) % 24;
                        var day = parseInt(parseInt(parseInt(second_time / 60) /60) / 24);
                        time = day + "天" + hours + "小时" + minutes + "分" + second + "秒";
                    }
                }
            }else if(parseInt(second_time) == 0){
                time = '无限制';
            }
            return time;
        }
    };

    //对外暴露的接口
    exports('common', obj);
});